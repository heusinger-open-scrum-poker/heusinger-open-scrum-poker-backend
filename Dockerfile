FROM docker.io/eclipse-temurin:21.0.2_13-jre

COPY ./application/target/multi-module-application-0.0.1-SNAPSHOT.jar /usr/app/

RUN useradd guest
RUN chown -R guest /usr/app/
USER guest
WORKDIR /usr/app

ENV SPRING_JPA_HIBERNATE_DDL-AUTO=none

EXPOSE 8080

ENTRYPOINT java -jar multi-module-application-0.0.1-SNAPSHOT.jar
