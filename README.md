# Agile Casino Backend

The current interface contract can be found at <https://app.swaggerhub.com/apis/Kehrwasser/heusinger-open-scrum-poker-api/1.1.0>


## Build

Using Maven:

```sh
mvn package -Dmaven.test.skip
```

Run JAR:



## Configuration

Set following environment variables to configure the connection to your MySQL5 server.


```sh
MYSQL_HOST=<mysql server address>
MYSQL_PORT=<mysql server port>
MYSQL_DATABASE=db
MYSQL_USER=user
MYSQL_PASSWORD=<mysql password>

ADMIN_USER=<username for secured API endpoints>
ADMIN_PASSWORD=<password for secured API endpoints>

SMTP_HOST=<smtp server host>
SMTP_PORT=<smtp port>
SMTP_USER=<smtp user>
SMTP_PASSWORD=<smtp password>
SMTP_SENDER_ADDRESS=<email address>
SMTP_SENDER_NAME=<display name>

JIRA_CLIENT_ID=<jira oauth client id>
JIRA_SECRET=<jira oauth secret>

OPENAI_API_KEY=<openai api key>

FRONTEND_BASE_URL=<url of the frontend>

PERFORMANCE_LOGGING_PROBABILITY=<value ranging from 0.0 to 1.0>
```

## Continuous Deployment Guide

"Schnell scheitern. Schnell lernen."

> The goal of continuous deployment is to release applications (and business value) to end users faster and more cost-effectively by managing small, incremental software changes.

1. Make sure useful progress outputs often
2. Not work faster, but in smaller batches
3. Small is beautiful: It can be debugged faster and value arrives faster at the user
4. If something small is useful and production ready, push it to `master`
5. Only push finished, potentially releasable stuff to master
6. Break down features in daily ready tasks
7. Automatic testing happens on each commit on `master`
8. Passing code is considered as an valid increment and thus a release candidate
9. It is automatically deployed to the staging environment (dockerized as `:unstable` and deployed on Digital Ocean)
10. For a feature to be considered as done, it must be successfully reviewed on staging
11. On staging validated commits get a tag
12. Tags are deployed to production - a simple confirmation is needed (dockerized as the tag and as `:latest` and deployed on Digital Ocean)

## Flyway

IF DB is not prepared yet, it needs to be baseline according to it's actual state.

```mvn flyway:baseline```

Find options in pom file.

Migrations are found in `resources/db/migration`.
### Migrating
`mvn flyway:migrate`
