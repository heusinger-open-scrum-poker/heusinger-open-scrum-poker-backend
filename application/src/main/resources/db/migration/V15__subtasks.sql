create table subtask
(
    id       int                  not null
        primary key,
    task_id  int                  not null,
    subject  text                 not null,
    checked  tinyint(1) default 0 null,
    lexorank varchar(255)         null,
    constraint subtask_task_id_fk
        foreign key (task_id) references task (id)
);