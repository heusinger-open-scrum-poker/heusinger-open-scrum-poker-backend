create table openai_usage (
    id integer not null,
    user varchar(200) not null,
    model tinytext,
    prompt_tokens integer,
    completion_tokens integer,
    total_tokens integer,
    timestamp datetime,
    primary key (id)
);

alter table openai_usage add constraint fk_user_has_openai_usage foreign key (user) references user (uuid);
