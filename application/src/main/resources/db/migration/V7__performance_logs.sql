create table performance_log_sequence (
    next_val bigint,
    primary key (next_val)
);

insert into performance_log_sequence values ( 1 );

create table performance_log (
    id integer not null,
    start_time timestamp not null,
    duration_millis integer not null,
    method tinytext not null,
    url text not null,
    primary key (id)
);
