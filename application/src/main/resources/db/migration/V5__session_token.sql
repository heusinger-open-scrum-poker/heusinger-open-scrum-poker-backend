CREATE TABLE session_token (
    id INTEGER NOT NULL,
    value VARCHAR(255) NOT NULL,
    created_at DATE NOT NULL,
    expires_at DATE NOT NULL,
    user VARCHAR(200) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE session_token ADD CONSTRAINT FKGANG5aGtS1aAftC3aQPGnbbGY FOREIGN KEY (user) REFERENCES user (uuid);
