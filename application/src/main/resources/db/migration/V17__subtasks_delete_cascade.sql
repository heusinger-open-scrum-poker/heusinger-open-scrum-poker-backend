alter table subtask
    drop foreign key subtask_task_id_fk;

alter table subtask
    add constraint subtask_task_id_fk
        foreign key (task_id) references task (id)
            on delete cascade;