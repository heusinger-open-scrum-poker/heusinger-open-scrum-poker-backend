create table feedback (id integer not null, content varchar(255), primary key (id));
create table hibernate_sequence (next_val bigint);
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
create table player (id integer not null, avatar varchar(255), card_value varchar(255), name varchar(255), type varchar(255), room_id integer, primary key (id));
create table room (id integer not null, created_at date not null, max_players integer, rounds_played integer, status varchar(255), primary key (id));
alter table player add constraint FK5ngp5074vt6ns2dixliw5ry36 foreign key (room_id) references room (id);
