create table game
(
    id         int      not null
        primary key,
    room_id    int      not null,
    name       text     not null,
    created_at datetime not null,
    constraint game_room_id_fk
        foreign key (room_id) references room (id)
);

alter table room
    add active_game_id int null;

alter table room
    add constraint room_game_id_fk
        foreign key (active_game_id) references game (id);

alter table task
    add game_id int null;

alter table task
    add constraint task_game_id_fk
        foreign key (game_id) references game (id);

