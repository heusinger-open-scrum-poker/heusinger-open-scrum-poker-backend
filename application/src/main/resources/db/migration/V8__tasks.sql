create table task (
    id integer not null,
    room_id integer not null,
    subject text not null,
    description text not null,
    estimate tinytext not null,
    provider text,
    metadata text,
    primary key (id)
);

alter table task add constraint FKAeXah9ou9AiyaekahyeSeMis0 foreign key (room_id) references room (id);

alter table room add current_task integer;
alter table room add constraint FKoocee6xaeTaipongae7du9aedF foreign key (current_task) references task (id);
