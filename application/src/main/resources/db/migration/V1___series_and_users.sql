create table user (uuid varchar(200) not null, created_at date not null, hash varchar(200) not null, invited_by_uuid varchar(200), primary key (uuid));
alter table user add constraint FKebs40vpfyb9if6tk0bxiunurc foreign key (invited_by_uuid) references user (uuid);
ALTER TABLE room ADD COLUMN series varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE player ADD COLUMN user_uuid varchar(200) DEFAULT NULL;
alter table player add constraint FKnu2lebgkv7f5eqf7dsxxd0e52 foreign key (user_uuid) references user (uuid);
