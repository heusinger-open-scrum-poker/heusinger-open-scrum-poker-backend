alter table auth_method add column reset_token text;
alter table auth_method add column reset_token_timestamp datetime;
