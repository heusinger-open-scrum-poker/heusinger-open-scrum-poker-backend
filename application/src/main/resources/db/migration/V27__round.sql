create table round
(
    id int not null primary key,
    initiator_id varchar(200) not null,
    revealer_id varchar(200) null,
    task_id int null,
    game_id int not null,
    room_id int not null,
    started_at datetime not null,
    revealed_at datetime null,
    consensus tinytext null
);

alter table room
    add active_round_id int null;

alter table room
    add constraint room_round_id_fk
        foreign key (active_round_id) references round (id);