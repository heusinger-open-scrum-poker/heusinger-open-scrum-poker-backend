create table jira_oauth (
    id integer not null,
    user varchar(200) not null,
    state text,
    access_token text,
    refresh_token text,
    token_type text,
    scope text,
    expiration_datetime datetime,
    primary key (id)
);

alter table jira_oauth add constraint fk_user_has_jira_source foreign key (user) references user (uuid);
