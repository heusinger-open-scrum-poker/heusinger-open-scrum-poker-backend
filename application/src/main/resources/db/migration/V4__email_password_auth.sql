ALTER TABLE user ENGINE=InnoDB;

CREATE TABLE auth_method (
    id INTEGER NOT NULL,
    user VARCHAR(200) NOT NULL,
    auth_type VARCHAR(80) NOT NULL,
    email VARCHAR(255),
    password_hash VARBINARY(64),
    password_salt VARBINARY(64),
    PRIMARY KEY (id)
);

ALTER TABLE auth_method ADD CONSTRAINT FKc3PECTzvPvFHcCLobPNdJhWWy FOREIGN KEY (user) REFERENCES user (uuid);
ALTER TABLE user ADD COLUMN auth_method INTEGER;
ALTER TABLE user ADD CONSTRAINT FK0c3Ih6dx3SORITRie4DR6Jgai FOREIGN KEY (auth_method) REFERENCES auth_method (id);
