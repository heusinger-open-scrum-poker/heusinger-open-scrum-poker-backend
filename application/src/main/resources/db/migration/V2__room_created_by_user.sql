ALTER TABLE room ADD COLUMN created_by varchar(200) DEFAULT NULL;
ALTER TABLE room ADD CONSTRAINT FKoig2phaedo6saengooNgazequ FOREIGN KEY (created_by) REFERENCES user (uuid);
