package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class HttpForbidden extends RuntimeException {
    public HttpForbidden() {
        super("Unauthorized");
    }

    public HttpForbidden(String message) {
        super(message);
    }
}
