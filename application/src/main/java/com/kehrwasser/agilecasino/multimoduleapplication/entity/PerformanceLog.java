package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "performance_log")
public class PerformanceLog {

    @Id
    @GeneratedValue(generator = "performance_log_sequence")
    private Integer id;

    @Column(name = "start_time")
    private Timestamp startTime;

    @Column(name = "duration_millis")
    private Integer durationMillis;

    @Column(name = "method", columnDefinition = "TINYTEXT")
    private String method;

    @Column(name = "url", columnDefinition = "TEXT")
    private String url;

    public PerformanceLog() {}

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Integer getDurationMillis() {
        return this.durationMillis;
    }

    public void setDurationMillis(Integer durationMillis) {
        this.durationMillis = durationMillis;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
