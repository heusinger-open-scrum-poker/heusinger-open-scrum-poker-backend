package com.kehrwasser.agilecasino.multimoduleapplication.service.impl;

import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Game;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.kehrwasser.agilecasino.multimoduleapplication.payload.KickInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.LeaveInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.PromotionInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.RevealInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.RoomChange;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.SideliningInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;

public class WebSocketSender implements IWebSocketSender {

    @Autowired
    private SimpMessagingTemplate simp;

    @Override
    public void emitRoomChanged(Room room, RoomChange payload) {
        send(room, "/changed", payload);
    }

    @Override
    public void emitRoomReset(Room room, Optional<Player> payload) {
        send(room, "/reset", payload);
    }

    @Override
    public void emitRoomRevealed(Room room, RevealInfo payload) {
        send(room, "/reveal", payload);
    }

    @Override
    public void emitPlayerJoined(Room room, Player payload) {
        send(room, "/join", payload);
    }

    @Override
    public void emitPlayerUpdated(Room room, Player payload) {
        send(room, "/play", payload);
    }

    @Override
    public void emitPlayerLeft(Room room, LeaveInfo payload) {
        send(room, "/left", payload);
    }

    @Override
    public void emitPlayerKicked(Room room, KickInfo payload) {
        send(room, "/kicked", payload);
    }

    @Override
    public void emitPlayerSidelined(Room room, SideliningInfo payload) {
        send(room, "/sidelined", payload);
    }

	@Override
	public void emitPlayerPromoted(Room room, PromotionInfo payload) {
        send(room, "/promoted", payload);
	}

    @Override
    public void emitTaskCreated(Room room, Task payload) {
        send(room, "/taskCreated", payload);
    }

    @Override
    public void emitTaskUpdated(Room room, Task payload) {
        send(room, "/taskUpdated", payload);
    }

    @Override
    public void emitTaskDeleted(Room room, Task payload) {
        send(room, "/taskDeleted", payload);
    }

    @Override
    public void emitCurrentTaskChanged(Room room, Optional<Task> payload) {
        send(room, "/currentTaskChanged", payload);
    }

    @Override
    public void emitSubtaskCreated(Room room, Subtask payload) {
        send(room, "/subtaskCreated", payload);
    }

    @Override
    public void emitSubtaskUpdated(Room room, Subtask payload) {
        send(room, "/subtaskUpdated", payload);
    }

    @Override
    public void emitSubtaskDeleted(Room room, Subtask payload) {
        send(room, "/subtaskDeleted", payload);
    }

    @Override
    public void emitGameCreated(Room room, Game payload) {
        send(room, "/gameCreated", payload);
    }

    @Override
    public void emitGamesUpdated(Room room) {
        GamesUpdatedPayload payload = new GamesUpdatedPayload();
        payload.setDummyValue(1);
        send(room, "/gamesUpdated", payload);
    }

    @Override
    public void emitGameActivated(Room room, Game game) {
        GameActivatedPayload payload = new GameActivatedPayload();
        payload.setGame(game);
        send(room, "/gameActivated", payload);
    }

    private void send(Room room, String relativeDestination, Object payload) {
        String fallbackDestination = "/room/" + room.getId() + relativeDestination;

        if (room.isLocked()) {
            String destination = "/room/" + room.getAccessToken().get() + relativeDestination;

            simp.convertAndSend(destination, payload);
            simp.convertAndSend(fallbackDestination, payload);
        } else {
            simp.convertAndSend(fallbackDestination, payload);
        }
    }
}
