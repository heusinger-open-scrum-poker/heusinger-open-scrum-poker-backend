package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.Optional;


public class GetCustomFields {

    private JiraOAuth jira;
    private String resourceId;
    private String boardId;

    public GetCustomFields(JiraOAuth jira, String resourceId, String boardId) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.boardId = boardId;
    }

    public static class CustomFieldInfo {
        Optional<String> rankingField = Optional.of("customfield_10019");
        Optional<String> estimateField = Optional.empty();
        public Optional<String> getRankingField() {
            return rankingField;
        }
        public void setRankingField(Optional<String> rankingField) {
            this.rankingField = rankingField;
        }
        public Optional<String> getEstimateField() {
            return estimateField;
        }
        public void setEstimateField(Optional<String> estimateField) {
            this.estimateField = estimateField;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ConfigurationResponse {
        Integer id;
        String name;
        String type;
        String self;
        EstimationSpec estimation;
        RankingSpec ranking;
        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }
        public String getSelf() {
            return self;
        }
        public void setSelf(String self) {
            this.self = self;
        }
        public EstimationSpec getEstimation() {
            return estimation;
        }
        public void setEstimation(EstimationSpec estimation) {
            this.estimation = estimation;
        }
        public RankingSpec getRanking() {
            return ranking;
        }
        public void setRanking(RankingSpec ranking) {
            this.ranking = ranking;
        }
    }

    public static class EstimationSpec {
        String type;
        FieldSpec field;
        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }
        public FieldSpec getField() {
            return field;
        }
        public void setField(FieldSpec field) {
            this.field = field;
        }
    }

    public static class FieldSpec {
        String fieldId;
        String displayName;
        public String getFieldId() {
            return fieldId;
        }
        public void setFieldId(String fieldId) {
            this.fieldId = fieldId;
        }
        public String getDisplayName() {
            return displayName;
        }
        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }
    }

    public static class RankingSpec {
        private Integer rankCustomFieldId;

        public Integer getRankCustomFieldId() {
            return rankCustomFieldId;
        }

        public void setRankCustomFieldId(Integer rankCustomFieldId) {
            this.rankCustomFieldId = rankCustomFieldId;
        }
    }

    public CustomFieldInfo fetch() {
        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/agile/1.0/board/"
                        + boardId
                        + "/configuration"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();


        ObjectMapper om = new ObjectMapper();
        ConfigurationResponse resp = Util.fetchJson(om, request, new TypeReference<ConfigurationResponse>() {});

        CustomFieldInfo info = new CustomFieldInfo();
        if (resp.getEstimation() != null && resp.getEstimation().getType().equals("field")) {
            String estimateField = resp.getEstimation().getField().getFieldId();
            info.setEstimateField(Optional.of(estimateField));
        }
        if (resp.getRanking() != null) {
            String rankingField = "customfield_" + resp.getRanking().getRankCustomFieldId();
            info.setRankingField(Optional.of(rankingField));
        }

        return info;
    }
}


