package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kehrwasser.agilecasino.multimoduleapplication.repository.AuthRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.JiraOAuthRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.PlayerRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.AuthUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.EmailPasswordAuthentication;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.SessionToken;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.SessionTokenRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter.Email;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter.EmailBloomFilter;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IEmailService;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserAlreadySignedUpException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.EmailInUseException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.EmailValidationException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.ExpiredResetTokenException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.InvalidResetTokenException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.LoginAccountDoesntExistException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.LoginWrongAuthenticationMethodException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PasswordVerificationException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserMergeSourceDoesntExistException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserMergeSourceIsRegisteredException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserMergeTargetDoesntExistException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.EmailRegistrationForm;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.LoginForm;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.LoginResponse;

@RestController
@CrossOrigin(allowCredentials = "true")
public class AuthController {
    public static final String COOKIE_USER_UUID = "user-uuid";
    public static final String COOKIE_SESSION_TOKEN = "session-token";
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    @Lazy
    private AuthRepository authRepository;

    @Autowired
    @Lazy
    private SessionTokenRepository sessionTokenRepository;

    @Autowired
    @Lazy
    private UserRepository userRepository;

    @Autowired
    @Lazy
    private PlayerRepository playerRepository;

    @Autowired
    @Lazy
    private RoomRepository roomRepository;

    @Autowired
    @Lazy
    private EmailBloomFilter emailBloomFilter;

    @Autowired
    @Lazy
    private JiraOAuthRepository jiraOAuthRepository;

    @Autowired
    @Lazy
    private IEmailService emailService;

    @PostMapping("/register")
    public ResponseEntity<LoginResponse> register(@RequestBody EmailRegistrationForm body)
        throws EmailValidationException, EmailInUseException, UserAlreadySignedUpException
    {
        String password = body.getPassword();
        Optional<String> userUuid = Optional.empty();

        if (body.getUserUuid() != null) {
            userUuid = Optional.of(body.getUserUuid());
        };

        User user = userUuid.flatMap(id -> userRepository.findById(id)).orElseGet(() -> {
            User u = new User();
            u = userRepository.save(u);
            return u;
        });


        if (!user.getRole().equals(User.ROLE_ANONYMOUS)) {
            throw new UserAlreadySignedUpException(user.getUuid());
        }

        Email email = new Email(body.getEmail());

        try {
            userRepository.findByEmail(email.getEmail()).iterator().next();
            throw new EmailInUseException(email.getEmail());
        } catch (NoSuchElementException e) {
        }

        EmailPasswordAuthentication auth = new EmailPasswordAuthentication(user, email.getEmail(), password);
        user.setAuthMethod(auth);
        user.setRole(User.ROLE_SIGNEDUP);
        auth = authRepository.save(auth);
        
        SessionToken token = generateSessionToken(user);
        token = sessionTokenRepository.save(token);
        user = userRepository.save(user);

        emailBloomFilter.put(email);

        try {
            emailService.sendRegistrationEmail(user);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            LOGGER.error("User uuid was: " + user.getUuid());
            LOGGER.error("User email address was: " + user.getEmailAddress());
            e.printStackTrace();
        }

        HttpHeaders headers = createHeaders(user.getUuid(), token);
        LoginResponse responseBody = new LoginResponse(user.getUuid(), token);

        ResponseEntity<LoginResponse> responseEntity = new ResponseEntity<>(responseBody, headers, HttpStatus.OK);
        return responseEntity;
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginForm body)
        throws PasswordVerificationException, LoginWrongAuthenticationMethodException, LoginAccountDoesntExistException
    {
        String email = body.getEmail();
        String password = body.getPassword();

        Optional<User> anonUser = Optional.empty();
        if (body.getUserUuid() != null) {
            anonUser = userRepository.findById(body.getUserUuid());
        }


        Iterator<User> query = userRepository.findByEmail(email).iterator();

        if (!query.hasNext()) {
            throw new LoginAccountDoesntExistException(email);
        }

        User user = query.next();

        
        if (!(user.getAuthMethod().getAuthType().equals("email/password"))) {
            throw new LoginWrongAuthenticationMethodException("email/password");
        }

        EmailPasswordAuthentication auth = authRepository.findEmailPasswordAuthById(user.getAuthMethod().getId()).get();
        auth.verifyPassword(password);

        SessionToken token = generateSessionToken(user);

        if (anonUser.isPresent() && anonUser.get().isAnonymous()) {
            mergeAnonymousUser(anonUser.get().getUuid(), user.getUuid());
        }

        token = sessionTokenRepository.save(token);

        HttpHeaders headers = createHeaders(user.getUuid(), token);
        LoginResponse responseBody = new LoginResponse(user.getUuid(), token);

        ResponseEntity<LoginResponse> responseEntity = new ResponseEntity<>(responseBody, headers, HttpStatus.OK);
        return responseEntity;
    }

    private HttpHeaders createHeaders(String userUuid, SessionToken token) {
        long maxAge = (token.getExpiresAt().getTime() - (new Date()).getTime()) / 1000;
        HttpHeaders headers = new HttpHeaders();


        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");

        String userCookie = formatCookie(COOKIE_USER_UUID, userUuid, maxAge, true, true, SameSite.Strict);
        headers.add(HttpHeaders.SET_COOKIE, userCookie);

        String tokenCookie = formatCookie(COOKIE_SESSION_TOKEN, token.getValue(), maxAge, true, true, SameSite.Strict);
        headers.add(HttpHeaders.SET_COOKIE, tokenCookie);

        return headers;
    }

    private enum SameSite {
        Strict,
        Lax,
        None,
    }

    private String formatCookie(String key, String value, Long maxAge, Boolean httpOnly, Boolean secure, SameSite sameSite) {
        String cookie = key + "=" + value;

        if (maxAge != null) {
            cookie += "; Max-Age=" + maxAge;
        }

        if (httpOnly != null && httpOnly) {
            cookie += "; HttpOnly";
        }

        if (secure != null && secure) {
            cookie += "; Secure";
        }

        if (sameSite != null) {
            cookie += "; SameSite=" + sameSite.toString();
        }

        return cookie;
    }


    private SessionToken generateSessionToken(User user) {
        SessionToken token = new SessionToken(user);
        user.addSessionToken(token);

        return token;
    }


    public void mergeAnonymousUser(String anonUuid, String targetUuid) throws UserMergeTargetDoesntExistException, UserMergeSourceDoesntExistException, UserMergeSourceIsRegisteredException {
        Optional<User> anonUser = userRepository.findById(anonUuid);

        if (anonUser.isEmpty()) {
            throw new UserMergeSourceDoesntExistException(anonUuid);
        }

        if (!anonUser.get().isAnonymous()) {
            throw new UserMergeSourceIsRegisteredException(anonUuid);
        }

        Optional<User> targetUser = userRepository.findById(targetUuid);

        if (targetUser.isEmpty()) {
            throw new UserMergeTargetDoesntExistException(targetUuid);
        }

        List<Player> players = anonUser.get().getPlayers();
        anonUser.get().setPlayers(new ArrayList<Player>());
        targetUser.get().addPlayers(players);
        for (Player player: players) {
            player.setUser(targetUser.get());
        }

        List<Room> rooms = anonUser.get().getCreatedRooms();
        anonUser.get().setCreatedRooms(new ArrayList<Room>());
        targetUser.get().addCreatedRooms(rooms);
        for (Room room: rooms) {
            room.setCreatedBy(targetUser.get());
        }

        playerRepository.saveAll(players);
        roomRepository.saveAll(rooms);
        userRepository.save(targetUser.get());
        Optional<JiraOAuth> jira = jiraOAuthRepository.findByUser(anonUser.get().getUuid());
        if (jira.isPresent()) {
            jiraOAuthRepository.delete(jira.get());
        }
        userRepository.delete(anonUser.get());
    }

    @PostMapping("/email_validity_check")
    public boolean checkEmailAvailability(@RequestParam("email") String email)
        throws EmailValidationException
    {
        Email validatedEmail = new Email(email);
        boolean maybeUnavailable = emailBloomFilter.test(validatedEmail);
        if (maybeUnavailable) {
            Iterator<User> iter = userRepository.findByEmail(email).iterator();
            boolean isAvailable = !iter.hasNext();

            if (!isAvailable) {
                emailBloomFilter.put(validatedEmail);
            }

            return isAvailable;
        } else {
            return true;
        }
    }

    @PostMapping("/request_password_reset")
    public void requestPasswordReset(@RequestParam("email") String email)
    throws UserNotFoundException, RuntimeException
    {
        User user = RestUtil.requireUserByEmail(userRepository, email);

        EmailPasswordAuthentication auth = user.getAuthMethod().asEmailPasswordAuthentication();
        if (auth == null) {
            throw new RuntimeException("Cannot reset password for this auth method");
        }

        auth.generateResetToken();
        auth = authRepository.save(auth);

        Optional<String> token = auth.getResetToken();

        if (token.isEmpty()) {
            throw new RuntimeException("No reset token has been generated");
        }
        

        try {
            emailService.sendPasswordResetEmail(user, token.get());
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
    }


    @PostMapping("/reset_password")
    public void resetPassword(@RequestParam("email") String email, @RequestParam("token") String resetToken, @RequestParam("new_password") String newPassword)
    throws UserNotFoundException, InvalidResetTokenException, ExpiredResetTokenException
    {
        User user = RestUtil.requireUserByEmail(userRepository, email);

        EmailPasswordAuthentication auth = user.getAuthMethod().asEmailPasswordAuthentication();
        if (auth == null) {
            throw new RuntimeException("Cannot reset password for this auth method");
        }

        AuthUtil.validateResetToken(auth, resetToken);

        auth.setPassword(newPassword);
        auth.removeResetToken();

        auth = authRepository.save(auth);
    }
}
