package com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter;

import com.google.common.hash.Funnel;
import com.google.common.hash.PrimitiveSink;

public enum EmailFunnel implements Funnel<Email> {
    INSTANCE;
    public void funnel(Email email, PrimitiveSink into) {
        into.putUnencodedChars(email.getEmail());
    }
}

