package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ObjectType extends JsonType {
    private Map<String, JsonType> properties = new TreeMap<String, JsonType>();
    private List<String> required = new ArrayList<String>();

    public ObjectType() {
        setType("object");
    }

    public Map<String, JsonType> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, JsonType> properties) {
        this.properties = properties;
    }

    public List<String> getRequired() {
        return required;
    }

    public void setRequired(List<String> required) {
        this.required = required;
    }

    public void addOptional(String name, JsonType type) {
        this.properties.put(name, type);
    }

    public void addRequired(String name, JsonType type) {
        this.required.add(name);
        this.properties.put(name, type);
    }
}
