package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Round;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Optional;

@RestController
@CrossOrigin(allowCredentials = "true")
public class RoundController {
	@Autowired
	@Lazy
	private RoomRepository roomRepository;

	@Autowired
	@Lazy
	private UserRepository userRepository;

	@Autowired
	@Lazy
	private RoundRepository roundRepository;

	@PostMapping("/room/{roomIdOrAccessToken}/round/start")
	public Round startRound(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid
	) {
		RestUtil.userHasPlayerInRoomOrCreatedTheRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
		Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));

		Round round = new Round();
		round.setInitiatorId(userUuid);
		round.setStartedAt(new Timestamp(System.currentTimeMillis()));
		round.setRoomId(room.getId());
		round.setGameId(room.getActiveGameId());

		if (room.getCurrentTaskId().isPresent()) {
			round.setTaskId(room.getCurrentTaskId().get());
		}

		roundRepository.save(round);

		room.setActiveRoundId(Optional.of(round.getId()));
		roomRepository.save(room);

		return round;
	}

	@PatchMapping("/room/{roomIdOrAccessToken}/round/end")
	public Round endRound(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid,
			@RequestBody Round update
	) {
		RestUtil.userHasPlayerInRoomOrCreatedTheRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
		Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));

		if (!room.getActiveRoundId().isPresent()) {
			throw new RoundNotFoundException(roomIdOrAccessToken);
		}

		Optional<Round> roundOptional = roundRepository.findById(room.getActiveRoundId().get());

		if (!roundOptional.isPresent() || !room.getId().equals(roundOptional.get().getRoomId())) {
			throw new RoundNotFoundException(roomIdOrAccessToken);
		}

		Round round = roundOptional.get();

		round.setRevealerId(userUuid);
		round.setRevealedAt(new Timestamp(System.currentTimeMillis()));
		round.setConsensus(update.getConsensus());

		room.setActiveRoundId(Optional.empty());

		roundRepository.save(round);
		roomRepository.save(room);

		return round;
	}
}
