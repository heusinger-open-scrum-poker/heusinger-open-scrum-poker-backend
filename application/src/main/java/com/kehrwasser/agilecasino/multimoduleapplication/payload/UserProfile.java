package com.kehrwasser.agilecasino.multimoduleapplication.payload;


public class UserProfile {
    private String email;

    public UserProfile(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
