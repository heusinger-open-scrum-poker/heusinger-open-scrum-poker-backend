package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotRegisteredException;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.ImportModalState;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.JoinedRoom;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserProfile;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.AuthRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository.RegisteredUserInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IUserService;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserPatch;

@RestController
@CrossOrigin(allowCredentials = "true")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    @Lazy
    private ObjectMapper objectMapper;

    @Autowired
    @Lazy
    private UserRepository userRepository;

    @Autowired
    @Lazy
    private AuthRepository authRepository;

    @GetMapping("/user/{userId}")
    public User getUser(@PathVariable String userId) throws UserNotFoundException {
        return userService.findUserByUuid(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @PostMapping("/user")
    public User postUser(@RequestParam(name = "invited_by_hash") Optional<String> invited_by_hash) {
        return userService.createUser(invited_by_hash);
    }

    @GetMapping("/user/{userId}/profile")
    public UserProfile getUserProfile(
        @PathVariable String userId,
        @RequestHeader(value = HttpHeaders.AUTHORIZATION) Optional<String> auth_header
    ) {
        User user = userService.findUserByUuid(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));

        return userService.findUserProfile(user)
            .orElseThrow(() -> new UserNotRegisteredException(userId));
    }

    @PatchMapping("/user/{userId}")
    public User patchUser(
        @PathVariable String userId,
        @RequestBody UserPatch patch
    ) {
        User user = userService.findUserByUuid(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));

        return userService.updateUser(user, patch);
    }

    @GetMapping("/admin/registeredUsers")
    public List<RegisteredUserInfo> getRegisteredUsers() {
        return userService.findRegisteredUsers();
    }

    @GetMapping("/user/{userId}/joinedRooms")
    public List<JoinedRoom> getJoinedRooms(
        @PathVariable String userId
    ) {
        User user = userService.findUserByUuid(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));

        return userService.findJoinedRooms(user);
    }

    @GetMapping("/user/{userId}/importModalState")
    public ImportModalState getImportModalState(
        @PathVariable String userId
    ) {
        User user = userService.findUserByUuid(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));

        return userService.getImportModalState(user)
            .orElse(null);
    }

    @PutMapping("/user/{userId}/importModalState")
    public void putImportModalState(
        @PathVariable String userId,
        @RequestBody Optional<ImportModalState> importModalState
    ) {
        User user = userService.findUserByUuid(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));

        userService.setImportModalState(user, importModalState);
    }
}
