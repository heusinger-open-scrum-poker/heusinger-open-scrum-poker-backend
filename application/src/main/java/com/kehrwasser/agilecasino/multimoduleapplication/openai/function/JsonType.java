package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

public abstract class JsonType {
    private String type = "";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
