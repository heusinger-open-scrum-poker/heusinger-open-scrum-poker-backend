package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImportModalState {
    public static class Metadata {
        private String modifiedAt;
        private HashMap<String, String> store;

        public String getModifiedAt() {
            return modifiedAt;
        }
        public void setModifiedAt(String modifiedAt) {
            this.modifiedAt = modifiedAt;
        }
        public HashMap<String, String> getStore() {
            return store;
        }
        public void setStore(HashMap<String, String> store) {
            this.store = store;
        }
    }

    private List<String> currentPath;
    private HashMap<String, Metadata> metadata;

    public List<String> getCurrentPath() {

        return currentPath;
    }
    public void setCurrentPath(List<String> currentPath) {
        this.currentPath = currentPath;
    }
    public HashMap<String, Metadata> getMetadata() {
        return metadata;
    }
    public void setMetadata(HashMap<String, Metadata> metadata) {
        this.metadata = metadata;
    }
}
