package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import java.util.List;

public class Pagination<T> {
    private Integer maxResults;
    private Integer startAt;
    private Integer total;
    private Boolean isLast;
    private List<T> values;
    public Integer getMaxResults() {
        return maxResults;
    }
    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }
    public Integer getStartAt() {
        return startAt;
    }
    public void setStartAt(Integer startAt) {
        this.startAt = startAt;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public Boolean getIsLast() {
        return isLast;
    }
    public void setIsLast(Boolean isLast) {
        this.isLast = isLast;
    }
    public List<T> getValues() {
        return values;
    }
    public void setValues(List<T> values) {
        this.values = values;
    }
}
