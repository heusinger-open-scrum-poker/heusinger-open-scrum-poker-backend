package com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueType {
    private String self;
    private String id;
    private String description;
    private String iconUrl;
    private String name;
    private String untranslatedName;
    private Boolean subtask;
    private Integer hierarchyLevel;
    public String getSelf() {
        return self;
    }
    public void setSelf(String self) {
        this.self = self;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getIconUrl() {
        return iconUrl;
    }
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUntranslatedName() {
        return untranslatedName;
    }
    public void setUntranslatedName(String untranslatedName) {
        this.untranslatedName = untranslatedName;
    }
    public Boolean isSubtask() {
        return subtask;
    }
    public void setSubtask(Boolean subtask) {
        this.subtask = subtask;
    }
    public Integer getHierarchyLevel() {
        return hierarchyLevel;
    }
    public void setHierarchyLevel(Integer hierarchyLevel) {
        this.hierarchyLevel = hierarchyLevel;
    }
}
