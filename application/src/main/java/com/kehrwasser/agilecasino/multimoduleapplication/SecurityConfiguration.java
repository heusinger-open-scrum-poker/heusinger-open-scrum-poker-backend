package com.kehrwasser.agilecasino.multimoduleapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    AgileCasinoConfiguration config;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .requestMatchers()
            .antMatchers("/admin/**", "/actuator/**")
            .and()
        .authorizeRequests()
            .antMatchers("/admin/**", "/actuator/**").hasRole("ADMIN")
            .and()
        .authorizeRequests()
            .anyRequest().permitAll()
            .and()
        .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
        UserDetails admin = User.builder()
            .username(config.getAdminUser())
            .password(config.getAdminPassword())
            .roles("ADMIN")
            .build();

        inMemoryUserDetailsManager.createUser(admin);

        return inMemoryUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
