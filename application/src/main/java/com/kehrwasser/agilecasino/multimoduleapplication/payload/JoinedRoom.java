package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;

public class JoinedRoom {
    private Room room;
    private Player player;
    private long numPlayers;
    public JoinedRoom(Room room, Player player, long numPlayers) {
        this.room = room;
        this.player = player;
        this.numPlayers = numPlayers;
    }
    public Room getRoom() {
        return this.room;
    }
    public Player getPlayer() {
        return this.player;
    }
    public long getNumPlayers() {
        return this.numPlayers;
    }
}

