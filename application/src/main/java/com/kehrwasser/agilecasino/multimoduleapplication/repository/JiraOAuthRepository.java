package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface JiraOAuthRepository extends CrudRepository<JiraOAuth, String> {
    @Query("from JiraOAuth jira where jira.user.uuid = :userUuid")
    Optional<JiraOAuth> findByUser(@Param("userUuid") String userUuid);

    @Query("from JiraOAuth jira where jira.state = :oauthState")
    Optional<JiraOAuth> findByState(@Param("oauthState") String oauthState);
}
