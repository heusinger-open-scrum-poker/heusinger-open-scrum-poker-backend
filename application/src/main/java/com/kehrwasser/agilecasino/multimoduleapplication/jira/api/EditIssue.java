package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.JiraUtil;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-issues/#api-rest-api-3-issue-issueidorkey-put
 */
public class EditIssue {

    private JiraOAuth jira;
    private String resourceId;
    private String issueIdOrKey;
    private String summary;
    private Optional<String> descriptionHtml;

    public EditIssue(JiraOAuth jira, String resourceId, String issueIdOrKey, String summary, Optional<String> descriptionHtml) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.issueIdOrKey = issueIdOrKey;
        this.summary = summary;
        this.descriptionHtml = descriptionHtml;
    }

    public static class EditIssueRequestBody {
        Update update;
        public Update getUpdate() {
            return update;
        }
        public void setUpdate(Update update) {
            this.update = update;
        }
    }

    public static class Update {
        List<UpdateOperation> summary;
        List<UpdateOperation> description;
        public List<UpdateOperation> getSummary() {
            return summary;
        }
        public void setSummary(List<UpdateOperation> summary) {
            this.summary = summary;
        }
        public List<UpdateOperation> getDescription() {
            return description;
        }
        public void setDescription(List<UpdateOperation> description) {
            this.description = description;
        }
    }

    public static abstract class UpdateOperation {}

    public static class StringSetOperation extends UpdateOperation {
        String set;
        public StringSetOperation(String set) {
            this.set = set;
        }
        public String getSet() {
            return set;
        }
        public void setSet(String set) {
            this.set = set;
        }
    }

    public static class NodeSetOperation extends UpdateOperation {
        JsonNode set;
        public NodeSetOperation(JsonNode set) {
            this.set = set;
        }
        public JsonNode getSet() {
            return set;
        }
        public void setSet(JsonNode set) {
            this.set = set;
        }
    }

    public void fetch() {
        EditIssueRequestBody body = makeBody();

        ObjectMapper om = new ObjectMapper();
        String jsonBody = null;
        try {
            jsonBody = om.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assert jsonBody != null;

        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/api/3/issue/"
                        + issueIdOrKey))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .PUT(HttpRequest.BodyPublishers.ofString(jsonBody))
            .build();
        
        Util.fetchHttp(request);
    }

    public EditIssueRequestBody makeBody() {
        EditIssueRequestBody body = new EditIssueRequestBody();

        Update update = new Update();
        List<UpdateOperation> summaryUpdate = new ArrayList<>();
        summaryUpdate.add(new StringSetOperation(summary));
        update.setSummary(summaryUpdate);

        List<UpdateOperation> descriptionUpdate = new ArrayList<>();
        update.setDescription(descriptionUpdate);
        if (descriptionHtml.isPresent()) {
            JsonNode adfNode = JiraUtil.adfNodeFromHtml(descriptionHtml.get());
            descriptionUpdate.add(new NodeSetOperation(adfNode));
        }
        body.setUpdate(update);

        return body;
    }
}
