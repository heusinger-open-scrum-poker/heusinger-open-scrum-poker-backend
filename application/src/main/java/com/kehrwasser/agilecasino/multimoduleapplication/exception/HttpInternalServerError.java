package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class HttpInternalServerError extends RuntimeException {
    public HttpInternalServerError() {
        super("Internal Server Error");
    }

    public HttpInternalServerError(String message) {
        super(message);
    }
}
