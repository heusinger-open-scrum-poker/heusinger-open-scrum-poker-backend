package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kehrwasser.agilecasino.multimoduleapplication.CardSeries;

import org.hibernate.annotations.CreationTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "room")
public class Room {

    static Logger logger = LoggerFactory.getLogger(Room.class);

    // TODO currently up to 15 players per room are supported
    public static final String[] AVATARS = {"darkblue", "orange", "darkred", "lightblue", "red", "rose", "yellow", "green", "black", "blue", "brown", "lilac", "neon", "pink", "turquoise" /*, "white" */ };

    public static final String BREAK_CARD_VALUE = "@break";

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @OneToMany(
        fetch = FetchType.EAGER,
        cascade = CascadeType.REMOVE,
        mappedBy = "room",
        orphanRemoval = true
    )
    @JsonIgnore
    private List<Player> players = new ArrayList<>();

    @Column(name = "status")
    private String status = "estimating";

    @Column(name = "max_players")
    private Integer maxPlayers = 0;

    @Column(name = "rounds_played")
    private Integer roundsPlayed = 0;

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private Timestamp created_at;

    @OneToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "created_by", nullable = true)
    @JsonIgnore
    private User created_by;

    private String series = CardSeries.FIBONACCI.toJSON();

    @OneToOne(
        fetch = FetchType.EAGER
    )
    @JoinColumn(name = "current_task", nullable = true)
    @JsonIgnore
    private Task currentTask;

    @Column(name = "access_token", nullable = true, columnDefinition = "TEXT")
    private String accessToken = null;

    @Column(name = "active_game_id")
    private Integer activeGameId;

    @Column(name = "active_round_id", nullable = true)
    @JsonIgnore
    private Integer activeRoundId;

    public void addPlayer(Player player) {

        players.add(player);

        if (player.getRoom() == null) {
            player.setRoom(this);
        }

    }

    @JsonIgnore
    public Room setEstimating() throws Exception {

        try {

            if (this.status.equals("estimating")) {
                throw new Exception("Already in `estimating`");
            }

            setStatus("estimating");

        } catch (Exception e) {
            throw new Exception("Already estimating");
        }

        return this;

    }

    @JsonIgnore
    public void setRevealed() throws Exception {

        if (this.status.equals("revealed")) {
            throw new Exception("Already in revealed");
        }

        this.setStatus("revealed");

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return this.players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) throws Exception {

        if (status.equals("estimating") || status.equals("revealed")) {
            this.status = status;
        } else {
            throw new Exception("Status not allowed");
        }

    }

    public Integer getMaxPlayers() {
        return this.maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Integer getRoundsPlayed() {
        return this.roundsPlayed;
    }

    public void setRoundsPlayed(Integer roundsPlayed) {
        this.roundsPlayed = roundsPlayed;
    }

    public Timestamp getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public String getSeries() {
        return this.series;
    }

    public void setSeries(String json) {
        try {
            CardSeries.parse(json);
            this.series = json;
        } catch (Exception e) {
            Room.logger.error(e.toString());
        }
    }

    @JsonIgnore
    public Optional<User> getCreatedBy() {
        if (this.created_by == null) {
            return Optional.empty();
        }

        return Optional.of(this.created_by);
    }

    public void setCreatedBy(User user) {
        this.created_by = user;
    }

    @JsonIgnore
    public Optional<Task> getCurrentTask() {
        if (this.currentTask == null) {
            return Optional.empty();
        }

        return Optional.of(this.currentTask);
    }

    public void setCurrentTask(Optional<Task> task) {
        if (task.isEmpty()) {
            this.currentTask = null;
        } else {
            this.currentTask = task.get();
        }
    }

    public Optional<Integer> getCurrentTaskId() {
        return getCurrentTask().map(task -> task.getId());
    }

    public Optional<String> getAccessToken() {
        if (this.accessToken == null) {
            return Optional.empty();
        } else {
            return Optional.of(this.accessToken);
        }
    }

    public void setAccessToken(Optional<String> accessToken) {
        if (accessToken.isEmpty()) {
            this.accessToken = null;
        } else {
            this.accessToken = accessToken.get();
        }
    }

    public void generateAccessToken() {
        this.accessToken = UUID.randomUUID().toString();
    }

    public boolean isLocked() {
        return this.getAccessToken().isPresent();
    }

    public Integer getActiveGameId() {
        return this.activeGameId;
    }

    public void setActiveGameId(Integer activeGameId) {
        this.activeGameId = activeGameId;
    }

    public Optional<Integer> getActiveRoundId() {
        return this.activeRoundId != null ? Optional.of(this.activeRoundId) : Optional.empty();
    }

    public void setActiveRoundId(Optional<Integer> activeRoundId) {
        this.activeRoundId = activeRoundId.orElse(null);
    }

    @Override
    public String toString() {
        List<Integer> playerIds = getPlayers().stream().map(player -> player.getId()).collect(Collectors.toList());
        return "{" +
                " id='" + getId() + "'" +
                ", players='" + playerIds + "'" +
                ", status='" + getStatus() + "'" +
                ", maxPlayers='" + getMaxPlayers() + "'" +
                ", roundsPlayed='" + getRoundsPlayed() + "'" +
                ", created_at='" + getCreated_at() + "'" +
                ", series=" + getSeries() +
                ", activeGameId=" + getActiveGameId() +
                "}";
    }

}
