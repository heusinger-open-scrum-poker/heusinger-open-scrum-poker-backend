package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class EmailInUseException extends RuntimeException {
    public EmailInUseException(String email) {
        super("email already in use: " + email);
    }
}
