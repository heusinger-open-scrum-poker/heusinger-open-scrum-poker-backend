package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;

public class SubtaskOperation {
    public static final String OPERATION_CREATE = "create";
    public static final String OPERATION_UPDATE = "update";
    public static final String OPERATION_DELETE = "delete";

    private String type;
    private Subtask subtask;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Subtask getSubtask() {
        return subtask;
    }

    public void setSubtask(Subtask subtask) {
        this.subtask = subtask;
    }

    public boolean isCreateType() {
        return getType().equals(SubtaskOperation.OPERATION_CREATE);
    }

    public boolean isUpdateType() {
        return getType().equals(SubtaskOperation.OPERATION_UPDATE);
    }

    public boolean isDeleteType() {
        return getType().equals(SubtaskOperation.OPERATION_DELETE);
    }
}
