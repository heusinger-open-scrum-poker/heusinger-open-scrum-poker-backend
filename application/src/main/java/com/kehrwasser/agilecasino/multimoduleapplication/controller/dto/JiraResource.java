package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

public class JiraResource {
    private String id;
    private String url;
    private String name;
    private String avatarUrl;
    public JiraResource(String id, String url, String name, String avatarUrl) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.avatarUrl = avatarUrl;
    }
    public String getName() {
        return name;
    }
    public String getId() {
        return id;
    }
    public String getUrl() {
        return url;
    }
    public String getAvatarUrl() {
        return avatarUrl;
    }
}
