package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "player")
public class Player {
    public static final String TYPE_ESTIMATOR = "estimator";
    public static final String TYPE_INSPECTOR = "inspector";

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "card_value")
    private String cardValue;

    @Column(name = "type")
    private String type;

    @ManyToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "room_id")
    @JsonIgnore
    private Room room;

    @ManyToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "user_uuid")
    @JsonIgnore
    private User user;

    public Player() {
    }

    public Player(Integer id, String name, String avatar, Room room, String type, User user) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.room = room;
        this.type = type;
        this.user = user;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {

        if ( ! type.equals("estimator")) {
            type = "inspector";
        }

        this.type = type;

    }

    public Room getRoom() {
        return this.room;
    }

    public void setRoom(Room room) {
        this.room = room;
        room.addPlayer(this);
    }

    public Optional<User> getUser() {
        if (this.user != null) {
            return Optional.of(this.user);
        } else {
            return Optional.empty();
        }
    }

    public void setUser(User user) {
        this.user = user;
        if (user != null) {
            user.addPlayer(this);
        }
    }

    public Optional<String> getCardValue() {
        if (this.cardValue == null) {
            return Optional.empty();
        }

        return Optional.of(this.cardValue);
    }

    public void setCardValue(Optional<String> cardValue) {
        if (cardValue == null || cardValue.isEmpty()) {
            this.cardValue = null;
            return;
        }

        this.cardValue = cardValue.get();
    }

    @Override
    public String toString() {
        String roomId = "undefined";
        if (getRoom() != null) {
            roomId = room.getId().toString();
        }
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", avatar='" + getAvatar() + "'" +
            ", cardValue='" + getCardValue() + "'" +
            ", room='" + roomId + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
