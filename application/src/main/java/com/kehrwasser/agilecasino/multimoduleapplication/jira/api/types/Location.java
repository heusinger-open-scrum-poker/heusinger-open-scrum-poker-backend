package com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {
    private String displayName;
    private String name;
    private Integer projectId;
    private String projectKey;
    private String projectName;
    private String projectTypeKey;
    private String userAccountId;
    private Integer userId;
    private String avatarURI;
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getProjectId() {
        return projectId;
    }
    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
    public String getProjectKey() {
        return projectKey;
    }
    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }
    public String getProjectName() {
        return projectName;
    }
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    public String getProjectTypeKey() {
        return projectTypeKey;
    }
    public void setProjectTypeKey(String projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
    }
    public String getUserAccountId() {
        return userAccountId;
    }
    public void setUserAccountId(String userAccountId) {
        this.userAccountId = userAccountId;
    }
    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public String getAvatarURI() {
        return avatarURI;
    }
    public void setAvatarURI(String avatarURI) {
        this.avatarURI = avatarURI;
    }
}
