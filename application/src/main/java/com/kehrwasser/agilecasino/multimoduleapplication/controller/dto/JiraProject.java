package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

public class JiraProject {
    private String id;
    private String key;
    private String url;
    private String avatarUrl;
    public JiraProject(String id, String key, String url, String avatarUrl) {
        this.id = id;
        this.key = key;
        this.url = url;
        this.avatarUrl = avatarUrl;
    }
    public String getId() {
        return id;
    }
    public String getKey() {
        return key;
    }
    public String getUrl() {
        return url;
    }
    public String getAvatarUrl() {
        return avatarUrl;
    }
}
