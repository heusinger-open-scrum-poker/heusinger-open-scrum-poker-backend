package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.random.RandomGenerator;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.EmailPasswordAuthentication;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.ExpiredResetTokenException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.InvalidResetTokenException;

public class AuthUtil {
    public static long EXPIRATION_DURATION_MS = 1000 * 60 * 60; // 1 hour

    public static String generatePasswordResetToken() {
        SecureRandom random = new SecureRandom();
        return generatePasswordResetToken(random);
    }

    public static String generatePasswordResetToken(RandomGenerator random) {
        int length = 32;
        String charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        StringBuilder token = new StringBuilder();

        for (int i = 0; i < length; i++) {
            char ch = charSet.charAt(random.nextInt(charSet.length()));
            token.append(ch);
        }

        return token.toString();
    }

    public static void validateResetToken(EmailPasswordAuthentication auth, String testToken) throws InvalidResetTokenException, ExpiredResetTokenException {
        if (auth.getResetToken().isEmpty() || auth.getResetTokenTimestamp().isEmpty()) {
            throw new InvalidResetTokenException();
        }

        String resetToken = auth.getResetToken().get();

        if (!resetToken.equals(testToken)) {
            throw new InvalidResetTokenException();
        }

        Timestamp timestamp = auth.getResetTokenTimestamp().get();
        Timestamp now = Timestamp.from(Instant.now());

        if (now.getTime() - timestamp.getTime() > EXPIRATION_DURATION_MS) {
            throw new ExpiredResetTokenException();
        }

        return;
    }
}
