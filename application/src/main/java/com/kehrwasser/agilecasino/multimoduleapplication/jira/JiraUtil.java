package com.kehrwasser.agilecasino.multimoduleapplication.jira;

import com.atlassian.adf.model.node.Doc;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.IssueType;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraSubtask;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.TaskMetadata;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.atlassian.adf.html.parser.HtmlParser;
import com.atlassian.adf.jackson1.AdfJackson1;

public class JiraUtil {
    public static Doc docFromHtml(String html) {
        HtmlParser htmlParser = new HtmlParser();
        Doc doc = htmlParser.unmarshall(html);

        return doc;
    }

    public static String jsonFromDoc(Doc doc) {
        AdfJackson1 parser = new AdfJackson1();
        String json = parser.marshall(doc);
        return json;
    }

    public static JsonNode nodeFromJson(String json) {
        ObjectMapper om = new ObjectMapper();

        try {
            JsonNode node = om.readValue(json, JsonNode.class);
            return node;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JsonNode adfNodeFromHtml(String html) {
        return nodeFromJson(jsonFromDoc(docFromHtml(html)));
    }

    public static IssueType determineTaskType(List<IssueType> issueTypes) {
        List<IssueType> taskTypes = issueTypes.stream().filter(type -> !type.isSubtask()).collect(Collectors.toList());

        {
            Optional<IssueType> exact = taskTypes.stream().filter(type -> type.getUntranslatedName().equals("Story")).findFirst(); if (exact.isPresent()) {
                return exact.get();
            }
        }

        {
            Optional<IssueType> heuristic = taskTypes.stream().filter(type -> type.getUntranslatedName().toLowerCase().contains("story")).findFirst();
            if (heuristic.isPresent()) {
                return heuristic.get();
            }
        }

        {
            Optional<IssueType> exact = taskTypes.stream().filter(type -> type.getUntranslatedName().equals("Task")).findFirst(); if (exact.isPresent()) {
                return exact.get();
            }
        }

        {
            Optional<IssueType> heuristic = taskTypes.stream().filter(type -> type.getUntranslatedName().toLowerCase().contains("task")).findFirst();
            if (heuristic.isPresent()) {
                return heuristic.get();
            }
        }

        {
            Optional<IssueType> any = taskTypes.stream().findFirst();
            if (any.isPresent()) {
                return any.get();
            }
        }

        throw new RuntimeException("task type could not be determined");
    }


    public static IssueType determineSubtaskType(List<IssueType> issueTypes) {
        List<IssueType> subtaskTypes = issueTypes.stream().filter(type -> type.isSubtask()).collect(Collectors.toList());

        {
            Optional<IssueType> exact = subtaskTypes.stream().filter(type -> type.getUntranslatedName().equals("Sub-Task")).findFirst(); if (exact.isPresent()) {
                return exact.get();
            }
        }

        {
            Optional<IssueType> heuristic = subtaskTypes.stream().filter(type -> type.getUntranslatedName().toLowerCase().contains("task")).findFirst();
            if (heuristic.isPresent()) {
                return heuristic.get();
            }
        }

        {
            Optional<IssueType> any = subtaskTypes.stream().findFirst();
            if (any.isPresent()) {
                return any.get();
            }
        }

        throw new RuntimeException("subtask type could not be determined");
    }

    public static Optional<TaskMetadata> parseTaskMetadata(String metadata) {
        ObjectMapper om = new ObjectMapper();
        
        try {
            TaskMetadata parsed = om.readValue(metadata, TaskMetadata.class);
            return Optional.of(parsed);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public static class SubtaskMetadata {
        private String resource;
        private String project;
        private String parentId;
        private JiraSubtask subtask;
        public String getResource() {
            return resource;
        }
        public void setResource(String resource) {
            this.resource = resource;
        }
        public String getProject() {
            return project;
        }
        public void setProject(String project) {
            this.project = project;
        }
        public String getParentId() {
            return parentId;
        }
        public void setParentId(String parentId) {
            this.parentId = parentId;
        }
        public JiraSubtask getSubtask() {
            return subtask;
        }
        public void setSubtask(JiraSubtask subtask) {
            this.subtask = subtask;
        }
    }

    public static Optional<SubtaskMetadata> parseSubtaskMetadata(String metadata) {
        ObjectMapper om = new ObjectMapper();

        try {
            SubtaskMetadata parsed = om.readValue(metadata, SubtaskMetadata.class);
            return Optional.of(parsed);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }


    public static Optional<TaskMetadata> getTaskMetadata(Task task) {
        if (task == null) {
            return Optional.empty();
        }

        return task.getMetadata().flatMap(m -> parseTaskMetadata(m));
    }

    public static Optional<SubtaskMetadata> getSubtaskMetadata(Subtask subtask) {
        if (subtask == null) {
            return Optional.empty();
        }

        String metadata = subtask.getMetadata();

        if (metadata == null) {
            return Optional.empty();
        }

        return parseSubtaskMetadata(metadata);
    }
}
