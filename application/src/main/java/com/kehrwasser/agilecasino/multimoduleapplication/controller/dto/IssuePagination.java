package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import com.fasterxml.jackson.databind.JsonNode;

public class IssuePagination {
    private String expand;
    private Integer startAt;
    private Integer maxResults;
    private Integer total;
    public IssuePagination() {}
    public String getExpand() {
        return expand;
    }
    public void setExpand(String expand) {
        this.expand = expand;
    }
    public Integer getStartAt() {
        return startAt;
    }
    public void setStartAt(Integer startAt) {
        this.startAt = startAt;
    }
    public Integer getMaxResults() {
        return maxResults;
    }
    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public JsonNode[] getIssues() {
        return issues;
    }
    public void setIssues(JsonNode[] issues) {
        this.issues = issues;
    }
    private JsonNode[] issues;
}
