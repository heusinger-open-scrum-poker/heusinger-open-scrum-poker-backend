package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "round")
public class Round {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name = "initiator_id", nullable = false)
    @JsonIgnore
    private String initiatorId;

    @Column(name = "revealer_id", nullable = false)
    @JsonIgnore
    private String revealerId;

    @Column(name = "task_id")
    @JsonIgnore
    private Integer taskId;

    @Column(name = "game_id", nullable = false)
    @JsonIgnore
    private Integer gameId;

    @Column(name = "room_id", nullable = false)
    @JsonIgnore
    private Integer roomId;

    @Column(name = "consensus")
    private String consensus;

    @Column(name = "started_at", nullable = false)
    @JsonIgnore
    private Timestamp startedAt;

    @Column(name = "revealed_at", nullable = false)
    @JsonIgnore
    private Timestamp revealedAt;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInitiatorId() {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId) {
        this.initiatorId = initiatorId;
    }

    public String getRevealerId() {
        return revealerId;
    }

    public void setRevealerId(String revealerId) {
        this.revealerId = revealerId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getConsensus() {
        return consensus;
    }

    public void setConsensus(String consensus) {
        this.consensus = consensus;
    }

    public Timestamp getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Timestamp startedAt) {
        this.startedAt = startedAt;
    }

    public Timestamp getRevealedAt() {
        return revealedAt;
    }

    public void setRevealedAt(Timestamp revealedAt) {
        this.revealedAt = revealedAt;
    }

    public String toString() {
        ObjectMapper om = new ObjectMapper()
                .registerModule(new Jdk8Module());

        try {
            return om.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            System.err.println(e);
            return "failed to serialize " + super.toString();
        }
    }
}
