package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

public class JiraProjectResponse {
    private String expand;
    private String self;
    private String id;
    private String key;
    private String name;
    private Map<String, String> avatarUrls;
    private String projectTypeKey;
    private boolean simplified;
    private String style;
    @JsonProperty("isPrivate")
    private boolean isPrivate;
    private String entityId;
    private String uuid;
    private JsonNode properties;
    public JiraProjectResponse() {}
    public String getExpand() {
        return expand;
    }
    public void setExpand(String expand) {
        this.expand = expand;
    }
    public String getSelf() {
        return self;
    }
    public void setSelf(String self) {
        this.self = self;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Map<String, String> getAvatarUrls() {
        return avatarUrls;
    }
    public void setAvatarUrls(Map<String, String> avatarUrls) {
        this.avatarUrls = avatarUrls;
    }
    public String getProjectTypeKey() {
        return projectTypeKey;
    }
    public void setProjectTypeKey(String projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
    }
    public boolean isSimplified() {
        return simplified;
    }
    public void setSimplified(boolean simplified) {
        this.simplified = simplified;
    }
    public String getStyle() {
        return style;
    }
    public void setStyle(String style) {
        this.style = style;
    }
    public boolean isPrivate() {
        return isPrivate;
    }
    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
    public JsonNode getProperties() {
        return properties;
    }
    public void setProperties(JsonNode properties) {
        this.properties = properties;
    }
    public String getEntityId() {
        return entityId;
    }
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
    public String getUuid() {
        return uuid;
    }
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
