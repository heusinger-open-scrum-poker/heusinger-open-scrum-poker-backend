package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class RoomNotFoundException extends RuntimeException {
    public RoomNotFoundException(Integer id) {
        super("room not found: id=" + id);
    }

    public RoomNotFoundException(String id) {
        super("room not found: id=" + id);
    }
}
