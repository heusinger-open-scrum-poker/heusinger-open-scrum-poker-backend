package com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank;

import com.github.pravin.raha.lexorank4j.LexoRank;
import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class Rankable {
    public abstract String getRank();

    public abstract void setRank(String rank);

    @JsonIgnore
    public LexoRank getLexoRank() {
        String rank = this.getRank();

        if (rank == null) {
            return null;
        }

        try {
            return LexoRank.parse(rank);
        } catch (Exception e) {
            System.err.println("Invalid LexoRank " + rank);
            System.err.println(e.toString());
            return null;
        }
    }

    @JsonIgnore
    public void setLexoRank(LexoRank lexoRank) {
        if (lexoRank == null) {
            this.setRank(null);
        } else {
            this.setRank(lexoRank.toString());
        }
    }
}
