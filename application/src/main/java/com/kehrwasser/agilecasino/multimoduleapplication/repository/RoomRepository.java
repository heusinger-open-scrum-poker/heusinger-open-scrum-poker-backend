package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface RoomRepository extends CrudRepository<Room, Integer> {

    @Query(
        value = "SELECT avatar FROM player WHERE room_id = :roomId AND type='estimator'",
        nativeQuery = true
    )
    public List<String> findUsedAvatarsByRoomId(@Param("roomId") Integer roomId);

    @Query("from Room room where room.accessToken=:accessToken")
    public Optional<Room> findByAccessToken(@Param("accessToken") String accessToken);

}
