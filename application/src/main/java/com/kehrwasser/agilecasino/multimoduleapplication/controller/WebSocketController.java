package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.PlayerRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    Logger logger = LoggerFactory.getLogger(WebSocketController.class);

    @Autowired
    @Lazy
    private PlayerRepository playerRepository;

    @Autowired
    @Lazy
    private RoomRepository roomRepository;

    @Autowired
    @Lazy
    private RoomController roomController;

    @Autowired
    private IWebSocketSender sender;

    @MessageMapping("/channel/{roomIdOrAccessToken}/play")
    public void play(@DestinationVariable String roomIdOrAccessToken, @Payload Player updatedPlayer) {

        Player storedPlayer = playerRepository.findById(updatedPlayer.getId()).get();

        storedPlayer.setCardValue(updatedPlayer.getCardValue());
        playerRepository.save(storedPlayer);

        try {

            sender.emitPlayerUpdated(storedPlayer.getRoom(), storedPlayer);

        } catch (MessagingException e) {
            logger.error(e.toString());
        }

    }

    @MessageMapping("/channel/{roomIdOrAccessToken}/reveal")
    public void reveal(@DestinationVariable String roomIdOrAccessToken) {

        try {

            Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.empty());
            roomController.reveal(room, Optional.empty());

        } catch (RuntimeException e) {
            logger.error(e.toString());
        }

    }

    @MessageMapping("/channel/{roomIdOrAccessToken}/reset")
    public void reset(@DestinationVariable String roomIdOrAccessToken) {

        try {

            Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.empty());
            roomController.reset(room, Optional.empty());

        } catch (Exception e) {
            logger.error(e.toString());
        }

    }
}
