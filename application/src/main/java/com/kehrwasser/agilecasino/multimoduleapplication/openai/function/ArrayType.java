package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

public class ArrayType extends JsonType {
    private JsonType items = null;

    public ArrayType() {
        setType("array");
    }

    public JsonType getItems() {
        return items;
    }

    public void setItems(JsonType items) {
        this.items = items;
    }
}
