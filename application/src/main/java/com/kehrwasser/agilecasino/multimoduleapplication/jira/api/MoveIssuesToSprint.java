package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.List;


public class MoveIssuesToSprint {

    private JiraOAuth jira;
    private String resourceId;
    private Integer sprintId;
    private List<String> issueKeys;

    public MoveIssuesToSprint(JiraOAuth jira, String resourceId, Integer sprintId, List<String> issueKeys) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.sprintId = sprintId;
        this.issueKeys = issueKeys;
    }


    public static class MoveIssueToSprintRequestBody {
        private List<String> issues;
        public List<String> getIssues() {
            return issues;
        }
        public void setIssues(List<String> issues) {
            this.issues = issues;
        }
    }


    public void fetch() {
        MoveIssueToSprintRequestBody body = makeBody();

        ObjectMapper om = new ObjectMapper();
        String jsonBody;
        try {
            jsonBody = om.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            jsonBody = "{\"error\":\"in createIssue\"}";
            
            e.printStackTrace();
        }

        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/agile/1.0/sprint/"
                        + sprintId
                        + "/issue"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .POST(HttpRequest.BodyPublishers.ofString(jsonBody))
            .build();
        
        Util.fetchHttp(request);
    }

    public MoveIssueToSprintRequestBody makeBody() {
        MoveIssueToSprintRequestBody body = new MoveIssueToSprintRequestBody();
        body.setIssues(this.issueKeys);
        return body;
    }
}
