package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

public class StringType extends JsonType {
    private String description = "";

    public StringType() {
        this.setType("string");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
