package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.OpenaiUsage;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.OpenaiApi;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.SubtaskEstimate;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.OpenaiApi.GenerateSubtasksResult;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.functionResponse.Usage;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.OpenaiUsageRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;

@RestController
@CrossOrigin(allowCredentials = "true")
public class AiSubtaskController {

    public static final Integer MAX_FREE_AI_ESTIMATES = 5;

    @Autowired
    OpenaiApi api;

    @Autowired
    UserRepository userRepository;

    @Autowired
    OpenaiUsageRepository openaiUsageRepository;

    @PostMapping("/ai/subtaskEstimate")
    public List<SubtaskEstimate> postSubtaskEstimate(
            @RequestParam("summary") String summary,
            @RequestParam("user_uuid") String userUuid)
    {
        User user = RestUtil.requireNonAnonymousUser(userRepository, userUuid);

        RestUtil.requireAvailableAiEstimates(openaiUsageRepository, user);

        GenerateSubtasksResult result = api.generateSubtasks(summary);

        Usage usage = result.getUsage();

        OpenaiUsage persistedUsage = new OpenaiUsage();
        persistedUsage.setUser(user);
        persistedUsage.setModel(result.getModel());
        persistedUsage.setTotalTokens(usage.getTotal_tokens());
        persistedUsage.setPromptTokens(usage.getPrompt_tokens());
        persistedUsage.setCompletionTokens(usage.getCompletion_tokens());
        

        openaiUsageRepository.save(persistedUsage);

        return result.getSubtasks();
    }

    public static class QuotaResponse {
        private Integer freeAiEstimations;

        public Integer getFreeAiEstimations() {
            return freeAiEstimations;
        }

        public void setFreeAiEstimations(Integer freeAiEstimations) {
            this.freeAiEstimations = freeAiEstimations;
        }
    }

    @GetMapping("/ai/quota")
    public QuotaResponse getQuota(
            @RequestParam("user_uuid") String userUuid)
    {
        User user = RestUtil.requireNonAnonymousUser(userRepository, userUuid);

        if (user.getRole().equals(User.ROLE_SIGNEDUP)) {
            Integer freeAiEstimations = Math.max(0, MAX_FREE_AI_ESTIMATES - openaiUsageRepository.getNumberOfEstimatesForUser(user.getUuid()));

            QuotaResponse response = new QuotaResponse();
            response.setFreeAiEstimations(freeAiEstimations);

            return response;
        }

        QuotaResponse response = new QuotaResponse();
        response.setFreeAiEstimations(null);
        return response;
    }
}
