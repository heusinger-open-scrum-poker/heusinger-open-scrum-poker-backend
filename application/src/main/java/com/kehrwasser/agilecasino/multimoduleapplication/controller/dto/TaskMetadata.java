package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

public class TaskMetadata {
    private String resource;
    private String project;
    private JiraIssue issue;
    public String getResource() {
        return resource;
    }
    public void setResource(String resource) {
        this.resource = resource;
    }
    public String getProject() {
        return project;
    }
    public void setProject(String project) {
        this.project = project;
    }
    public JiraIssue getIssue() {
        return issue;
    }
    public void setIssue(JiraIssue issue) {
        this.issue = issue;
    }
}
