package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kehrwasser.agilecasino.multimoduleapplication.exception.HttpStatusLockedException;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.RepositoryUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PermissionDeniedException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PlayerNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.KickInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.LeaveInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.PlayerPatch;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.PromotionInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.RoomChange;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.SideliningInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IPlayerService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IUserService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;

@RestController
@CrossOrigin(allowCredentials = "true")
public class PlayerController {

    Logger logger = LoggerFactory.getLogger(PlayerController.class);

    @Autowired
    private IPlayerService playerService;

    @Autowired
    @Lazy
    private RoomRepository roomRepository;

    @Autowired
    private IUserService userService;

    @Autowired
    private IWebSocketSender sender;

    @GetMapping("/players/{playerId}")
    public Player readPlayer(@PathVariable String playerId) throws PlayerNotFoundException {

        return playerService.findPlayerById(Integer.parseInt(playerId))
            .orElseThrow(() -> new PlayerNotFoundException(playerId));

    }

    @Deprecated
    @PutMapping("/players/{playerId}")
    public Player putPlayer(@PathVariable String playerId,
            @RequestBody PlayerPatch patch,
            @RequestParam(name = "user_uuid") Optional<String> _userUuid) throws PlayerNotFoundException {

        String userUuid = _userUuid.orElseThrow(() -> new PermissionDeniedException("no user_uuid given"));
        Player player = playerService.findPlayerById(Integer.parseInt(playerId))
            .orElseThrow(() -> new PlayerNotFoundException(playerId));

        if (player.getUser().isEmpty()) {
            userService.findUserByUuid(userUuid)
                .ifPresent(user -> player.setUser(user));
        }

        boolean allowed = player.getUser().map(u -> u.getUuid().equals(userUuid)).orElse(false);
        if (!allowed) {
            throw new PermissionDeniedException("Not permitted to change player with id=" + playerId + ".");
        }

        if (patch.getCardValue() != null) {
            player.setCardValue(patch.getCardValue());
        }

        if (patch.getName() != null && RepositoryUtil.isAvailableName(patch.getName(), player.getRoom())) {
            player.setName(patch.getName());
        }

        if (patch.getAvatar() != null && RepositoryUtil.isAvailableAvatar(roomRepository, patch.getAvatar(), player.getRoom())) {
            player.setAvatar(patch.getAvatar());
        }

        // update type
        if (patch.getType() != null) {
            updatePlayerType(player, patch);
        }

        playerService.savePlayer(player);

        sendChangedEventToPeers(player);

        return player;
    }

    @PatchMapping("/players/{playerId}")
    public Player patchPlayer(@PathVariable String playerId,
            @RequestBody PlayerPatch patch,
            @RequestParam(name = "user_uuid") Optional<String> _userUuid) throws PlayerNotFoundException {

        String userUuid = _userUuid.orElseThrow(() -> new PermissionDeniedException("no user_uuid given"));
        Player player = playerService.findPlayerById(Integer.parseInt(playerId))
            .orElseThrow(() -> new PlayerNotFoundException(playerId));

        if (player.getUser().isEmpty()) {
            Optional<User> user = userService.findUserByUuid(userUuid);
            if (user.isPresent()) {
                player.setUser(user.get());
            }
        }

        boolean allowed = player.getUser().map(u -> u.getUuid().equals(userUuid)).orElse(false);
        if (!allowed) {
            throw new PermissionDeniedException("Not permitted to change player with id=" + playerId + ".");
        }

        if (patch.getCardValue() != null) {
            player.setCardValue(patch.getCardValue());
        }

        if (patch.getName() != null && RepositoryUtil.isAvailableName(patch.getName(), player.getRoom())) {
            player.setName(patch.getName());
        }

        if (patch.getAvatar() != null && RepositoryUtil.isAvailableAvatar(roomRepository, patch.getAvatar(), player.getRoom())) {
            player.setAvatar(patch.getAvatar());
        }

        // update type
        if (patch.getType() != null) {
            updatePlayerType(player, patch);
        }

        playerService.savePlayer(player);

        sendChangedEventToPeers(player);

        return player;
    }

    void updatePlayerType(Player storedPlayer, PlayerPatch patch) {
            String updatedType = patch.getType();

            boolean isRejoiningTable = patch.getType().equals("estimator") && ! storedPlayer.getType().equals("estimator");

            if (isRejoiningTable) {

                Set<String> avatars = RepositoryUtil.getAvailableAvatars(roomRepository, storedPlayer.getRoom());
                boolean isPreferredAvatarAvailable = avatars.contains(patch.getAvatar());

                if (!isPreferredAvatarAvailable) {

                    String newAvatar = RepositoryUtil.getAvailableAvatar(roomRepository, storedPlayer.getRoom());

                    if (newAvatar == null) {
                        throw new HttpStatusLockedException();
                    } else {
                        storedPlayer.setAvatar(newAvatar);

                    }
                }

            }

            try {
                storedPlayer.setType(updatedType);
            } catch (Exception e) {
                logger.error(e.toString());
            }
    }

    public void sendChangedEventToPeers(Player player) {

        sender.emitRoomChanged(player.getRoom(), RoomChange.playerChange(player));

    }

    public void sendLeftEventToPeers(Player player) {

        LeaveInfo info = new LeaveInfo(player);
        sender.emitPlayerLeft(player.getRoom(), info);

    }

    public void sendKickEventToPeers(Player player, Player initiatingPlayer) {

        KickInfo info = new KickInfo(player, initiatingPlayer);
        sender.emitPlayerKicked(player.getRoom(), info);

    }

    public void sendSideliningEventToPeers(Player player, Player initiatingPlayer) {
        SideliningInfo info = new SideliningInfo(player, initiatingPlayer);
        sender.emitPlayerSidelined(player.getRoom(), info);
    }

    public void sendPromotingEventToPeers(Player player, Player initiatingPlayer) {
        PromotionInfo info = new PromotionInfo(player, initiatingPlayer);
        sender.emitPlayerPromoted(player.getRoom(), info);
    }

    @DeleteMapping("/players/{playerId}")
    public void deletePlayer(@PathVariable String playerId, @RequestParam(name = "user_uuid") Optional<String> _userUuid) throws PermissionDeniedException, PlayerNotFoundException {
        String userUuid = RestUtil.mandatory(_userUuid, "no user_uuid given");
        Player player = playerService.findPlayerById(Integer.parseInt(playerId))
            .orElseThrow(() -> new PlayerNotFoundException(playerId));

        Room room = player.getRoom();

        Player initiatingPlayer = Util.findPlayerOfUser(room, userUuid)
            .orElseThrow(() -> new PermissionDeniedException("Kicking player with id=" + playerId + " not allowed."));

        playerService.deletePlayer(player);

        if (initiatingPlayer.getId().equals(player.getId())) {
            sendLeftEventToPeers(player);
        } else {
            sendKickEventToPeers(player, initiatingPlayer);
        }
    }

    @PostMapping("/players/{playerId}/sideline")
    public void sidelinePlayer(@PathVariable String playerId, @RequestParam(name = "user_uuid") Optional<String> _userUuid) throws PermissionDeniedException, PlayerNotFoundException {
        String userUuid = _userUuid.orElseThrow(() -> new PermissionDeniedException("no user_uuid given"));
        Player player = playerService.findPlayerById(Integer.parseInt(playerId))
            .orElseThrow(() -> new PlayerNotFoundException(playerId));

        if (player.getType().equals(Player.TYPE_INSPECTOR)) {
            return;
        }

        Room room = player.getRoom();

        Player initiatingPlayer = Util.findPlayerOfUser(room, userUuid)
            .orElseThrow(() -> new PermissionDeniedException("Sidelining player with id=" + playerId + " not allowed."));

        player.setType(Player.TYPE_INSPECTOR);
        playerService.savePlayer(player);

        boolean isSelfSidelining = initiatingPlayer.getId().equals(player.getId());
        if (!isSelfSidelining) {
            sendSideliningEventToPeers(player, initiatingPlayer);
        }
    }

    @PostMapping("/players/{playerId}/promote")
    public void promotePlayer(@PathVariable String playerId, @RequestParam(name = "user_uuid") Optional<String> _userUuid) throws PermissionDeniedException, PlayerNotFoundException {
        String userUuid = _userUuid.orElseThrow(() -> new PermissionDeniedException("no user_uuid given"));
        Player player = playerService.findPlayerById(Integer.parseInt(playerId))
            .orElseThrow(() -> new PlayerNotFoundException(playerId));

        if (player.getType().equals(Player.TYPE_ESTIMATOR)) {
            return;
        }

        Room room = player.getRoom();

        Player initiatingPlayer = Util.findPlayerOfUser(room, userUuid)
            .orElseThrow(() -> new PermissionDeniedException("Promoting player with id=" + playerId + " not allowed."));

        String avatar = playerService.findFreeAvatar(player)
            .orElseThrow(() -> new PermissionDeniedException("no avatar available"));
        player.setAvatar(avatar);
        player.setType(Player.TYPE_ESTIMATOR);
        playerService.savePlayer(player);

        boolean isSelfSidelining = initiatingPlayer.getId().equals(player.getId());
        if (!isSelfSidelining) {
            sendPromotingEventToPeers(player, initiatingPlayer);
        }
    }
}
