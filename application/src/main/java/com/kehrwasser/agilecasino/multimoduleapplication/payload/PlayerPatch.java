package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import java.util.Optional;

public class PlayerPatch {
    private Integer id;
    private String name;
    private String avatar;
    private Optional<String> cardValue;
    private String type;

    public PlayerPatch() {}

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Optional<String> getCardValue() {
        return this.cardValue;
    }

    public void setCardValue(Optional<String> cardValue) {
        this.cardValue = cardValue;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
