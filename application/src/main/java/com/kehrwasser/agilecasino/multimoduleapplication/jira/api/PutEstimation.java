package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;

import java.net.URI;
import java.net.http.HttpRequest;


public class PutEstimation {

    private JiraOAuth jira;
    private String resourceId;
    private String boardId;
    private String issueIdOrKey;
    private String estimation;

    public PutEstimation(JiraOAuth jira, String resourceId, String boardId, String issueIdOrKey, String estimation) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.boardId = boardId;
        this.issueIdOrKey = issueIdOrKey;
        this.estimation = estimation;
    }

    public static class PutEstimationRequestBody {
        private String value;
        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class PutEstimationResponse {
        private String fieldId;
        private String value;
        public String getFieldId() {
            return fieldId;
        }
        public void setFieldId(String fieldId) {
            this.fieldId = fieldId;
        }
        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }
    }

    public PutEstimationResponse fetch() {
        PutEstimationRequestBody body = makeBody();

        ObjectMapper om = new ObjectMapper();
        String jsonBody;
        try {
            jsonBody = om.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            jsonBody = "{\"error\":\"in createIssue\"}";
            
            e.printStackTrace();
        }

        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/agile/1.0/issue/"
                        + issueIdOrKey
                        + "/estimation?boardId="
                        + boardId))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .PUT(HttpRequest.BodyPublishers.ofString(jsonBody))
            .build();

        PutEstimationResponse resp = Util.fetchJson(om, request, new TypeReference<PutEstimationResponse>() {});
        return resp;
    }

    public PutEstimationRequestBody makeBody() {
        PutEstimationRequestBody body = new PutEstimationRequestBody();
        body.setValue(estimation);
        return body;
    }
}
