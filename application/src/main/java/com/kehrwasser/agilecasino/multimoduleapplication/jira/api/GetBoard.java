package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.Board;

import java.net.URI;
import java.net.http.HttpRequest;


public class GetBoard {

    private JiraOAuth jira;
    private String resourceId;
    private String boardId;

    public GetBoard(JiraOAuth jira, String resourceId, String boardId) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.boardId = boardId;
    }

    public Board fetch() {
        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/agile/1.0/board/"
                        + boardId))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();


        ObjectMapper om = new ObjectMapper();
        Board resp = Util.fetchJson(om, request, new TypeReference<Board>() {});
        return resp;
    }
}

