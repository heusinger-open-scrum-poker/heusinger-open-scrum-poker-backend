package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kehrwasser.agilecasino.multimoduleapplication.AvatarAvailability;
import com.kehrwasser.agilecasino.multimoduleapplication.CardSeries;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.RepositoryUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Game;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.InvalidSeriesJsonException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PermissionDeniedException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PlayerNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.RoomNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UnavailablePlayerNameException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.RevealInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.RoomChange;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.GameRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.PlayerRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.TaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;

@RestController
@CrossOrigin(allowCredentials = "true")
public class RoomController {

    Logger logger = LoggerFactory.getLogger(RoomController.class);

    @Autowired
    @Lazy
    private RoomRepository roomRepository;

    @Autowired
    @Lazy
    private PlayerRepository playerRepository;

    @Autowired
    @Lazy
    private UserRepository userRepository;

    @Autowired
    private IWebSocketSender sender;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private GameController gameController;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/room/{roomIdOrAccessToken}")
    public Room getRoom(
            @PathVariable String roomIdOrAccessToken,
            @RequestParam("user_uuid") Optional<String> userUuid
            ) throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException, UserNotFoundException
    {
        Room room = RestUtil.requireRoom(this.roomRepository, roomIdOrAccessToken, userUuid);

        if (room.getSeries() == null) {
            room.setSeries(CardSeries.FIBONACCI.toJSON());
            room = roomRepository.save(room);
        }

        if (room.getActiveGameId() == null) {
            Optional<Task> currentTask = room.getCurrentTask();

            Game game = new Game();
            game.setName("Game #1");
            game = gameController.createGame(roomIdOrAccessToken, userUuid, game);
            game = gameRepository.save(game);

            room.setActiveGameId(game.getId());
            room.setCurrentTask(currentTask);
            room = roomRepository.save(room);
        }

        Iterable<Task> tasks = taskRepository.findAllByRoomId(room.getId());
        for (Task task: tasks) {
            if (task.getGameId() == null) {
                task.setGameId(room.getActiveGameId());
                taskRepository.save(task);
            }
        }

        return room;
    }

    @PostMapping(path="/room")
    public Room createNewRoom(@RequestParam("user_uuid") Optional<String> userUuid) {

        Optional<User> user = userUuid.flatMap(uuid -> userRepository.findById(uuid));
        Room room = new Room();

        if (user.isPresent()) {
            room.setCreatedBy(user.get());
        }
        roomRepository.save(room);

        Game game = new Game();
        game.setName("Game #1");
        game = gameController.createGame(room.getId().toString(), userUuid, game);
        gameRepository.save(game);

        room.setActiveGameId(game.getId());
        roomRepository.save(room);

        return room;

    }

    @GetMapping("/room/{roomIdOrAccessToken}/players")
    public List<Player> getPlayersOfRoom(@PathVariable String roomIdOrAccessToken, @RequestParam("user_uuid") Optional<String> userUuid) throws RoomNotFoundException {

        return getRoom(roomIdOrAccessToken, userUuid).getPlayers();

    }

    /* expresses the event, when a new player joined the room */
    public void sendJoinEventToPeers(Player player) {

        sender.emitPlayerJoined(player.getRoom(), player);

    }

    public void reset(Room room, Optional<Player> resettingPlayer) throws RuntimeException {

        try {

            room.setEstimating();

            List<Player> players = room.getPlayers();

            players.forEach(player -> {
                Optional<String> cardValue = player.getCardValue();
                if (cardValue.isPresent() && cardValue.get().equals(Room.BREAK_CARD_VALUE)) {
                    return;
                }
                player.setCardValue(Optional.empty());
            });

            playerRepository.saveAll(players);
            roomRepository.save(room);

            sender.emitRoomReset(room, resettingPlayer);

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    public void reveal(Room room, Optional<Player> revealingPlayer) throws RuntimeException {

        try {

            room.setRevealed();
            room.setRoundsPlayed(room.getRoundsPlayed() + 1);
            roomRepository.save(room);

            RevealInfo info = new RevealInfo(revealingPlayer);

            sender.emitRoomRevealed(room, info);

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    @PutMapping(path="/room/{roomIdOrAccessToken}")
    public Room updateRoom(
            @PathVariable String roomIdOrAccessToken,
            @RequestBody Room roomUpdate,
            @RequestParam(name = "user_uuid") Optional<String> _userUuid) throws RoomNotFoundException, RuntimeException {

        String userUuid = RestUtil.mandatory(_userUuid, "no user_uuid given");
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, _userUuid);

        Optional<User> user = userRepository.findById(userUuid);
        Optional<Player> player = Optional.empty();

        boolean userlessPlayerExists = room.getPlayers().stream()
            .filter(p -> p.getUser().isEmpty())
            .findFirst().isPresent();
        player = room.getPlayers().stream()
            .filter(p -> p.getUser().map(u -> u.getUuid().equals(user.get().getUuid())).orElse(false))
            .findFirst();

        if (!userlessPlayerExists && player.isEmpty()) {
            throw new PermissionDeniedException("Changing room not allowed.");
        }

        if (roomUpdate.getId() != null && !roomUpdate.getId().equals(room.getId())) {
            throw new RuntimeException("Room ID inconsistent (" + room.getId() + ")");
        }

        if (!roomUpdate.getRoundsPlayed().equals(room.getRoundsPlayed())) {
            throw new RuntimeException("Rounds not matching");
        }

        if (!roomUpdate.getStatus().equals(room.getStatus())) {
            String newStatus = roomUpdate.getStatus();

            if (newStatus.equals("revealed")) {
                reveal(room, player);
            } else if (newStatus.equals("estimating")) {
                reset(room, player);
            }
        }

        return room;
    }

    @PostMapping("/room/{roomIdOrAccessToken}/players")
    public Player createPlayersOfRoom(
        @PathVariable String roomIdOrAccessToken, @RequestBody Player player, @RequestParam(name = "user_uuid") Optional<String> _userUuid)
        throws UnavailablePlayerNameException {

        String userUuid = RestUtil.mandatory(_userUuid, "no user_uuid given");
        Room room = RestUtil.requireRoom(this.roomRepository, roomIdOrAccessToken, _userUuid);
        Optional<User> user = this.userRepository.findById(userUuid);

        if (player.getId() != null) {
            Optional<Player> storedPlayer = playerRepository.findById(player.getId());
            storedPlayer = rejoinWithNewName(storedPlayer, player.getName(), room);

            if (storedPlayer.isPresent()) {
                return storedPlayer.get();
            }
        }

        if (!RepositoryUtil.isAvailableName(player.getName(), room)) {
            throw new UnavailablePlayerNameException(player.getName());
        }

        player.setRoom(room);
        if (user.isPresent()) {
            player.setUser(user.get());
        }

        try {
            player = savePlayerWithAvatar(room.getId(), player);

            room.setMaxPlayers(room.getMaxPlayers() + 1);
            roomRepository.save(room);
            if (user.isPresent()) {
                userRepository.save(user.get());
            }
        } catch (Exception e) {
            logger.error(e.toString());
            throw new RuntimeException(e);
        }

        sendJoinEventToPeers(player);

        return player;

    }

    public Optional<Player> rejoinWithNewName(Optional<Player> storedPlayer, String newName, Room room) {
        if (storedPlayer.isPresent() && !newName.equals(storedPlayer.get().getName()) && RepositoryUtil.isAvailableName(newName, room)) {
            Player player = storedPlayer.get();

            player.setName(newName);

            RoomChange change = RoomChange.nameChange(player);

            sender.emitRoomChanged(room, change);

            storedPlayer = Optional.of(playerRepository.save(player));
        }

        return storedPlayer;
    }

    public Player savePlayerWithAvatar(Integer roomId, Player player) {

        if (player.getType() != null && player.getType().equals("inspector")) {
            player.setAvatar(null);
            return playerRepository.save(player);
        }

        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);

        return transactionTemplate.execute(status -> {
            List<String> usedAvatars = roomRepository.findUsedAvatarsByRoomId(roomId);
            Iterator<String> availableAvatars = RepositoryUtil.getAvailableAvatars(roomRepository, usedAvatars).iterator();

            String avatar = null;
            if (availableAvatars.hasNext()) {
                avatar = availableAvatars.next();
            }

            player.setAvatar(avatar);

            if (avatar == null) {
                player.setType("inspector");
            } else {
                player.setType("estimator");
            }


            return playerRepository.save(player);
        });
    }

    @GetMapping("/room/{roomId}/series")
    public String getSeries(@PathVariable String roomId, @RequestParam("user_uuid") Optional<String> userUuid) throws RoomNotFoundException {
        return getRoom(roomId, userUuid).getSeries();
    }

    @PutMapping("/room/{roomId}/series")
    public String putSeries(@PathVariable String roomId, @RequestBody String seriesJson,
            @RequestParam(name = "playerId") Optional<Integer> _playerId,
            @RequestParam(name = "user_uuid") Optional<String> _userUuid) throws RoomNotFoundException, InvalidSeriesJsonException {

        String userUuid = RestUtil.mandatory(_userUuid, "no user_uuid given");
        Integer playerId = RestUtil.mandatory(_playerId, "no playerId given");

        Room room = getRoom(roomId, _userUuid);
        Optional<Player> player = playerRepository.findById(playerId);

        Optional<User> user = userRepository.findById(userUuid);

        boolean access = user.isPresent()
            && player.isPresent()
            && player.get().getUser().map(u -> u.getUuid().equals(user.get().getUuid())).orElse(false);

        if (!access) {
            return room.getSeries();
        }

        try {
            CardSeries.parse(seriesJson);
        } catch (Exception e) {
            throw new InvalidSeriesJsonException(e.getMessage());
        }

        room.setSeries(seriesJson);
        roomRepository.save(room);

        RoomChange change = RoomChange.seriesChange(player);

        sender.emitRoomChanged(room, change);

        return room.getSeries();
    }


    @GetMapping("/room/{roomIdOrAccessToken}/avatars")
    public List<AvatarAvailability> getAvatars(@PathVariable String roomIdOrAccessToken, @RequestParam("user_uuid") Optional<String> userUuid) throws RoomNotFoundException {
        Room room = getRoom(roomIdOrAccessToken, userUuid);
        Set<String> availableAvatars = RepositoryUtil.getAvailableAvatars(roomRepository, room);
        return Arrays.asList(Room.AVATARS).stream().map(avatar -> new AvatarAvailability(avatar, availableAvatars.contains(avatar))).collect(Collectors.toList());
    }

    @GetMapping("/room/{roomIdOrAccessToken}/locked")
    public boolean getLocked(
            @PathVariable String roomIdOrAccessToken,
            @RequestParam(name = "user_uuid") Optional<String> userUuid
            ) throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException, UserNotFoundException
    {
        Room room = getRoom(roomIdOrAccessToken, userUuid);
        return room.isLocked();
    }


    @PutMapping("/room/{roomIdOrAccessToken}/locked")
    public Room setLocked(
            @PathVariable String roomIdOrAccessToken,
            @RequestParam(name = "user_uuid") Optional<String> _userUuid,
            @RequestBody boolean shouldBeLocked
            ) throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException, UserNotFoundException
    {
        String userUuid = RestUtil.mandatory(_userUuid, "no user_uuid given");
        Room room = getRoom(roomIdOrAccessToken, _userUuid);
        User user = RestUtil.requireUser(userRepository, userUuid);
        Player player = RestUtil.userHasPlayerInRoom(roomRepository, userUuid, room);

        if (!user.getRole().equals(User.ROLE_PRO)) {
            throw new PermissionDeniedException("action requires 'pro' role");
        }

        if (shouldBeLocked && !room.isLocked()) {
            Room oldRoom = cloneRoomReference(room);
            room.generateAccessToken();
            room = roomRepository.save(room);
            sender.emitRoomChanged(oldRoom, RoomChange.locked(player));
        } else if (!shouldBeLocked && room.isLocked()) {
            Room oldRoom = cloneRoomReference(room);
            room.setAccessToken(Optional.empty());
            room = roomRepository.save(room);
            sender.emitRoomChanged(oldRoom, RoomChange.unlocked(player));
        }

        return room;
    }

    private Room cloneRoomReference(Room room) {
        Room oldRoom = new Room();
        oldRoom.setId(room.getId());
        oldRoom.setAccessToken(room.getAccessToken());
        return oldRoom;
    }
}
