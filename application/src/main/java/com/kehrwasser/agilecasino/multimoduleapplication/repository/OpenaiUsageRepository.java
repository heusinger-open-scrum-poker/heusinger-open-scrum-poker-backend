package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.OpenaiUsage;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface OpenaiUsageRepository extends CrudRepository<OpenaiUsage, Integer> {
    @Query(
        value = "SELECT COUNT(*) FROM openai_usage WHERE user = :userUuid",
        nativeQuery = true
    )
    Integer getNumberOfEstimatesForUser(@Param("userUuid") String userUuid);
}
