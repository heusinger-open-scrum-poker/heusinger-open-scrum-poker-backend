package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import java.net.URI;
import java.nio.charset.StandardCharsets;

import org.springframework.web.util.UriUtils;

public class UriBuilder {
    StringBuilder urlString = new StringBuilder();

    public UriBuilder() {}

    public static UriBuilder baseUri(String uri) {
        UriBuilder builder = new UriBuilder();
        builder.urlString.append(uri);
        return builder;
    }

    public UriBuilder pushSegment(String string) {
        urlString.append(UriUtils.encodePathSegment(string, StandardCharsets.UTF_8));
        return this;
    }

    public UriBuilder push(String string) {
        urlString.append(string);
        return this;
    }

    public URI build() {
        return URI.create(urlString.toString());
    }
}
