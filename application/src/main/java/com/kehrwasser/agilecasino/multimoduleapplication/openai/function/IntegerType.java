package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

public class IntegerType extends JsonType {
    private String description = "";

    public IntegerType() {
        this.setType("integer");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
