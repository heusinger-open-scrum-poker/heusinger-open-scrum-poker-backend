package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class UserNotRegisteredException extends RuntimeException {
    public UserNotRegisteredException(String uuid) {
        super("user not registered: uuid=" + uuid);
    }
}
