package com.kehrwasser.agilecasino.multimoduleapplication.openai;


public class SubtaskEstimate {
    private String summary = "";
    private Integer estimate = 0;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Integer getEstimate() {
        return estimate;
    }

    public void setEstimate(Integer estimate) {
        this.estimate = estimate;
    }
}

