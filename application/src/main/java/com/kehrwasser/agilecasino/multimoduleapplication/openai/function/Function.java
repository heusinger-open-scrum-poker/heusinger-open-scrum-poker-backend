package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

public class Function {
    private String name = "";
    private String description = "";
    private JsonType parameters = null;

    public Function() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JsonType getParameters() {
        return parameters;
    }

    public void setParameters(JsonType parameters) {
        this.parameters = parameters;
    }
}
