package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "session_token")
public class SessionToken {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name = "value")
    private String value;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "expires_at")
    private Date expiresAt;

    @OneToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "user", referencedColumnName = "uuid")
    @JsonIgnore
    private User user;

    public SessionToken() {
        long YEAR = 31536000000L;
        createdAt = new Date(System.currentTimeMillis());
        expiresAt = new Date(System.currentTimeMillis() + YEAR);
        value = UUID.randomUUID().toString();
    }

    public SessionToken(User user) {
        this();
        this.user = user;
    }


    public String getValue() {
        return value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }
}
