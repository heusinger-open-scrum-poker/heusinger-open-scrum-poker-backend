package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.METHOD_NOT_ALLOWED)
public class RoundNotFoundException extends RuntimeException {
    public RoundNotFoundException(String roomId) {
        super("There is no active round for room with id=" + roomId);
    }
}
