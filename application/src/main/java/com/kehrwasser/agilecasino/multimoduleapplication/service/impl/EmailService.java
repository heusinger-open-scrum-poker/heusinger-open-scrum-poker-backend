package com.kehrwasser.agilecasino.multimoduleapplication.service.impl;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.kehrwasser.agilecasino.multimoduleapplication.AgileCasinoConfiguration;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IEmailService;

public class EmailService implements IEmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);
    private static final Locale DEFAULT_LOCALE = Locale.forLanguageTag("de");

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private AgileCasinoConfiguration config;

    private SpringTemplateEngine htmlEngine;
    private SpringTemplateEngine textEngine;

    public EmailService() {
        initHtmlEngine();
        initTextEngine();
    }

    private void initHtmlEngine() {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setPrefix("templates/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode("HTML");
        resolver.setCharacterEncoding("UTF-8");

        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(resolver);

        this.htmlEngine = engine;
    }

    private void initTextEngine() {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setPrefix("templates/");
        resolver.setSuffix(".txt");
        resolver.setTemplateMode("TEXT");
        resolver.setCharacterEncoding("UTF-8");

        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(resolver);
        
        this.textEngine = engine;
    }

    public void sendRegistrationEmail(User user) throws MessagingException {
        MimeMessageHelper helper = makeRegistrationMail(user);
        sendMimeMessage(user, helper);
    }

    public void sendPasswordResetEmail(User user, String token) throws MessagingException {
        MimeMessageHelper helper = makePasswordResetMail(user, token);
        sendMimeMessage(user, helper);
    }

    private void sendMimeMessage(User user, MimeMessageHelper helper) throws MessagingException {
        Optional<String> emailAddress = user.getEmailAddress();

        if (emailAddress.isPresent()) {
            String senderAddress = config.getSmtpSenderAddress();
            String senderName = config.getSmtpSenderName();
            String senderSpec = senderName + " <" + senderAddress + ">";

            helper.setFrom(senderSpec);
            helper.setTo(emailAddress.get()); 

            MimeMessage message = helper.getMimeMessage();
            emailSender.send(message);
        } else {
            LOGGER.error("Mail cannot be sent. User(UUID=" + user.getUuid() + ") has no assigned email address.");
        }
    }

    public MimeMessageHelper makeRegistrationMail(User user) throws MessagingException {
        Context ctx = getContext(user);

        String subject = textEngine.process("subject-registration", ctx);
        String body = textEngine.process("registration", ctx);
        boolean isHtml = false;

        MimeMessageHelper helper = createMessage();
        helper.setSubject(subject); 
        helper.setText(body, isHtml);

        return helper;
    }

    public MimeMessageHelper makePasswordResetMail(User user, String token) throws MessagingException {
        Context ctx = getContext(user);
        ctx.setVariable("frontend_baseurl", config.getFrontendBaseUrl());
        ctx.setVariable("token", token);
        String encodedEmailAddress = URLEncoder.encode(user.getEmailAddress().orElse(""), StandardCharsets.UTF_8);
        ctx.setVariable("email", encodedEmailAddress);

        String subject = textEngine.process("subject-password-reset", ctx);
        String body = textEngine.process("password-reset", ctx);
        boolean isHtml = false;

        MimeMessageHelper helper = createMessage();
        helper.setSubject(subject); 
        helper.setText(body, isHtml);

        return helper;
    }

    private Locale getLocale(User user) {
        String localeString = user.getLanguage();

        if (localeString == null) {
            return DEFAULT_LOCALE;
        }

        Locale locale = Locale.forLanguageTag(localeString);

        if (locale == null) {
            return DEFAULT_LOCALE;
        }

        return locale;
    }

    private Context getContext(User user) {
        Locale locale = getLocale(user);

        return new Context(locale);
    }

    private MimeMessageHelper createMessage() throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        return new MimeMessageHelper(message, true, "UTF-8");
    }
}
