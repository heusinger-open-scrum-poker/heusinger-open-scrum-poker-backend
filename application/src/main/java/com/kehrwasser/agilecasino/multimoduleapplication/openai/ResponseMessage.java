package com.kehrwasser.agilecasino.multimoduleapplication.openai;

import java.util.ArrayList;
import java.util.List;

public class ResponseMessage {
    private List<SubtaskEstimate> subtasks = new ArrayList<SubtaskEstimate>();

	public List<SubtaskEstimate> getSubtasks() {
		return subtasks;
	}

	public void setSubtasks(List<SubtaskEstimate> subtasks) {
		this.subtasks = subtasks;
	}

    public void addSubtasks(SubtaskEstimate subtask) {
        this.subtasks.add(subtask);
    }
}

