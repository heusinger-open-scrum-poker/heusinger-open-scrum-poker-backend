package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import java.util.Optional;

public class UserPatch {
    private Optional<String> language;

    public UserPatch() {}

    public Optional<String> getLanguage() {
        return this.language;
    }

    public void setLanguage(Optional<String> language) {
        this.language = language;
    }
}
