package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import com.kehrwasser.agilecasino.multimoduleapplication.exception.EmailValidationException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PasswordTooShortException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PasswordVerificationException;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.AuthUtil;

@Entity
@DiscriminatorValue("email/password")
public class EmailPasswordAuthentication extends AuthMethod {
    @Column(name = "email")
    private String email;

    @Column(name = "password", columnDefinition = "TEXT")
    private String password;

    @Column(name = "reset_token", columnDefinition = "TEXT")
    private String resetToken;

    @Column(name = "reset_token_timestamp")
    private Timestamp resetTokenTimestamp;

    public EmailPasswordAuthentication() {
        super();
    }

    public EmailPasswordAuthentication(User user, String email, String password) {
        super(user);
        user.setAuthMethod(this);
        this.email = email;
        this.setPassword(password);
    }

    public String getEmail() {
        return email;
    }

	public void setEmail(String email) {
        this.email = email;
	}

    public void setPassword(String password) {
        validatePassword(password);

        this.password = passwordEncoder().encode(password);
    }

    public static void validateEmail(String email) throws EmailValidationException {
        if (!EmailValidator.getInstance().isValid(email)) {
            throw new EmailValidationException(email);
        }
    }


    public void verifyPassword(String rawPassword) throws PasswordVerificationException {
        boolean ok = passwordEncoder().matches(rawPassword, password);

        if (!ok) {
            throw new PasswordVerificationException();
        }
    }

    private static void validatePassword(String password) throws PasswordTooShortException {
        if (password.length() < 8) {
            throw new PasswordTooShortException();
        }
    }

    private static PasswordEncoder passwordEncoder() {
        CharSequence secret = "";
        return new Pbkdf2PasswordEncoder(secret, 65536, 512);
    }

    public Optional<String> getResetToken() {
        if (this.resetToken == null) {
            return Optional.empty();
        }

        return Optional.of(this.resetToken);
    }

    public Optional<Timestamp> getResetTokenTimestamp() {
        if (this.resetTokenTimestamp == null) {
            return Optional.empty();
        }

        return Optional.of(this.resetTokenTimestamp);
    }

    public void generateResetToken() {
        this.resetToken = AuthUtil.generatePasswordResetToken();
        this.resetTokenTimestamp = Timestamp.from(Instant.now());
    }

    public void removeResetToken() {
        this.resetToken = null;
        this.resetTokenTimestamp = null;
    }
}

