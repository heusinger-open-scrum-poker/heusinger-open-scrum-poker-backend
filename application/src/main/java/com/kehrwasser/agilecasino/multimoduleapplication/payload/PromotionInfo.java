package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;

public class PromotionInfo {
    Player player;
    Player initiatingPlayer;
    public PromotionInfo(Player player, Player initiatingPlayer) {
        this.player = player;
        this.initiatingPlayer = initiatingPlayer;
    }
    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }
    public Player getInitiatingPlayer() {
        return initiatingPlayer;
    }
    public void setInitiatingPlayer(Player initiatingPlayer) {
        this.initiatingPlayer = initiatingPlayer;
    }
}

