package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.net.HttpURLConnection;
import java.net.http.HttpRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.AgileCasinoConfiguration;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.InitialAccessTokenRequest;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.IsConnectedResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.IssuePagination;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraAccessTokenResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraIssue;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraProject;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraProjectResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraResource;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraResourceResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.JiraSubtask;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.Pagination;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.Sprint;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Game;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.JsonResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.UriBuilder;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.JiraUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.JiraUtil.SubtaskMetadata;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.TaskMetadata;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.CreateIssue;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.CreateSubtask;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.EditIssue;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.GetBoard;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.GetCustomFields;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.GetIssueTypes;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.MoveIssuesToSprint;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.PutEstimation;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.GetCustomFields.CustomFieldInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.Board;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.CreateIssueResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.IssueType;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.GameRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.JiraOAuthRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.SubtaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.TaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.HttpBadRequest;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.HttpForbidden;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.HttpInternalServerError;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.HttpNotFound;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.HttpUnauthorized;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.InternalServerException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PermissionDeniedException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RestController
@CrossOrigin(allowCredentials = "true")
public class JiraController {

    private static final Logger LOGGER = LoggerFactory.getLogger(JiraController.class);

    @Autowired
    AgileCasinoConfiguration config;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    GameRepository gameRepository;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    SubtaskRepository subtaskRepository;

    @Autowired
    JiraOAuthRepository jiraOAuthRepository;

    @PostMapping("/jira/oauth/state")
    public String jiraState(@RequestParam("user_uuid") String userUuid) throws UserNotFoundException, InternalServerException {
        User user = RestUtil.requireUser(userRepository, userUuid);

        String newState = UUID.randomUUID().toString();

        Optional<JiraOAuth> jiraInfo = jiraOAuthRepository.findByUser(user.getUuid());
        if (jiraInfo.isEmpty()) {
            jiraInfo = Optional.of(new JiraOAuth());
            jiraInfo.get().setUser(user);
        }

        jiraInfo.get().setState(newState);
        jiraOAuthRepository.save(jiraInfo.get());

        try {
            return objectMapper.writeValueAsString(newState);
        } catch (JsonProcessingException e) {
            throw new InternalServerException(e.getMessage());
        }
    }

    @GetMapping("/jira/oauth/submit_auth_code")
    public void submitAuthCode(
            @RequestParam(name = "code") String code,
            @RequestParam(name = "state") String state
            ) throws InternalServerException, PermissionDeniedException
    {
        Optional<JiraOAuth> jiraInfo = jiraOAuthRepository.findByState(state);

        if (jiraInfo.isEmpty() || jiraInfo.get().getState() == null) {
            throw new PermissionDeniedException("no state generated");
        }

        if (!jiraInfo.get().getState().equals(state)) {
            throw new PermissionDeniedException("invalid state");
        }

        InitialAccessTokenRequest body = new InitialAccessTokenRequest(
                config.getJiraClientId(),
                config.getJiraSecret(),
                code,
                config.getFrontendBaseUrl());

        HttpRequest request = HttpRequest.newBuilder()
            .uri(UriBuilder.baseUri("https://auth.atlassian.com/oauth/token").build())
            .header("Content-Type", "application/json")
            .POST(Util.toBodyPublisher(objectMapper, body))
            .build();

        JiraAccessTokenResponse access = Util.fetchJson(objectMapper, request, new TypeReference<JiraAccessTokenResponse>() {});

        // update and save info
        Integer MILLIS_PER_SECOND = 1000;
        Timestamp expiresAt = new Timestamp(System.currentTimeMillis() + MILLIS_PER_SECOND * access.getExpiresIn());
        jiraInfo.get().setAccessToken(access.getAccessToken());
        jiraInfo.get().setRefreshToken(access.getRefreshToken());
        jiraInfo.get().setExpirationDatetime(expiresAt);
        jiraInfo.get().setScope(access.getScope());
        jiraInfo.get().setTokenType(access.getTokenType());
        jiraInfo.get().setState(null);

        jiraOAuthRepository.save(jiraInfo.get());
    }

    @GetMapping("/jira/resources")
    public List<JiraResource> listResources(@RequestParam("user_uuid") String userUuid) throws UserNotFoundException, InternalServerException, PermissionDeniedException {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);

        HttpRequest request = HttpRequest.newBuilder()
            .uri(UriBuilder.baseUri("https://api.atlassian.com/oauth/token/accessible-resources").build())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();

        JsonResponse<JiraResourceResponse[]> response = Util.fetchJson2(objectMapper, request, new TypeReference<JiraResourceResponse[]>() {});

        if (response.isSuccess()) {
            JiraResourceResponse[] resources = response.getJson().get();

            List<JiraResource> result = new ArrayList<JiraResource>();
            for (int i = 0; i < resources.length; ++i) {
                JiraResourceResponse resource = resources[i];
                result.add(new JiraResource(resource.getId(), resource.getUrl(), resource.getName(), resource.getAvatarUrl()));

            }
            return result;
        }

        LOGGER.info("listResources(): Received error response from Jira: " + response.getHttpResponse().body());

        switch (response.getStatusCode()) {
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                throw new HttpUnauthorized();
            default:
                throw new HttpInternalServerError("Unexpected Error");
        }
    }



    @GetMapping("/jira/projects")
    public List<JiraProject> listProjects(@RequestParam("user_uuid") String userUuid, @RequestParam("resource_id") String resourceId) throws UserNotFoundException {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);


        HttpRequest request = HttpRequest.newBuilder()
            .uri(UriBuilder
                    .baseUri("https://api.atlassian.com/ex/jira/")
                    .pushSegment(resourceId)
                    .push("/rest/api/3/project/recent")
                    .build())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();


        JsonResponse<JiraProjectResponse[]> response = Util.fetchJson2(objectMapper, request, new TypeReference<JiraProjectResponse[]>() {});

        if (response.isSuccess()) {
            JiraProjectResponse[] projects = response.getJson().get();

            List<JiraProject> result = new ArrayList<JiraProject>();
            for (int i = 0; i < projects.length; ++i) {
                JiraProjectResponse project = projects[i];
                result.add(new JiraProject(project.getId(), project.getKey(), project.getSelf(), project.getAvatarUrls().get("48x48")));

            }
            return result;
        }

        LOGGER.info("listProjects(): Received error response from Jira: " + response.getHttpResponse().body());

        switch (response.getStatusCode()) {
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                throw new HttpUnauthorized();
            case HttpURLConnection.HTTP_BAD_REQUEST:
                throw new HttpBadRequest();
            default:
                throw new HttpInternalServerError("Unexpected Error");
        }
    }


    @GetMapping("/jira/issues")
    public List<JiraIssue> listIssues(
            @RequestParam("user_uuid") String userUuid,
            @RequestParam("resource_id") String resourceId,
            @RequestParam("project_key") String projectKey)
            throws UserNotFoundException
    {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);

        HttpRequest request = HttpRequest.newBuilder()
            .uri(UriBuilder
                    .baseUri("https://api.atlassian.com/ex/jira/")
                    .pushSegment(resourceId)
                    .push("/rest/api/3/search?jql=")
                    .push(
                        Util.encodeURIComponent("project = "
                            + projectKey
                            + " and sprint in futureSprints() and issueType in standardIssueTypes()"))
                    .build())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();

        JsonResponse<IssuePagination> response = Util.fetchJson2(objectMapper, request, new TypeReference<IssuePagination>() {});

        if (response.isSuccess()) {
            IssuePagination pagination = response.getJson().get();

            List<JiraIssue> result = new ArrayList<JiraIssue>();
            for (int i = 0; i < pagination.getIssues().length; ++i) {
                JsonNode node = pagination.getIssues()[i];
                CustomFieldInfo customFieldInfo = new CustomFieldInfo();
                JiraIssue issue = JiraIssue.fromJsonNode(node, customFieldInfo);
                result.add(issue);
            }
            return result;
        }


        LOGGER.info("listIssues(): Received error response from Jira: " + response.getHttpResponse().body());

        switch (response.getStatusCode()) {
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                throw new HttpUnauthorized();
            case HttpURLConnection.HTTP_BAD_REQUEST:
                throw new HttpBadRequest();
            case HttpURLConnection.HTTP_NOT_FOUND:
                throw new HttpNotFound("No issues matching the search criteria are found");
            default:
                throw new HttpInternalServerError("Unexpected Error");
        }
    }



    @GetMapping("/jira/boards")
    public List<Board> listBoards(
        @RequestParam("user_uuid") String userUuid,
        @RequestParam("resource_id") String resourceId)
        throws UserNotFoundException
    {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);

        HttpRequest request = HttpRequest.newBuilder()
            .uri(UriBuilder
                    .baseUri("https://api.atlassian.com/ex/jira/")
                    .pushSegment(resourceId)
                    .push("/rest/agile/1.0/board")
                    .build())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();


        JsonResponse<Pagination<Board>> response = Util.fetchJson2(objectMapper, request, new TypeReference<Pagination<Board>>() {});

        if (response.isSuccess()) {
            Pagination<Board> pagination = response.getJson().get();
            return pagination.getValues();
        }

        LOGGER.info("listBoards(): Received error response from Jira: " + response.getHttpResponse().body());

        switch (response.getStatusCode()) {
            case HttpURLConnection.HTTP_BAD_REQUEST:
                throw new HttpBadRequest();
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                throw new HttpUnauthorized();
            case HttpURLConnection.HTTP_FORBIDDEN:
                throw new HttpForbidden("User does not have the permission to access boards");
            default:
                throw new HttpInternalServerError("Unexpected Error");
        }
    }

    @GetMapping("/jira/sprints")
    public List<Sprint> listSprints(
        @RequestParam("user_uuid") String userUuid,
        @RequestParam("resource_id") String resourceId,
        @RequestParam("project_id") String projectId)
        throws UserNotFoundException
    {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);

        HttpRequest request = HttpRequest.newBuilder()
            .uri(UriBuilder.baseUri("https://api.atlassian.com/ex/jira/")
                    .pushSegment(resourceId)
                    .push("/rest/agile/1.0/board/")
                    .pushSegment(projectId)
                    .push("/sprint?state=future,active")
                    .build())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();



        JsonResponse<Pagination<Sprint>> response = Util.fetchJson2(objectMapper, request, new TypeReference<Pagination<Sprint>>() {});

        if (response.isSuccess()) {
            Pagination<Sprint> pagination = response.getJson().get();
            return pagination.getValues();
        }

        LOGGER.info("listSprints(): Received error response from Jira: " + response.getHttpResponse().body());

        switch (response.getStatusCode()) {
            case HttpURLConnection.HTTP_BAD_REQUEST:
                throw new HttpBadRequest();
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                throw new HttpUnauthorized();
            case HttpURLConnection.HTTP_FORBIDDEN:
                throw new HttpForbidden("User does not have the permission to access boards");
            case HttpURLConnection.HTTP_NOT_FOUND:
                throw new HttpNotFound("Board does not exist or the user does not have permission to view it.");
            default:
                throw new HttpInternalServerError("Unexpected Error");
        }
    }

    @GetMapping("/jira/issues2")
    public List<JiraIssue> listIssues2(
        @RequestParam("user_uuid") String userUuid,
        @RequestParam("resource_id") String resourceId,
        @RequestParam("board_id") String boardId,
        @RequestParam("sprint_id") String sprintId)
        throws UserNotFoundException
    {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);


        GetCustomFields getCustomFields = new GetCustomFields(jira, resourceId, boardId);
        CustomFieldInfo customFieldInfo = new CustomFieldInfo();
        try {
            customFieldInfo = getCustomFields.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        HttpRequest request = HttpRequest.newBuilder()
            .uri(UriBuilder.baseUri("https://api.atlassian.com/ex/jira/")
                    .pushSegment(resourceId)
                    .push("/rest/agile/1.0/sprint/")
                    .push(sprintId)
                    .push("/issue?jql=")
                    .push(Util.encodeURIComponent("issueType in standardIssueTypes()"))
                    .build())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();

        JsonResponse<IssuePagination> response = Util.fetchJson2(objectMapper, request, new TypeReference<IssuePagination>() {});

        if (response.isSuccess()) {
            IssuePagination pagination = response.getJson().get();

            List<JiraIssue> result = new ArrayList<JiraIssue>();
            for (int i = 0; i < pagination.getIssues().length; ++i) {
                JsonNode node = pagination.getIssues()[i];
                JiraIssue issue = JiraIssue.fromJsonNode(node, customFieldInfo);
                result.add(issue);
            }


            result.sort(new Comparator<JiraIssue>() {

                @Override
                public int compare(JiraIssue a, JiraIssue b) {
                    if (a.getRank() == null && b.getRank() == null) {
                        return 0;
                    } else if (a.getRank() == null) {
                        return 1;
                    } else if (b.getRank() == null) {
                        return -1;
                    }

                    return a.getRank().compareTo(b.getRank());
                }
            });

            return result;
        }


        LOGGER.info("listIssues2(): Received error response from Jira: " + response.getHttpResponse().body());

        switch (response.getStatusCode()) {
            case HttpURLConnection.HTTP_BAD_REQUEST:
                throw new HttpBadRequest();
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                throw new HttpUnauthorized();
            case HttpURLConnection.HTTP_FORBIDDEN:
                throw new HttpForbidden("User does not have the permission to access boards");
            case HttpURLConnection.HTTP_NOT_FOUND:
                throw new HttpNotFound("Board does not exist or the user does not have permission to view it.");
            default:
                throw new HttpInternalServerError("Unexpected Error");
        }
    }


    @PostMapping("/room/{roomId}/game/{gameId}/jira/export")
    public void export(
        @PathVariable("roomId") String roomIdOrAccessToken,
        @PathVariable("gameId") Integer gameId,
        @RequestParam("user_uuid") String userUuid,
        @RequestParam("resource_id") String resourceId,
        @RequestParam("board_id") String boardId,
        @RequestParam("sprint_id") Integer sprintId)
        throws UserNotFoundException
    {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        Game game = RestUtil.requireGame(gameRepository, gameId);
        boolean gameBelongsToRoom = game.getRoomId().equals(room.getId());

        if (!gameBelongsToRoom) {
            throw new RuntimeException("game " + game.getId() + " does not belong to room " + room.getId());
        }


        GetBoard getBoard = new GetBoard(jira, resourceId, boardId);
        Board board = getBoard.fetch();
        String projectId = board.getLocation().getProjectId().toString();

        GetIssueTypes getIssueTypes = new GetIssueTypes(jira, resourceId, projectId);
        List<IssueType> issueTypes = getIssueTypes.fetch();

        IssueType taskType = JiraUtil.determineTaskType(issueTypes);
        IssueType subtaskType = JiraUtil.determineSubtaskType(issueTypes);

        LOGGER.debug("export: determined task type = " + taskType.getName());
        LOGGER.debug("export: determined subtask type = " + subtaskType.getName());

        List<Task> tasks = new ArrayList<>();
        List<Subtask> subtasks = new ArrayList<>();
        List<Task> tasksToBeUpdated = new ArrayList<>();
        List<Subtask> subtasksToBeUpdated = new ArrayList<>();
        taskRepository.findAllByGameId(game.getId()).forEach(tasks::add);
        List<String> createdIssueKeys = new ArrayList<>();

        LOGGER.debug("export: creating tasks and subtasks");
        try {
            for (Task task: tasks) {
                boolean taskExistsInJira = task.getProvider().equals(Optional.of("jira"));
                Optional<TaskMetadata> taskMetadata = JiraUtil.getTaskMetadata(task);
                Optional<String> taskId = taskMetadata.map(metadata -> metadata.getIssue().getId());
                
                if (taskMetadata.isEmpty()) {
                    taskExistsInJira = false;
                }

                if (!taskExistsInJira) {
                    String issueType = taskType.getId();
                    String descriptionHtml = task.getDescription();
                    String summary = task.getSubject();
                    CreateIssue createIssue = new CreateIssue(jira, resourceId, projectId, issueType, descriptionHtml, summary);
                    ObjectMapper om = new ObjectMapper();
                    LOGGER.debug("export: create " + task.getSubject() + " with payload " + om.writeValueAsString(createIssue.makeBody()));
                    CreateIssueResponse response = createIssue.fetch();

                    createdIssueKeys.add(response.getKey());
                    taskId = Optional.of(response.getId());

                    task.setProvider(Optional.of("jira"));
                    String metadataJson = createTaskMetadataJson(resourceId, projectId, response);
                    task.setMetadata(Optional.of(metadataJson));
                } else {
                    tasksToBeUpdated.add(task);
                }

                assert taskId.isPresent();

                List<Subtask> subtasksForTask = new ArrayList<>();
                subtaskRepository.findAllByTaskId(task.getId()).forEach(subtasksForTask::add);
                subtasks.addAll(subtasksForTask);

                for (Subtask subtask: subtasksForTask) {
                    boolean subtaskExistsInJira = subtask.getProvider() != null && subtask.getProvider().equals("jira");

                    if (!subtaskExistsInJira) {
                        String parentId = taskId.get();
                        String issueType = subtaskType.getId();
                        String summary = subtask.getSubject();
                        CreateSubtask createIssue = new CreateSubtask(jira, resourceId, projectId, parentId, issueType, summary);
                        CreateIssueResponse response = createIssue.fetch();

                        subtask.setProvider("jira");
                        String metadataJson = createSubtaskMetadataJson(resourceId, projectId, parentId, response);
                        subtask.setMetadata(metadataJson);
                    } else {
                        subtasksToBeUpdated.add(subtask);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            taskRepository.saveAll(tasks);
            subtaskRepository.saveAll(subtasks);
        }

        try {
            ObjectMapper om = new ObjectMapper();
            LOGGER.debug("export: move created issues " + om.writeValueAsString(createdIssueKeys));
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        try {
            MoveIssuesToSprint moveIssuesToSprint = new MoveIssuesToSprint(jira, resourceId, sprintId, createdIssueKeys);
            moveIssuesToSprint.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOGGER.debug("export: update tasks");
        for (Task task: tasksToBeUpdated) {
            Optional<TaskMetadata> metadata = JiraUtil.getTaskMetadata(task);
            assert metadata.isPresent();
            String issueId = metadata.get().getIssue().getId();
            String summary = task.getSubject();
            Optional<String> descriptionHtml = Optional.of(task.getDescription());
            EditIssue updateIssue = new EditIssue(jira, resourceId, issueId, summary, descriptionHtml);
            ObjectMapper om = new ObjectMapper();
            try {
				LOGGER.debug("export: update task " + issueId + " and payload " + om.writeValueAsString(updateIssue.makeBody()));
                updateIssue.fetch();
			} catch (Exception e) {
				e.printStackTrace();
			}
        }

        LOGGER.debug("export: update subtasks");
        for (Subtask subtask: subtasksToBeUpdated) {
            Optional<SubtaskMetadata> metadata = JiraUtil.getSubtaskMetadata(subtask);
            assert metadata.isPresent();
            String issueId = metadata.get().getSubtask().getId();
            String summary = subtask.getSubject();
            Optional<String> descriptionHtml = Optional.empty();
            EditIssue updateIssue = new EditIssue(jira, resourceId, issueId, summary, descriptionHtml);
            ObjectMapper om = new ObjectMapper();
            try {
				LOGGER.debug("export: update subtask " + issueId + " and payload " + om.writeValueAsString(updateIssue.makeBody()));
                updateIssue.fetch();
			} catch (Exception e) {
				e.printStackTrace();
			}
        }

        LOGGER.debug("export: update estimations");
        for (Task task: tasks) {
            Optional<TaskMetadata> metadata = JiraUtil.getTaskMetadata(task);
            assert metadata.isPresent();
            String issueId = metadata.get().getIssue().getId();
            String estimation = task.getEstimate();
            try {
                PutEstimation putEstimation = new PutEstimation(jira, resourceId, boardId, issueId, estimation);
                putEstimation.fetch();
            } catch (Exception e) {
                LOGGER.error("export: estimation update failed for " + issueId);
                e.printStackTrace();
            }
        }

        LOGGER.debug("export: done");
    }

    String createTaskMetadataJson(String resourceId, String projectId, CreateIssueResponse response) {
        Optional<String> metadataJson = Optional.empty();
        {
            TaskMetadata metadata = new TaskMetadata();
            metadata.setProject(projectId);
            metadata.setResource(resourceId);
            JiraIssue issue = new JiraIssue();
            issue.setId(response.getId());
            issue.setKey(response.getKey());
            issue.setSelf(response.getSelf());
            metadata.setIssue(issue);

            try {
                metadataJson = Optional.of(objectMapper.writeValueAsString(metadata));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }
        assert metadataJson.isPresent();

        return metadataJson.get();
    }

    String createSubtaskMetadataJson(String resourceId, String projectId, String parentId, CreateIssueResponse response) {
        Optional<String> metadataJson = Optional.empty();
        {
            SubtaskMetadata metadata = new SubtaskMetadata();
            metadata.setProject(projectId);
            metadata.setResource(resourceId);
            metadata.setParentId(parentId);
            JiraSubtask subtask = new JiraSubtask();
            subtask.setId(response.getId());
            subtask.setKey(response.getKey());
            subtask.setSelf(response.getSelf());
            metadata.setSubtask(subtask);

            try {
                metadataJson = Optional.of(objectMapper.writeValueAsString(metadata));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }

        assert metadataJson.isPresent();

        return metadataJson.get();
    }

    @PostMapping("/jira/disconnect")
    public void disconnectJira(
        @RequestParam("user_uuid") String userUuid
        ) throws UserNotFoundException
    {
        User user = RestUtil.requireUser(userRepository, userUuid);
        JiraOAuth jira = RestUtil.requireJira(jiraOAuthRepository, user);

        jiraOAuthRepository.delete(jira);
    }

    @GetMapping("/jira/isConnected")
    public IsConnectedResponse isConnected(
        @RequestParam("user_uuid") String userUuid
        ) throws UserNotFoundException
    {
        RestUtil.requireUser(userRepository, userUuid);

        try {
            listResources(userUuid);
            
            return new IsConnectedResponse(true);
        } catch (Exception e) {
            return new IsConnectedResponse(false);
        }
    }
}

