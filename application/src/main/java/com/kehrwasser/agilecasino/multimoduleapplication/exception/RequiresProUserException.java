package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class RequiresProUserException extends RuntimeException {
    public RequiresProUserException(String userUuid) {
        super("user missing pro role: " + userUuid);
    }
}
