package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import java.net.http.HttpResponse;
import java.util.Optional;


public class JsonResponse<JsonBody> {
    private HttpResponse<String> httpResponse;
    private Optional<JsonBody> json;

    public JsonResponse(HttpResponse<String> httpResponse, Optional<JsonBody> json) {
        this.httpResponse = httpResponse;
        this.json = json;
    }

    public HttpResponse<String> getHttpResponse() {
        return httpResponse;
    }

    public Optional<JsonBody> getJson() {
        return json;
    }

    public int getStatusCode() {
        return httpResponse.statusCode();
    }

    public boolean isSuccess() {
        return 200 <= getStatusCode() && getStatusCode() < 300;
    }
}
