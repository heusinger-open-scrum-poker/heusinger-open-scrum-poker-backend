package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class UnavailablePlayerNameException extends RuntimeException {
    public UnavailablePlayerNameException(String name) {
        super("unavailable player name: " + name);
    }
}


