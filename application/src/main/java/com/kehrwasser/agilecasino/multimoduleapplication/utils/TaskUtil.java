package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.github.pravin.raha.lexorank4j.LexoRank;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.SubtaskOperation;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.TaskOperation;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.InvalidDataException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PermissionDeniedException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PlayerNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.RoomNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.TaskNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank.LexorankService;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.SubtaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.TaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;

public class TaskUtil {
	public static List<Task> batchUpdateTasks(
            TaskRepository taskRepository,
            SubtaskRepository subtaskRepository,
            RoomRepository roomRepository,
            Optional<IWebSocketSender> sender,
            LexorankService lexorankService,
			Room room,
            List<TaskOperation> taskOperations
	) {
        List<Task> results = new ArrayList<Task>();

        for (TaskOperation operation: taskOperations) {
            if (operation.isCreateType()) {
                try {
                    Task created = createTask(taskRepository, roomRepository, Optional.empty(), lexorankService, room, operation.getTask());
                    results.add(created);

                    if (operation.getSubtaskOperations() != null) {
                        for (SubtaskOperation subOperation: operation.getSubtaskOperations()) {
                            subOperation.getSubtask().setTaskId(created.getId());
                        }

                        batchUpdateSubtasks(subtaskRepository, Optional.empty(), lexorankService, room, operation.getSubtaskOperations());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (operation.isUpdateType()) {
                try {
                    Integer taskId = operation.getTask().getId();
                    Optional<Task> original = taskRepository.findById(taskId);

                    if (original.isEmpty()) {
                        throw new RuntimeException("error during batch operation: task " + taskId + " does not exist");
                    }

                    Task updated = updateTask(taskRepository, Optional.empty(), room, original.get(), operation.getTask());

                    if (updated != null) {
                        results.add(updated);
                    }

                    if (operation.getSubtaskOperations() != null) {
                        if (operation.getSubtaskOperations() != null) {
                            for (SubtaskOperation subOperation: operation.getSubtaskOperations()) {
                                subOperation.getSubtask().setTaskId(updated.getId());
                            }

                            batchUpdateSubtasks(subtaskRepository, Optional.empty(), lexorankService, room, operation.getSubtaskOperations());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (operation.isDeleteType()) {
                try {
                    Integer taskId = operation.getTask().getId();
                    Optional<Task> task = taskRepository.findById(taskId);

                    if (task.isEmpty()) {
                        throw new RuntimeException("error during batch operation: task " + taskId + " does not exist");
                    }

                    deleteTask(taskRepository, roomRepository, Optional.empty(), room, task.get());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (sender.isPresent()) {
            sender.get().emitGamesUpdated(room);
        }
        
        return results;
	}

    public static Task createTask(
            TaskRepository taskRepository,
            RoomRepository roomRepository,
            Optional<IWebSocketSender> sender,
            LexorankService lexorankService,
            Room room,
            Task task)
            throws RoomNotFoundException, PermissionDeniedException {
        task.setId(null);
        task.setRoomId(room.getId());
        
        if (task.getEstimate() == null) {
            task.setEstimate("");
        }

        if (room.getActiveGameId() != null) {
            task.setGameId(room.getActiveGameId());
        }

        boolean roomHasNoTask = room.getCurrentTask().isEmpty();

        if (roomHasNoTask) {
            task.setLexoRank(LexoRank.middle());
            task = taskRepository.save(task);

            room.setCurrentTask(Optional.of(task));
            roomRepository.save(room);

            if (sender.isPresent()) {
                sender.get().emitTaskCreated(room, task);
                sender.get().emitCurrentTaskChanged(room, Optional.of(task));
            }
        } else {
            List<Task> tasks = new ArrayList<Task>();
            Iterator<Task> it = taskRepository.findAllByRoomId(room.getId()).iterator();
            while (it.hasNext()) {
                tasks.add(it.next());
            }

            boolean ok = lexorankService.repairRanks(tasks);

            if (!ok) {
                taskRepository.saveAll(tasks);
            }

            lexorankService.sortEntitiesByRank(tasks);
            task.setLexoRank(tasks.get(tasks.size() - 1).getLexoRank().genNext());
            taskRepository.save(task);

            if (sender.isPresent()) {
                sender.get().emitTaskCreated(room, task);
            }
        }

        return task;
    }

    public static Task updateTask(
            TaskRepository taskRepository,
            Optional<IWebSocketSender> sender,
            Room room,
            Task task,
            Task update)
            throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException, TaskNotFoundException {

        if (update.getSubject() != null) {
            task.setSubject(update.getSubject());
        }
        if (update.getEstimate() != null) {
            task.setEstimate(update.getEstimate());
        }
        if (update.getDescription() != null) {
            task.setDescription(update.getDescription());
        }
        if (update.getRank() != null) {
            LexoRank validRank = LexoRank.parse(update.getRank());
            if (validRank != null) {
                task.setRank(update.getRank());
            } else {
                throw new InvalidDataException("invalid rank " + update.getRank());
            }
        }
        task = taskRepository.save(task);

        if (sender.isPresent()) {
            sender.get().emitTaskUpdated(room, task);
        }

        return task;
    }

    public static void deleteTask(
            TaskRepository taskRepository,
            RoomRepository roomRepository,
            Optional<IWebSocketSender> sender,
            Room room,
            Task task)
            throws TaskNotFoundException, PermissionDeniedException {

        Task taskClone = task.cloneTask();

        boolean deletedTaskIsCurrentTask = room.getCurrentTask().isPresent() && room.getCurrentTask().get().getId() == task.getId();
        if (deletedTaskIsCurrentTask) {
            room.setCurrentTask(Optional.empty());
            roomRepository.save(room);
        }

        taskRepository.delete(task);

        Optional<Task> currentTask = RestUtil.ensureRoomHasACurrentTask(roomRepository, taskRepository, room);

        if (sender.isPresent()) {
            sender.get().emitTaskDeleted(room, taskClone);

            if (deletedTaskIsCurrentTask) {
                sender.get().emitCurrentTaskChanged(room, currentTask);
            }
        }
    }

	public static List<Subtask> batchUpdateSubtasks(
            SubtaskRepository subtaskRepository,
            Optional<IWebSocketSender> sender,
            LexorankService lexorankService,
			Room room,
            List<SubtaskOperation> subtaskOperations
	) {
        List<Subtask> results = new ArrayList<Subtask>();

        for (SubtaskOperation operation: subtaskOperations) {
            if (operation.isCreateType()) {
                try {
                    Subtask created = createSubtask(subtaskRepository, Optional.empty(), lexorankService, room, operation.getSubtask());
                    results.add(created);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (operation.isUpdateType()) {
                try {
                    Integer subtaskId = operation.getSubtask().getId();
                    Optional<Subtask> subtask = subtaskRepository.findById(subtaskId);

                    if (subtask.isEmpty()) {
                        throw new RuntimeException("error during batch operation: subtask " + subtaskId + " does not exist");
                    }

                    Subtask updated = updateSubtask(subtaskRepository, Optional.empty(), lexorankService, room, subtask.get(), operation.getSubtask());

                    if (updated != null) {
                        results.add(updated);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (operation.isDeleteType()) {
                try {
                    Integer subtaskId = operation.getSubtask().getId();
                    Optional<Subtask> subtask = subtaskRepository.findById(subtaskId);

                    if (subtask.isEmpty()) {
                        throw new RuntimeException("error during batch operation: subtask " + subtaskId + " does not exist");
                    }

                    deleteSubtask(subtaskRepository, Optional.empty(), room, subtask.get());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        
        if (sender.isPresent()) {
            sender.get().emitGamesUpdated(room);
        }

        return results;
	}

	public static Subtask createSubtask(
            SubtaskRepository subtaskRepository,
            Optional<IWebSocketSender> sender,
            LexorankService lexorankService,
            Room room,
			Subtask subtask)
	{

		// add new subtask to a list
		ArrayList<Subtask> subtasks = new ArrayList<>();
		subtasks.add(subtask);

		// fetch existing subtasks and add them to list
		for (Subtask value : subtaskRepository.findAllByTaskId(subtask.getTaskId())) {
			subtasks.add(value);
		}

		// repair ranks and save all
		lexorankService.repairRanks(subtasks);
		subtaskRepository.saveAll(subtasks);

        if (sender.isPresent()) {
            sender.get().emitSubtaskCreated(room, subtask);
        }

		return subtask;
	}

	public static Subtask updateSubtask(
            SubtaskRepository subtaskRepository,
			Optional<IWebSocketSender> sender,
            LexorankService lexorankService,
			Room room,
            Subtask subtask,
			Subtask update)
	{
		boolean isValidRank = lexorankService.isValidRank(update.getRank());
		if (!isValidRank) {
			throw new InvalidDataException("invalid rank " + update.getRank());
		}

		subtask.setSubject(update.getSubject());
		subtask.setChecked(update.getChecked());
		subtask.setRank(update.getRank());

		subtask = subtaskRepository.save(subtask);

        if (sender.isPresent()) {
            sender.get().emitSubtaskUpdated(room, subtask);
        }

        return subtask;
	}


	public static void deleteSubtask(
            SubtaskRepository subtaskRepository,
			Optional<IWebSocketSender> sender,
            Room room,
			Subtask subtask
	) {
		Subtask clone = subtask.clone();
		subtaskRepository.delete(subtask);

        if (sender.isPresent()) {
            sender.get().emitSubtaskDeleted(room, clone);
        }
	}
}
