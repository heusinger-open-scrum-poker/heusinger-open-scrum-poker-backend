package com.kehrwasser.agilecasino.multimoduleapplication.service;

import java.util.List;
import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.ImportModalState;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.JoinedRoom;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserPatch;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserProfile;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository.RegisteredUserInfo;

public interface IUserService {
    Optional<User> findUserByUuid(String uuid);
    User createUser(Optional<String> invitedByHash);
    User updateUser(User user, UserPatch patch);
    User saveUser(User user);
    Optional<ImportModalState> getImportModalState(User user);
    void setImportModalState(User user, Optional<ImportModalState> importModalState);
    List<JoinedRoom> findJoinedRooms(User user);
    List<RegisteredUserInfo> findRegisteredUsers();
    Optional<UserProfile> findUserProfile(User user);
}
