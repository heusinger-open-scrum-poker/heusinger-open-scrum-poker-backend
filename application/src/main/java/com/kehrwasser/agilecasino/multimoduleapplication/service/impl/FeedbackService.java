package com.kehrwasser.agilecasino.multimoduleapplication.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Feedback;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.FeedbackRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IFeedbackService;

public class FeedbackService implements IFeedbackService {

    @Autowired
    private FeedbackRepository feedbackRepository;

	@Override
	public Optional<Feedback> findFeedbackById(Integer feedbackId) {
        return feedbackRepository.findById(feedbackId);
	}

	@Override
	public Feedback createFeedback(Feedback feedback) {
        feedback.setId(null);
        feedbackRepository.save(feedback);
        return feedback;
	}

    @Override
    public Optional<Feedback> updateFeedback(Feedback update) {
        return feedbackRepository.findById(update.getId()).map(feedback -> {
            if (update.getEmail() != null) {
                feedback.setEmail(update.getEmail());
            }
            if (update.getContent() != null) {
                feedback.setContent(update.getContent());
            }

            return feedbackRepository.save(feedback);
        });
	}
}
