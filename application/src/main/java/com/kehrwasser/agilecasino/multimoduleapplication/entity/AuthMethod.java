package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import javax.persistence.DiscriminatorType;

@Entity(name = "auth_method")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="auth_type", discriminatorType = DiscriminatorType.STRING)
public class AuthMethod {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name="auth_type", insertable = false, updatable = false)
    protected String authType;
    
    @OneToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "user", referencedColumnName = "uuid")
    private User user;

    public AuthMethod() {
    }

    public AuthMethod(User user) {
        if (user == null) {
            throw new Error("user is null");
        }

        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public String getAuthType() {
        return authType;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public EmailPasswordAuthentication asEmailPasswordAuthentication() {
        if (this instanceof EmailPasswordAuthentication) {
            return (EmailPasswordAuthentication)this;
        } else {
            return null;
        }
    }
}
