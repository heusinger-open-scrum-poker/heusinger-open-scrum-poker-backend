package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.AuthMethod;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.EmailPasswordAuthentication;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface AuthRepository extends CrudRepository<AuthMethod, Integer> {

    @Query("FROM EmailPasswordAuthentication auth WHERE auth.id=:id")
    public Optional<EmailPasswordAuthentication> findEmailPasswordAuthById(Integer id);
}

