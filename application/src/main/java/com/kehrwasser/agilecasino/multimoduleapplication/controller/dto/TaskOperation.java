package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import java.util.List;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;

public class TaskOperation {
    public static final String OPERATION_CREATE = "create";
    public static final String OPERATION_UPDATE = "update";
    public static final String OPERATION_DELETE = "delete";

    private String type;
    private Task task;
    private List<SubtaskOperation> subtaskOperations;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public boolean isCreateType() {
        return getType().equals(TaskOperation.OPERATION_CREATE);
    }

    public boolean isUpdateType() {
        return getType().equals(TaskOperation.OPERATION_UPDATE);
    }

    public boolean isDeleteType() {
        return getType().equals(TaskOperation.OPERATION_DELETE);
    }

    public List<SubtaskOperation> getSubtaskOperations() {
        return subtaskOperations;
    }

    public void setSubtaskOperations(List<SubtaskOperation> subtaskOperations) {
        this.subtaskOperations = subtaskOperations;
    }
}
