package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Game;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.GameDoesNotBelongToRoomException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.GameNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PermissionDeniedException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.RoomNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.GameRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.TaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(allowCredentials = "true")
public class GameController {
	@Autowired
	@Lazy
	private RoomRepository roomRepository;

	@Autowired
	@Lazy
	private UserRepository userRepository;

	@Autowired
	@Lazy
	private GameRepository gameRepository;

	@Autowired
	private IWebSocketSender sender;

    @Autowired
    private TaskRepository taskRepository;

	@GetMapping("/room/{roomIdOrAccessToken}/games")
	public List<Game> getGames(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid)
	{
		Room room = validate(userUuid, roomIdOrAccessToken);

		return this.getGamesList(room);
	}

	private ArrayList<Game> getGamesList(Room room) {
		ArrayList<Game> games = new ArrayList<>();
		for (Game value : gameRepository.findAllByRoomId(room.getId())) {
			games.add(value);
		}

		return games;
	}

	@PostMapping("/room/{roomIdOrAccessToken}/game")
	public Game createGame(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") Optional<String> userUuid,
			@RequestBody Game game)
	{
        Room room;
        if (userUuid.isPresent()) {
            room = validate(userUuid.get(), roomIdOrAccessToken);
        } else {
            room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.empty());
        }

		if (game.getName() == null) {
			ArrayList<Game> games = this.getGamesList(room);
			game.setName("Game #".concat(String.valueOf(games.size() + 1)));
		}

		game.setRoomId(room.getId());
		gameRepository.save(game);

		room.setActiveGameId(game.getId());
		room.setCurrentTask(Optional.empty());
		roomRepository.save(room);

		sender.emitGameActivated(room, game);

		return game;
	}

	@PatchMapping("/room/{roomIdOrAccessToken}/game/{gameId}/activate")
	public void activateGame(
			@PathVariable String roomIdOrAccessToken,
			@PathVariable Integer gameId,
			@RequestParam("user_uuid") String userUuid
	) {
		Room room = validate(userUuid, roomIdOrAccessToken);
		Optional<Game> game = gameRepository.findById(gameId);

		if (!game.isPresent()) {
			throw new GameNotFoundException(gameId);
		}

		if (!room.getId().equals(game.get().getRoomId())) {
			throw new GameDoesNotBelongToRoomException(gameId, roomIdOrAccessToken);
		}

		room.setActiveGameId(gameId);

		Iterator<Task> tasks = taskRepository.findAllByGameId(gameId).iterator();
		if (tasks.hasNext()) {
			room.setCurrentTask(Optional.of(tasks.next()));
		} else {
			room.setCurrentTask(Optional.empty());
		}

		roomRepository.save(room);

        sender.emitGameActivated(room, game.get());
	}

	@GetMapping("/room/{roomIdOrAccessToken}/games/active")
	public Game getActiveGame(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid)
	{
		Room room = validate(userUuid, roomIdOrAccessToken);
		Optional<Game> game = gameRepository.findById(room.getActiveGameId());

		if (!game.isPresent()) {
			throw new GameNotFoundException(room.getActiveGameId());
		}

		return game.get();
	}

	@GetMapping("/room/{roomIdOrAccessToken}/games/archived")
	public List<Game> getArchivedGames(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid)
	{
		Room room = validate(userUuid, roomIdOrAccessToken);

		ArrayList<Game> games = new ArrayList<>();
		for (Game value : gameRepository.findAllByRoomId(room.getId())) {
			if (!room.getActiveGameId().equals(value.getId())) {
				games.add(value);
			}
		}

		return games;
	}

	@GetMapping("/room/{roomIdOrAccessToken}/game/{gameId}/importParameters")
	public String getImportParameters(
			@PathVariable String roomIdOrAccessToken,
            @PathVariable Integer gameId,
			@RequestParam("user_uuid") String userUuid)
	{
		Room room = validate(userUuid, roomIdOrAccessToken);
        Game game = RestUtil.requireGameOfRoom(gameRepository, gameId, room);

        return game.getImportParameters();
	}

	@PutMapping("/room/{roomIdOrAccessToken}/game/{gameId}/importParameters")
	public void putImportParameters(
			@PathVariable String roomIdOrAccessToken,
            @PathVariable Integer gameId,
			@RequestParam("user_uuid") String userUuid,
            @RequestBody String body)
	{
		Room room = validate(userUuid, roomIdOrAccessToken);
        Game game = RestUtil.requireGameOfRoom(gameRepository, gameId, room);

        game.setImportParameters(body);

        gameRepository.save(game);

        sender.emitGamesUpdated(room);
	}

	private Room validate(
			String userUuid,
			String roomIdOrAccessToken
	)
			throws RoomNotFoundException, PermissionDeniedException
	{
        RestUtil.userHasPlayerInRoomOrCreatedTheRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
		return RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
	}
}
