package com.kehrwasser.agilecasino.multimoduleapplication;

public class AvatarAvailability {
    private String avatar;
    private boolean available;

    public AvatarAvailability(String avatar, boolean available) {
        this.avatar = avatar;
        this.available = available;
    }

    public String getAvatar() {
        return avatar;
    }

    public boolean getAvailable() {
        return available;
    }
}
