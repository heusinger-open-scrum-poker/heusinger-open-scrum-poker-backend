package com.kehrwasser.agilecasino.multimoduleapplication.service;

import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.*;

public interface IWebSocketSender {

    public void emitRoomChanged(Room room, RoomChange payload);

    public void emitRoomReset(Room room, Optional<Player> payload);

    public void emitRoomRevealed(Room room, RevealInfo payload);

    public void emitPlayerJoined(Room room, Player payload);

    public void emitPlayerUpdated(Room room, Player payload);

    public void emitPlayerLeft(Room room, LeaveInfo payload);

    public void emitPlayerKicked(Room room, KickInfo payload);

    public void emitPlayerSidelined(Room room, SideliningInfo payload);

    public void emitPlayerPromoted(Room room, PromotionInfo payload);

    public void emitTaskCreated(Room room, Task payload);

    public void emitTaskUpdated(Room room, Task payload);

    public void emitTaskDeleted(Room room, Task payload);

    public void emitCurrentTaskChanged(Room room, Optional<Task> payload);

    public void emitSubtaskCreated(Room room, Subtask payload);

    public void emitSubtaskUpdated(Room room, Subtask payload);

    public void emitSubtaskDeleted(Room room, Subtask payload);

    public void emitGameCreated(Room room, Game payload);

    public static class GamesUpdatedPayload {
        Integer dummyValue;

        public Integer getDummyValue() {
            return dummyValue;
        }

        public void setDummyValue(Integer dummyValue) {
            this.dummyValue = dummyValue;
        }
    }

    public void emitGamesUpdated(Room room);

    public static class GameActivatedPayload {
        private Game game;

        public Game getGame() {
            return game;
        }

        public void setGame(Game game) {
            this.game = game;
        }
    }

    public void emitGameActivated(Room room, Game game);

}
