package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.CreateIssueResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.IdNode;

import java.net.URI;
import java.net.http.HttpRequest;


public class CreateSubtask {

    private JiraOAuth jira;
    private String resourceId;
    private String projectId;
    private String parentId;
    private String issueType;
    private String summary;

    public CreateSubtask(JiraOAuth jira, String resourceId, String projectId, String parentId, String issueType, String summary) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.projectId = projectId;
        this.parentId = parentId;
        this.issueType = issueType;
        this.summary = summary;
    }

    public static class CreateSubtaskRequestBody {
        private CreateSubtaskRequestBodyFields fields;
        public CreateSubtaskRequestBodyFields getFields() {
            return fields;
        }
        public void setFields(CreateSubtaskRequestBodyFields fields) {
            this.fields = fields;
        }   
    }

    public static class CreateSubtaskRequestBodyFields {
        IdNode project;
        IdNode issuetype;
        String summary;
        IdNode parent;
        public IdNode getProject() {
            return project;
        }
        public void setProject(IdNode project) {
            this.project = project;
        }
        public IdNode getIssuetype() {
            return issuetype;
        }
        public void setIssuetype(IdNode issuetype) {
            this.issuetype = issuetype;
        }
        public String getSummary() {
            return summary;
        }
        public void setSummary(String summary) {
            this.summary = summary;
        }
        public IdNode getParent() {
            return parent;
        }
        public void setParent(IdNode parent) {
            this.parent = parent;
        }
    }

    public CreateIssueResponse fetch() {
        CreateSubtaskRequestBody body = makeBody();

        ObjectMapper om = new ObjectMapper();
        String jsonBody;
        try {
            jsonBody = om.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            jsonBody = "{\"error\":\"in createIssue\"}";
            
            e.printStackTrace();
        }

        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/api/3/issue"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .POST(HttpRequest.BodyPublishers.ofString(jsonBody))
            .build();

        
        CreateIssueResponse resp = Util.fetchJson(om, request, new TypeReference<CreateIssueResponse>() {});
        return resp;
    }

    public CreateSubtaskRequestBody makeBody() {
        CreateSubtaskRequestBody body = new CreateSubtaskRequestBody();
        {
            CreateSubtaskRequestBodyFields fields = new CreateSubtaskRequestBodyFields();
            body.setFields(fields);

            IdNode project = new IdNode(projectId);
            fields.setProject(project);

            IdNode issuetype = new IdNode(issueType);
            fields.setIssuetype(issuetype);

            IdNode parent = new IdNode(parentId);
            fields.setParent(parent);

            fields.setSummary(summary);
        }

        return body;
    }
}
