package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;


public class JiraResourceResponse {
    private String id;
    private String url;
    private String name;
    private String[] scopes;
    private String avatarUrl;
    public JiraResourceResponse() {}
    public void setId(String id) {
        this.id = id;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String[] getScopes() {
        return scopes;
    }
    public void setScopes(String[] scopes) {
        this.scopes = scopes;
    }
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
    public String getId() {
        return id;
    }
    public String getUrl() {
        return url;
    }
    public String getAvatarUrl() {
        return avatarUrl;
    }
}
