package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNAUTHORIZED)
public class ExpiredResetTokenException extends RuntimeException {
    public ExpiredResetTokenException() {
        super("reset token expired");
    }
}
