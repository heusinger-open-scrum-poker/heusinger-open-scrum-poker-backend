package com.kehrwasser.agilecasino.multimoduleapplication.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.PlayerRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IPlayerService;


public class PlayerService implements IPlayerService {


    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private RoomRepository roomRepository;

	@Override
	public Optional<Player> findPlayerById(Integer playerId) {
        return playerRepository.findById(playerId);
	}

	@Override
	public Optional<String> findFreeAvatar(Player player) {
        if (player.getType().equals(Player.TYPE_ESTIMATOR)) {
            return Optional.ofNullable(player.getAvatar());
        }

        Integer roomId = player.getRoom().getId();
        Set<String> usedAvatars = roomRepository.findUsedAvatarsByRoomId(roomId).stream().collect(Collectors.toSet());
        String currentAvatar = player.getAvatar();

        if (currentAvatar != null && !usedAvatars.contains(currentAvatar)) {
            return Optional.of(currentAvatar);
        }

        Set<String> availableAvatars = new HashSet<>(Arrays.asList(Room.AVATARS));
        availableAvatars.removeAll(usedAvatars);

        String newAvatar = availableAvatars.iterator().next();
        return Optional.ofNullable(newAvatar);
    }

    @Override
    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public void deletePlayer(Player player) {
        playerRepository.delete(player);
    }
}
