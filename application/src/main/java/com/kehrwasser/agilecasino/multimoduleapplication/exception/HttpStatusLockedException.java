package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.LOCKED, reason="LOCKED")
public class HttpStatusLockedException extends RuntimeException {

    private static final long serialVersionUID = 1994651932670471226L;

}
