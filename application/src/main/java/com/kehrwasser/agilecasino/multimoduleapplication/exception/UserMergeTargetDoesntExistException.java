package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class UserMergeTargetDoesntExistException extends RuntimeException {
    public UserMergeTargetDoesntExistException(String targetUuid) {
        super("merge target doesn't exist: uuid=" + targetUuid);
    }
}
