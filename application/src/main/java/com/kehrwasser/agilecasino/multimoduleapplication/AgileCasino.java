package com.kehrwasser.agilecasino.multimoduleapplication;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank.LexorankService;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.OpenaiApi;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IEmailService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IFeedbackService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IPlayerService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IUserService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.impl.EmailService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.impl.FeedbackService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.impl.PlayerService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.impl.UserService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;
import com.kehrwasser.agilecasino.multimoduleapplication.service.impl.WebSocketSender;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter.Email;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter.EmailBloomFilter;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.EmailValidationException;

@SpringBootApplication(scanBasePackages = "com.kehrwasser.agilecasino")
@EnableAsync
@EnableCaching
public class AgileCasino {

    Logger logger = LoggerFactory.getLogger(AgileCasino.class);

    @Autowired
    AgileCasinoConfiguration config;

    public static void main(String[] args) {
        SpringApplication.run(AgileCasino.class, args);
    }

    @Bean
    public static AgileCasinoConfiguration initConfig() {
        return new AgileCasinoConfiguration();
    }

    @Bean
    public void spawnFlyway() {
        Thread t = new Thread(() -> {
        logger.info("Running Flyway");

        Integer SECOND = 1000;
        boolean firstTry = true;

        while (true) {
            try {
                if (!firstTry) {
                    Thread.sleep(5 * SECOND);
                }

                schemaHotfix();

                Flyway flyway = Flyway.configure()
                    .dataSource(config.getUrl(), config.getUser(), config.getPassword()).load();

                flyway.migrate();

                break;
            } catch (Exception e) {
                logger.error(e.toString());
                logger.warn("Retrying to run Flyway...");
            } finally {
                firstTry = false;
            }
        }
        });
        t.start();
    }

    @Bean
    public AsyncTaskExecutor taskExecutor() {
        AsyncTaskExecutor exec = new ThreadPoolTaskScheduler();
        return exec;
    }

    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
            .registerModule(new Jdk8Module());
    }

    private void schemaHotfix() throws SQLException {
        logger.info("Applying schema hotfix");

        try {
            Connection connection = DriverManager.getConnection(config.getUrl(), config.getUser(), config.getPassword());
            String query1 = "update flyway_schema_history set checksum = -994851101 where script = 'V0___ddl_jpa_creation.sql';";
            String query2 = "update flyway_schema_history set checksum = 1147264913 where script = 'V1___series_and_users.sql';";
            connection.prepareStatement(query1).executeUpdate();
            connection.prepareStatement(query2).executeUpdate();
        }
        catch (SQLException e) {
            logger.debug(e.toString());
        }

    }

    @Bean
    public IWebSocketSender webSocketSender() {
        return new WebSocketSender();
    }

    
    @Bean
    public EmailBloomFilter emailBloomFilter() {
        logger.info("Initializing the Email Bloom Filter");

        EmailBloomFilter filter = new EmailBloomFilter();
        try {
            Connection connection = DriverManager.getConnection(config.getUrl(), config.getUser(), config.getPassword());

            ResultSet results = connection.prepareStatement("SELECT email from auth_method;").executeQuery();

            long count = 0;
            while (results.next()) {
                try {
                    Email email = new Email(results.getString("email"));
                    filter.put(email);
                    count ++;
                } catch (EmailValidationException e) {}
            }
            logger.info("Number of emails added to the bloom filter: " + count);
        }
        catch (SQLException e) {
            logger.debug(e.toString());
        }
        return filter;
    }

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(config.getSmtpHost());
        mailSender.setPort(config.getSmtpPort());
        mailSender.setUsername(config.getSmtpUser());
        mailSender.setPassword(config.getSmtpPassword());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Bean
    public IEmailService emailService() {
        return new EmailService();
    }

    @Bean
    public LexorankService lexorankService() {
        return new LexorankService();
    }

    @Bean
    public OpenaiApi getOpenaiApi() {
        String apiKey = config.getOpenaiApiKey();
        return new OpenaiApi(apiKey, objectMapper());
    }

    @Bean
    public CacheManager cacheManager() {
        ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager();
        return cacheManager;
    }

    @Bean
    public IUserService userService() {
        return new UserService();
    }

    @Bean
    public IFeedbackService feedbackService() {
        return new FeedbackService();
    }

    @Bean
    public IPlayerService playerService() {
        return new PlayerService();
    }
}
