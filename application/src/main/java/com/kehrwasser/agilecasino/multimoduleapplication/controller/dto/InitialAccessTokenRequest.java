package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class InitialAccessTokenRequest {
    private String grant_type;
    private String clientId;
    private String clientSecret;
    private String code;
    private String redirectUri;

    public InitialAccessTokenRequest(String clientId, String clientSecret, String code, String frontendBaseUrl) {
        this.grant_type = "authorization_code";
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.code = code;
        this.redirectUri = frontendBaseUrl + "/submit_jira_auth_code";
    }

    public String getGrant_type() {
        return grant_type;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getCode() {
        return code;
    }

    public String getRedirectUri() {
        return redirectUri;
    }
}
