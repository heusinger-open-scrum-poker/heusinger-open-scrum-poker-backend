package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.GetCustomFields.CustomFieldInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;

public class JiraIssue {
    String id;
    String self;
    String key;
    String issuetypeId;
    String issuetypeSelf;
    Integer timeestimate;
    String estimate;
    // String sprintId;
    // String sprintName;
    // String sprintState;
    JsonNode description;
    String rank;
    String summary;
    List<JiraIssue> subtasks;
    public static JiraIssue fromJsonNode(JsonNode node, CustomFieldInfo customFieldInfo) {
        JiraIssue issue = new JiraIssue();

        issue.id = node.get("id").asText();
        issue.key = node.get("key").asText();
        issue.self = node.get("self").asText();
        JsonNode fields = node.get("fields");
        issue.issuetypeId = Util.optional(fields.get("issuetype")).flatMap((JsonNode it) -> Util.optional(it.get("id"))).map((JsonNode it) -> it.asText()).orElse(null);
        issue.issuetypeSelf = Util.optional(fields.get("issuetype")).flatMap((JsonNode it) -> Util.optional(it.get("self"))).map((JsonNode it) -> it.asText()).orElse(null);
        // JsonNode sprint = fields.get("customfield_10020");
        // issue.sprintId = ;
        // issue.sprintName = ;
        // issue.sprintState = ;

        issue.timeestimate = Util.optional(fields.get("timeestimate")).map(it -> it.asInt()).orElse(null);
        issue.description = fields.get("description");
        issue.summary = Util.optional(fields.get("summary")).map(it -> it.asText()).orElse(null);
        if (customFieldInfo.getRankingField().isPresent()) {
            issue.rank = Util.optional(fields.get(customFieldInfo.getRankingField().get())).map(it -> it.asText()).orElse(null);
        }
        if (customFieldInfo.getEstimateField().isPresent()) {
            issue.estimate = Util.optional(fields.get(customFieldInfo.getEstimateField().get())).map(it -> {
                if (it.isNull()) {
                    return null;
                }
                return it.asText();
            }).orElse(null);
        }

        issue.subtasks = new ArrayList<>();
        Util.optional(fields.get("subtasks")).map(it -> {
            it.elements().forEachRemaining(elt -> {
                issue.subtasks.add(JiraIssue.fromJsonNode(elt, customFieldInfo));
            });
            return 0;
        });

        return issue;
    }
    public String getId() {
        return id;
    }
    public String getSelf() {
        return self;
    }
    public String getKey() {
        return key;
    }
    public String getIssuetypeId() {
        return issuetypeId;
    }
    public String getIssuetypeSelf() {
        return issuetypeSelf;
    }
    public Integer getTimeestimate() {
        return timeestimate;
    }
    public String getEstimate() {
        return estimate;
    }
    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }
    // public String getSprintId() {
    //     return sprintId;
    // }
    // public String getSprintName() {
    //     return sprintName;
    // }
    // public String getSprintState() {
    //     return sprintState;
    // }
    public JsonNode getDescription() {
        return description;
    }
    public String getRank() {
        return rank;
    }
    public String getSummary() {
        return summary;
    }
    public List<JiraIssue> getSubtasks() {
        return subtasks;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setSelf(String self) {
        this.self = self;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public void setIssuetypeId(String issuetypeId) {
        this.issuetypeId = issuetypeId;
    }
    public void setIssuetypeSelf(String issuetypeSelf) {
        this.issuetypeSelf = issuetypeSelf;
    }
    public void setTimeestimate(Integer timeestimate) {
        this.timeestimate = timeestimate;
    }
    public void setDescription(JsonNode description) {
        this.description = description;
    }
    public void setRank(String rank) {
        this.rank = rank;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
    public void setSubtasks(List<JiraIssue> subtasks) {
        this.subtasks = subtasks;
    }
}
