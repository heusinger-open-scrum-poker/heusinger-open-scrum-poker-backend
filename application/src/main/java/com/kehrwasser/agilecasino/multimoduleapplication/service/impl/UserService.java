package com.kehrwasser.agilecasino.multimoduleapplication.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.AuthMethod;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.ImportModalState;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.JoinedRoom;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserPatch;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserProfile;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.AuthRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository.RegisteredUserInfo;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IUserService;

public class UserService implements IUserService {
    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

	@Override
	public Optional<User> findUserByUuid(String uuid) {
        return userRepository.findById(uuid);
	}

	@Override
	public User createUser(Optional<String> invitedByHash) {
        Optional<User> inviter = invitedByHash.flatMap(hash -> findUserByUuid(hash));
        
        if (inviter.isEmpty()) {
            User user = new User();
            return userRepository.save(user);
        }
        
        User user = new User(inviter.get());
        return userRepository.save(user);
	}

	@Override
	public User updateUser(User user, UserPatch patch) {
        if (patch != null && patch.getLanguage().isPresent()) {
            user.setLanguage(patch.getLanguage().get());
        }
        return userRepository.save(user);
	}

	@Override
	public User saveUser(User user) {
        return userRepository.save(user);
	}

	@Override
    public Optional<ImportModalState> getImportModalState(User user) {
        String importModalStateJson = user.getImportModalState();

        if (importModalStateJson == null) {
            return Optional.empty();
        }

        try {
            ImportModalState importModalState = objectMapper.readValue(importModalStateJson, ImportModalState.class);
            return Optional.of(importModalState);
        } catch (Exception e) {
            e.printStackTrace();
            user.setImportModalState(null);
            userRepository.save(user);
            return Optional.empty();
        }
	}

	@Override
    public void setImportModalState(User user, Optional<ImportModalState> importModalState) {
        importModalState.ifPresentOrElse((state) -> {
            try {
                String importModalStateJson = objectMapper.writeValueAsString(importModalState);
                user.setImportModalState(importModalStateJson);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, () -> {
            user.setImportModalState(null);
        });
        userRepository.save(user);
	}

	@Override
	public List<JoinedRoom> findJoinedRooms(User user) {
        List<JoinedRoom> list = new ArrayList<>();
        userRepository.findJoinedRoomsById(user.getUuid()).forEach(list::add);
        return list;
	}

	@Override
	public List<RegisteredUserInfo> findRegisteredUsers() {
        List<RegisteredUserInfo> list = new ArrayList<>();
        userRepository.findRegisteredUsers().forEach(list::add);
        return list;
	}

	@Override
	public Optional<UserProfile> findUserProfile(User user) {
        if (user.isAnonymous()) {
            return Optional.empty();
        }

        String email = "";

        AuthMethod authMethod = user.getAuthMethod();

        if (authMethod != null && authMethod.getAuthType().equals("email/password")) {

            email = authRepository.findEmailPasswordAuthById(authMethod.getId())
                .map(auth -> auth.getEmail())
                .orElse("");

        }
        
        return Optional.of(new UserProfile(email));
	}
}
