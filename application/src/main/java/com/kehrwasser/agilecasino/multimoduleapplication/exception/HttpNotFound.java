package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class HttpNotFound extends RuntimeException {
    public HttpNotFound() {
        super("Not Found");
    }

    public HttpNotFound(String message) {
        super(message);
    }
}
