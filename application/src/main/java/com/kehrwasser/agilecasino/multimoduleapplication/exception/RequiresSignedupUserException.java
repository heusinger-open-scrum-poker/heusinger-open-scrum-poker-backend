package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class RequiresSignedupUserException extends RuntimeException {
    public RequiresSignedupUserException(String userUuid) {
        super("user missing signedup role: " + userUuid);
    }
}
