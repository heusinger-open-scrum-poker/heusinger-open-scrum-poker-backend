package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;

public class LeaveInfo {
    private Player player;

    public LeaveInfo(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return this.player;
    }
}
