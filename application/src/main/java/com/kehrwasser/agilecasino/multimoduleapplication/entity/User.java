package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.zip.CRC32;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user")
public class User {
    public static final String ROLE_ANONYMOUS = "anonymous";
    public static final String ROLE_SIGNEDUP = "signedup";
    public static final String ROLE_PRO = "pro";

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "uuid", nullable = false, updatable = false)
    private String uuid;

    @Column(name = "hash", nullable = false, updatable = false)
    private String hash;

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "language", nullable = true, updatable = true)
    private String language;

    @OneToMany(
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL,
        mappedBy = "user",
        orphanRemoval = true
    )
    private List<SessionToken> sessionTokens = new ArrayList<SessionToken>();

    @OneToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "invited_by_uuid")
    @JsonIgnore
    private User invitedBy;

    @OneToMany(
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL,
        mappedBy = "user"
    )
    @JsonIgnore
    private List<Player> players = new ArrayList<Player>();

    @OneToMany(
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL,
        mappedBy = "created_by"
    )
    @JsonIgnore
    private List<Room> createdRooms = new ArrayList<Room>();

    @OneToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "auth_method", referencedColumnName = "id")
    @JsonIgnore
    private AuthMethod authMethod;

    @Column(name = "role")
    private String role;

    @Column(name = "import_modal_state", columnDefinition = "TEXT")
    @JsonIgnore
    private String importModalState;

    public User() {
        this.uuid = UUID.randomUUID().toString();
        this.hash = computeHash(this.uuid);
        this.createdAt = new Date(System.currentTimeMillis());
        this.invitedBy = null;
        this.role = ROLE_ANONYMOUS;
    }

    public User(User inviter) {
        this.uuid = UUID.randomUUID().toString();
        this.hash = computeHash(this.uuid);
        this.createdAt = new Date(System.currentTimeMillis());
        this.invitedBy = inviter;
        this.role = ROLE_ANONYMOUS;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getHash() {
        return this.hash;
    }

    public List<Player> getPlayers() {
        return this.players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addPlayers(Collection<Player> players) {
        this.players.addAll(players);
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public List<Room> getCreatedRooms() {
        return this.createdRooms;
    }

    public void setCreatedRooms(List<Room> createdRooms) {
        this.createdRooms = createdRooms;
    }

    public void addCreatedRooms(Collection<Room> createdRooms) {
        this.createdRooms.addAll(createdRooms);
    }

    public User getInvitedBy() {
        return this.invitedBy;
    }

    public AuthMethod getAuthMethod() {
        return this.authMethod;
    }

    public void setAuthMethod(AuthMethod authMethod) {
        this.authMethod = authMethod;
    }

    @JsonIgnore
    public List<SessionToken> getSessionTokens() {
        return this.sessionTokens;
    }

    @Override
    public String toString() {
        return "User {" +
            " uuid='" + getUuid() + "'" +
            " hash='" + getHash() + "'" +
            ", players='" + getPlayers() + "'" +
            "}";
    }

    private static String computeHash(String uuid) {
        CRC32 hasher = new CRC32();
        hasher.update(uuid.getBytes());
        return String.format(Locale.US, "%08x", hasher.getValue());
    }

	public void addSessionToken(SessionToken token) {
        sessionTokens.add(token);
	}

    
    @JsonIgnore
    public boolean isAnonymous() {
        return this.getRole().equals(User.ROLE_ANONYMOUS);
    }

    @JsonIgnore
    public boolean isPro() {
        return this.getRole().equals(User.ROLE_PRO);
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRole() {
        if (this.role == null) {
            if (this.getAuthMethod() == null) {
                this.role = User.ROLE_ANONYMOUS;
            } else {
                this.role = User.ROLE_SIGNEDUP;
            }
        }
        
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getImportModalState() {
        return this.importModalState;
    }

    public void setImportModalState(String importModalState) {
        this.importModalState = importModalState;
    }

    @JsonIgnore
    public Optional<String> getEmailAddress() {
        AuthMethod auth = getAuthMethod();
        if (auth == null) {
            return Optional.empty();
        }

        EmailPasswordAuthentication typeEmail = auth.asEmailPasswordAuthentication();
        if (typeEmail != null) {
            return Optional.of(typeEmail.getEmail());
        } else {
            return Optional.empty();
        }
    }
}
