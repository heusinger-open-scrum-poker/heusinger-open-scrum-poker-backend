package com.kehrwasser.agilecasino.multimoduleapplication.service;

import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;


public interface IPlayerService {
    Optional<Player> findPlayerById(Integer playerId);
    /** Finds a free avatar for the player.
     * If the current avatar is available, that one is returned.
     */
    Optional<String> findFreeAvatar(Player player);
    Player savePlayer(Player player);
    void deletePlayer(Player player);
}
