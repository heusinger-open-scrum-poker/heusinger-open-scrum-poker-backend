package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class LoginWrongAuthenticationMethodException extends RuntimeException {
    public LoginWrongAuthenticationMethodException(String method) {
        super("wrong authentication method:" + method);
    }
}
