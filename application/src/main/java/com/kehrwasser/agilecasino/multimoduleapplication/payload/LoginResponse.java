package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.SessionToken;

public class LoginResponse {
    private String uuid;
    private SessionToken token;

    public LoginResponse(String uuid, SessionToken token) {
        this.uuid = uuid;
        this.token = token;
    }

    public String getUuid() {
        return uuid;
    }

    public SessionToken getToken() {
        return token;
    }
}
