package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class NoFreeEstimatesLeftException extends RuntimeException {
    public NoFreeEstimatesLeftException() {
        super("no free ai estimates left");
    }
}
