package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class UserMergeSourceDoesntExistException extends RuntimeException {
    public UserMergeSourceDoesntExistException(String sourceUuid) {
        super("merge source doesn't exist: uuid=" + sourceUuid);
    }
}
