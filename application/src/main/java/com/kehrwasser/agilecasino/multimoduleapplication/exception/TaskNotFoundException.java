package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class TaskNotFoundException extends RuntimeException {
    public TaskNotFoundException(Integer id) {
        super("task not found: id=" + id);
    }

    public TaskNotFoundException(String id) {
        super("task not found: id=" + id);
    }
}
