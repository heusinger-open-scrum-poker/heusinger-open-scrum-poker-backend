package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.JiraUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.CreateIssueResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.IdNode;

import java.net.URI;
import java.net.http.HttpRequest;


public class CreateIssue {

    private JiraOAuth jira;
    private String resourceId;
    private String projectId;
    private String issueType;
    private String descriptionHtml;
    private String summary;

    public CreateIssue(JiraOAuth jira, String resourceId, String projectId, String issueType, String descriptionHtml, String summary) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.projectId = projectId;
        this.issueType = issueType;
        this.descriptionHtml = descriptionHtml;
        this.summary = summary;
    }

    public static class CreateIssueRequestBody {
        private CreateIssueRequestBodyFields fields;
        public CreateIssueRequestBodyFields getFields() {
            return fields;
        }
        public void setFields(CreateIssueRequestBodyFields fields) {
            this.fields = fields;
        }   
    }

    public static class CreateIssueRequestBodyFields {
        JsonNode description;
        public JsonNode getDescription() {
            return description;
        }
        public void setDescription(JsonNode description) {
            this.description = description;
        }
        public IdNode getProject() {
            return project;
        }
        public void setProject(IdNode project) {
            this.project = project;
        }
        public IdNode getIssuetype() {
            return issuetype;
        }
        public void setIssuetype(IdNode issuetype) {
            this.issuetype = issuetype;
        }
        public String getSummary() {
            return summary;
        }
        public void setSummary(String summary) {
            this.summary = summary;
        }
        IdNode project;
        IdNode issuetype;
        String summary;
    }

    public CreateIssueResponse fetch() {
        CreateIssueRequestBody body = makeBody();

        ObjectMapper om = new ObjectMapper();
        String jsonBody;
        try {
            jsonBody = om.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            jsonBody = "{\"error\":\"in createIssue\"}";
            
            e.printStackTrace();
        }

        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/api/3/issue"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .POST(HttpRequest.BodyPublishers.ofString(jsonBody))
            .build();

        
        CreateIssueResponse resp = Util.fetchJson(om, request, new TypeReference<CreateIssueResponse>() {});
        return resp;
    }

    public CreateIssueRequestBody makeBody() {
        CreateIssueRequestBody body = new CreateIssueRequestBody();
        {
            CreateIssueRequestBodyFields fields = new CreateIssueRequestBodyFields();
            body.setFields(fields);

            IdNode project = new IdNode(projectId);
            fields.setProject(project);

            IdNode issuetype = new IdNode(issueType);
            fields.setIssuetype(issuetype);

            fields.setSummary(summary);

            JsonNode descriptionNode = JiraUtil.adfNodeFromHtml(descriptionHtml);
            fields.setDescription(descriptionNode);
        }

        return body;
    }
}
