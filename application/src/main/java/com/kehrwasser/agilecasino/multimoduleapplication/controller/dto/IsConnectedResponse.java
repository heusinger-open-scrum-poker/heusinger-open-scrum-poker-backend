package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;


public class IsConnectedResponse {
    private Boolean isConnected;
    public IsConnectedResponse(boolean isConnected) {
        this.isConnected = isConnected;
    }
    public Boolean getIsConnected() {
        return isConnected;
    }
    public void setIsConnected(Boolean isConnected) {
        this.isConnected = isConnected;
    }
}

