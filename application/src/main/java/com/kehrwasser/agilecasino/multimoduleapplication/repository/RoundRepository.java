package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Round;
import org.springframework.data.repository.CrudRepository;

public interface RoundRepository extends CrudRepository<Round, Integer> {
}
