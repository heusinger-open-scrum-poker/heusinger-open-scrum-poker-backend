package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import org.springframework.data.repository.CrudRepository;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Game;

public interface GameRepository extends CrudRepository<Game, Integer> {
    public Iterable<Game> findAllByRoomId(Integer roomId);
}
