package com.kehrwasser.agilecasino.multimoduleapplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

public class CardSeries {
    static int MAX_CARD_VALUE_LENGTH = 3;

    List<String> cards = new ArrayList<String>();
    boolean hasInf = false;
    boolean hasUnclear = false;
    boolean hasBreak = false;

    public static List<String> RESERVED_CARDS = Arrays.asList(new String[]{
        "inf",
        "unclear",
        "break",
    });

    public static CardSeries FIBONACCI = new CardSeries(Arrays.asList( new String[]{ "1", "2", "3", "5", "8", "13", "21", "34"}), true, true, false);
    public static CardSeries POWER_OF_TWO = new CardSeries(Arrays.asList(new String[]{ "1", "2", "4", "8", "16", "32", "64", ""}), true, true, false);

    CardSeries() {
    }

    CardSeries(List<String> cards, boolean hasBreak, boolean hasUnclear, boolean hasInf) {
        this.cards = cards;
        this.hasInf = hasInf;
        this.hasUnclear = hasUnclear;
        this.hasBreak = hasBreak;
    }

    public String toJSON() {
        Gson gson = new Gson();
        List<String> list = new ArrayList<String>();

        list.addAll(this.cards);

        if (this.hasInf) {
            list.add("inf");
        }

        if (this.hasUnclear) {
            list.add("unclear");
        }

        if (this.hasBreak) {
            list.add("break");
        }
        
        return gson.toJson(list);
    }

    public static CardSeries parse(String json) throws Exception {

        CardSeries series = new CardSeries();
        Gson gson = new Gson();
        String[] parsedCards = gson.fromJson(json, String[].class);

        if (parsedCards == null) {
            throw new Exception("series parsed as null");
        }

        for (int i = 0; i < parsedCards.length; ++i) {
            String card = parsedCards[i];
            if (CardSeries.RESERVED_CARDS.contains(card)) {
                switch (card) {
                    case "break":
                        series.hasBreak = true;
                        break;
                    case "unclear":
                        series.hasUnclear = true;
                        break;
                    case "inf":
                        series.hasInf = true;
                        break;
                }
            } else {
                if (card.length() > MAX_CARD_VALUE_LENGTH)  {
                    throw new Exception("name of " + i + "-th card too long");
                }

                series.cards.add(card);
            }
        }

        return series;
    }
}
