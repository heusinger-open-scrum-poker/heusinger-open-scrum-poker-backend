package com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateIssueResponse {
    private String id;
    private String key;
    private String self;
    private JsonNode transition;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getSelf() {
        return self;
    }
    public void setSelf(String self) {
        this.self = self;
    }
    public JsonNode getTransition() {
        return transition;
    }
    public void setTransition(JsonNode transition) {
        this.transition = transition;
    }
}
