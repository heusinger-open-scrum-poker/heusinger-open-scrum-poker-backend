package com.kehrwasser.agilecasino.multimoduleapplication.openai;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.function.ArrayType;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.function.Function;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.function.FunctionCall;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.function.Message;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.function.ObjectType;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.function.StringType;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.functionResponse.Choice;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.functionResponse.FunctionResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.functionResponse.Usage;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.function.IntegerType;

public class OpenaiApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenaiApi.class);

    private String apiKey;
    private ObjectMapper objectMapper;

    public OpenaiApi(String apiKey, ObjectMapper objectMapper) {
        this.apiKey = apiKey;
        this.objectMapper = objectMapper;
    }

    public class GenerateSubtasksResult {
        private List<SubtaskEstimate> subtasks;
        private Usage usage;
        private String model;

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public List<SubtaskEstimate> getSubtasks() {
            return subtasks;
        }

        public void setSubtasks(List<SubtaskEstimate> subtasks) {
            this.subtasks = subtasks;
        }

        public Usage getUsage() {
            return usage;
        }

        public void setUsage(Usage usage) {
            this.usage = usage;
        }
    }

    public GenerateSubtasksResult generateSubtasks(String summary) {
        FunctionCall functionCall = createFunctionCall(summary);

        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://api.openai.com/v1/chat/completions"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + this.apiKey)
            .POST(Util.toBodyPublisher(objectMapper, functionCall))
            .build();

        FunctionResponse response = Util.fetchJson(objectMapper, request, new TypeReference<FunctionResponse>() {});
        Choice choice = response.getChoices().get(0);
        if (choice == null) {
            throw new RuntimeException("GPT Error");
        }

        try {
			LOGGER.info("GPT USAGE: " + objectMapper.writeValueAsString(response.getUsage()));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

        String answerString = choice.getMessage().getFunction_call().getArguments();

        ResponseMessage answer;
		try {
			answer = objectMapper.readValue(answerString, ResponseMessage.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
            throw new RuntimeException("GPT Error");
		} catch (JsonProcessingException e) {
			e.printStackTrace();
            throw new RuntimeException("GPT Error");
		}

        GenerateSubtasksResult result = new GenerateSubtasksResult();
        result.setSubtasks(answer.getSubtasks());
        result.setUsage(response.getUsage());
        result.setModel(response.getModel());

        return result;
    }

    private FunctionCall createFunctionCall(String summary) {
        FunctionCall functionCall = new FunctionCall();
        Message message = new Message();
        Function function = new Function();
        ObjectType functionSchema = new ObjectType();
        ArrayType subtaskArraySchema = new ArrayType();
        ObjectType subtaskItemSchema = new ObjectType();
        StringType summarySchema = new StringType();
        IntegerType estimateSchema = new IntegerType();


        functionCall.setModel("gpt-3.5-turbo");
        functionCall.addMessage(message);
        message.setRole("user");
        message.setContent("Always output valid JSON. Suggest a sequence of subtasks and estimate their size in order to complete the following task: " + summary);
        functionCall.addFunction(function);
        function.setName("get_subtasks");
        function.setDescription("Suggest a subdivision of the task into subtasks");
        function.setParameters(functionSchema);
        functionSchema.addOptional("subtasks", subtaskArraySchema);
        subtaskArraySchema.setItems(subtaskItemSchema);
        subtaskItemSchema.addOptional("summary", summarySchema);
        summarySchema.setDescription("describe the subtask");
        subtaskItemSchema.addRequired("estimate", estimateSchema);
        estimateSchema.setDescription("estimate the time (in hours) it takes to complete the subtasks");

        return functionCall;
    }
}
