package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class LoginAccountDoesntExistException extends RuntimeException {
    public LoginAccountDoesntExistException(String email) {
        super("no account associated with this email: " + email);
    }
}
