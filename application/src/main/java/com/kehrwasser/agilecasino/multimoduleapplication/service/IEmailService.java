package com.kehrwasser.agilecasino.multimoduleapplication.service;

import javax.mail.MessagingException;

import org.springframework.mail.javamail.MimeMessageHelper;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;

public interface IEmailService {
    public void sendRegistrationEmail(User user) throws MessagingException;

    public void sendPasswordResetEmail(User user, String token) throws MessagingException;

    public MimeMessageHelper makeRegistrationMail(User user) throws MessagingException;

    public MimeMessageHelper makePasswordResetMail(User user, String token) throws MessagingException;
}
