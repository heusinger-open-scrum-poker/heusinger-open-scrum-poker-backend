package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class TaskDoesNotBelongToActiveGameException extends RuntimeException {
    public TaskDoesNotBelongToActiveGameException(Integer id) {
        super("task with id=" + id + " does not belong to active game");
    }

    public TaskDoesNotBelongToActiveGameException(String id) {
        super("task with id=" + id + " does not belong to active game");
    }
}
