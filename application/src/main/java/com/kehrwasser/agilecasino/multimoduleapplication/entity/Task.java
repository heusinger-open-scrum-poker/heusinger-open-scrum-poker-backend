package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank.Rankable;

@Entity()
@Table(name = "task")
public class Task extends Rankable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name = "room_id")
    private Integer roomId;

    @Column(name = "subject")
    private String subject;

    @Column(name = "description")
    private String description;

    @Column(name = "estimate")
    private String estimate;

    @Column(name = "provider")
    private String provider;

    @Column(name = "metadata")
    private String metadata;

    @Column(name = "lexorank")
    private String rank;

    @Column(name = "game_id")
    private Integer gameId;

    public Task() {
        this.subject = "";
        this.description = "";
        this.estimate = "";
        this.provider = null;
        this.metadata = null;
        this.rank = null;
        this.gameId = null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public Optional<String> getProvider() {
        if (provider == null) {
            return Optional.empty();
        } else {
            return Optional.of(provider);
        }
    }

    public void setProvider(Optional<String> provider) {
        if (provider.isEmpty()) {
            this.provider = null;
        } else {
            this.provider = provider.get();
        }
    }

    public Optional<String> getMetadata() {
        if (metadata == null) {
            return Optional.empty();
        } else {
            return Optional.of(metadata);
        }
    }

    public void setMetadata(Optional<String> metadata) {
        if (metadata.isEmpty()) {
            this.metadata = null;
        } else {
            this.metadata = metadata.get();
        }
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Integer getGameId() {
        return this.gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    @Override
    public String toString() {
        ObjectMapper om = new ObjectMapper()
            .registerModule(new Jdk8Module());

        try {
            return om.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            System.err.println(e);
            return "failed to serialize " + super.toString();
        }
    }

    public Task cloneTask() {
        Task clone = new Task();
        clone.setId(getId());
        clone.setSubject(getSubject());
        clone.setEstimate(getEstimate());
        clone.setDescription(getDescription());
        clone.setMetadata(getMetadata());
        clone.setProvider(getProvider());
        clone.setRoomId(getRoomId());
        clone.setGameId(getGameId());
        return clone;
    }
}
