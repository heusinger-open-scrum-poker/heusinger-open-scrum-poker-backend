package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublisher;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.InternalServerException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PermissionDeniedException;

public class Util {
    public static String encodeURIComponent(String text) {
        String encoded = URLEncoder.encode(text, StandardCharsets.UTF_8);
        encoded = encoded.replaceAll("\\+", "%20");
        encoded = encoded.replaceAll("%7E", "~");
        encoded = encoded.replaceAll("%27", "'");
        encoded = encoded.replaceAll("%28", "(");
        encoded = encoded.replaceAll("%29", ")");
        encoded = encoded.replaceAll("%21", "!");
        return encoded;
    }


    public static <JsonBody> JsonBody fetchJson(
        ObjectMapper objectMapper,
        HttpRequest request,
        TypeReference<JsonBody> responseClass)
        throws InternalServerException, PermissionDeniedException
    {
        HttpResponse<String> response = fetchHttp(request);

        JsonBody jsonValue;
        String bodyText = response.body();
        try {
            jsonValue = objectMapper.readValue(bodyText, responseClass);
        } catch (JsonMappingException e) {
            System.err.println(bodyText);
            e.printStackTrace();
            throw new InternalServerException(e.getMessage());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerException(e.getMessage());
        }

        return jsonValue;
    }

    public static <JsonBody> JsonResponse<JsonBody> fetchJson2(
        ObjectMapper objectMapper,
        HttpRequest request,
        TypeReference<JsonBody> responseClass)
        throws InternalServerException, PermissionDeniedException
    {
        HttpResponse<String> httpResponse = fetchHttp(request);

        JsonBody jsonValue;
        String bodyText = httpResponse.body();
        try {
            jsonValue = objectMapper.readValue(bodyText, responseClass);
        } catch (JsonMappingException e) {
            System.err.println(bodyText);
            e.printStackTrace();
            throw new InternalServerException(e.getMessage());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerException(e.getMessage());
        }

        JsonResponse<JsonBody> response = new JsonResponse<>(httpResponse, Optional.ofNullable(jsonValue));
        return response;
    }

    public static HttpResponse<String> fetchHttp(HttpRequest request)
        throws InternalServerException, PermissionDeniedException
    {
        HttpClient client = HttpClient.newHttpClient();
        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException e) {
            throw new InternalServerException(e.getMessage());
        } catch (IOException e) {
            throw new InternalServerException(e.getMessage());
        }

        if (response.statusCode() < 200 || response.statusCode() > 299) {
            throw new PermissionDeniedException(response.body());
        }

        return response;
    }

    public static <JsonBody> BodyPublisher toBodyPublisher(
        ObjectMapper objectMapper,
        JsonBody body)
        throws InternalServerException
    {
        try {
            String bodyJson = objectMapper.writeValueAsString(body);
            return HttpRequest.BodyPublishers.ofString(bodyJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerException(e.getMessage());
        }
    }

    public static Optional<Player> findPlayerOfUser(Room room, String userUuid) {
        return room.getPlayers().stream()
            .filter((Player p) -> p.getUser().map(u -> u.getUuid().equals(userUuid)).orElse(false))
            .findFirst();
    }

    public static <T> Optional<T> optional(T value) {
        if (value == null) {
            return Optional.empty();
        } else {
            return Optional.of(value);
        }
    }
}
