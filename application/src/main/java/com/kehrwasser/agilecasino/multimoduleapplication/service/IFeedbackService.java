package com.kehrwasser.agilecasino.multimoduleapplication.service;

import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Feedback;

public interface IFeedbackService {
    Optional<Feedback> findFeedbackById(Integer feedbackId);
    Feedback createFeedback(Feedback feedback);
    Optional<Feedback> updateFeedback(Feedback feedback);
}
