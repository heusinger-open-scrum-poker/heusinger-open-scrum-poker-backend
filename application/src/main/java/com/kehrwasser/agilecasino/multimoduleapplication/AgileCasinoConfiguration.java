package com.kehrwasser.agilecasino.multimoduleapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgileCasinoConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgileCasinoConfiguration.class);
    private static final double PERFORMANCE_LOGGING_PROBABILITY_DEFAULT = 0.01;

    private String host;
    private Integer port;
    private String database;
    private String user;
    private String password;
    private String adminUser;
    private String adminPassword;
    private String performanceLoggingProbability;
    private String smtpHost;
    private String smtpPort;
    private String smtpUser;
    private String smtpPassword;
    private String smtpSenderAddress;
    private String smtpSenderName;
    private String frontendBaseUrl;
    private String jiraClientId;
    private String jiraSecret;
    private String openaiApiKey;

    public AgileCasinoConfiguration() {
        this.host = getEnv("MYSQL_HOST", "localhost");
        this.port = Integer.parseInt(getEnv("MYSQL_PORT", "3306"));
        this.database = getEnv("MYSQL_DATABASE", getEnv("MYSQL_DB", "db"));
        this.user = getEnv("MYSQL_USER", "user");
        this.password = getEnv("MYSQL_PASSWORD", "password");
        this.adminUser = getEnv("ADMIN_USER", "admin");
        this.adminPassword = getEnv("ADMIN_PASSWORD", "password");
        this.performanceLoggingProbability = getEnv("PERFORMANCE_LOGGING_PROBABILITY", Double.valueOf(PERFORMANCE_LOGGING_PROBABILITY_DEFAULT).toString());
        this.smtpHost = getEnv("SMTP_HOST", "");
        this.smtpPort = getEnv("SMTP_PORT", "");
        this.smtpUser = getEnv("SMTP_USER", "");
        this.smtpPassword = getEnv("SMTP_PASSWORD", "");
        this.smtpSenderAddress = getEnv("SMTP_SENDER_ADDRESS", "");
        this.smtpSenderName = getEnv("SMTP_SENDER_NAME", "Agile Casino");
        this.frontendBaseUrl = getEnv("FRONTEND_BASE_URL", "http://localhost:4200");
        this.jiraClientId = getEnv("JIRA_CLIENT_ID", "");
        this.jiraSecret = getEnv("JIRA_SECRET", "");
        this.openaiApiKey = getEnv("OPENAI_API_KEY", "");
    }

    public String getUrl() {
        return "jdbc:mysql://" + host + ":" + port + "/" + database;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminUser() {
        return adminUser;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    private static String getEnv(String key, String def) {
        String value = System.getenv(key);
        if (value == null) {
            return def;
        } else {
            return value;
        }
    }

    public double getPerformanceLoggingProbability() {
        try {
            return Double.parseDouble(this.performanceLoggingProbability);
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            LOGGER.error("Falling back to default value " + PERFORMANCE_LOGGING_PROBABILITY_DEFAULT);
            return PERFORMANCE_LOGGING_PROBABILITY_DEFAULT;
        }
    }

    public String getSmtpHost() {
        return smtpHost;
    }

    public Integer getSmtpPort() {
        try {
            return Integer.parseUnsignedInt(smtpPort);
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
            Integer defaultPort = 587;
            LOGGER.error("Falling back to default value " + defaultPort);
            return defaultPort;
        }
    }

    public String getSmtpUser() {
        return smtpUser;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public String getSmtpSenderAddress() {
        return smtpSenderAddress;
    }

    public String getSmtpSenderName() {
        return smtpSenderName;
    }

    public String getFrontendBaseUrl() {
        return frontendBaseUrl;
    }

    public String getJiraClientId() {
        return jiraClientId;
    }

    public String getJiraSecret() {
        return jiraSecret;
    }

    public String getOpenaiApiKey() {
        return openaiApiKey;
    }
}
