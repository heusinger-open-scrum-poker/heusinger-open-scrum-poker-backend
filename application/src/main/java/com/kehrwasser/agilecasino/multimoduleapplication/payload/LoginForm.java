package com.kehrwasser.agilecasino.multimoduleapplication.payload;

public class LoginForm {
    private String email;
    private String password;
    private String userUuid;

    public LoginForm() {}

    public LoginForm(String email, String password, String userUuid) {
        this.email = email;
        this.password = password;
        this.userUuid = userUuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

}
