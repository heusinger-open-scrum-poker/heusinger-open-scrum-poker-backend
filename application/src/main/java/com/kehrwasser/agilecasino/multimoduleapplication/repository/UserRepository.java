package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.JoinedRoom;

import java.sql.Date;

import org.springframework.data.jpa.repository.Query;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, String> {

    @Query("from User u where u.hash=:ref")
    public Iterable<User> findByHash(@Param("ref") String ref);


    @Query(
        value = "SELECT user.* FROM user INNER JOIN auth_method WHERE user.uuid=auth_method.user AND auth_method.auth_type='email/password' AND auth_method.email=:email",
        nativeQuery = true
    )
    public Iterable<User> findByEmail(@Param("email") String email);

    @Query(
        value = "SELECT u.uuid AS uuid, u.created_at AS createdAt, a.auth_type AS authType, a.email AS email "
        + "FROM user AS u INNER JOIN auth_method AS a WHERE u.uuid=a.user AND u.role IS NOT NULL AND u.role IS NOT 'anonymous'",
        nativeQuery = true
    )
    public Iterable<RegisteredUserInfo> findRegisteredUsers();

    public interface RegisteredUserInfo {
        public String getUuid();
        public Date getCreatedAt();
        public String getAuthType();
        public String getEmail();
    }

    @Query("SELECT new com.kehrwasser.agilecasino.multimoduleapplication.payload.JoinedRoom(room, player, COUNT(other)) FROM Room room, Player player, Player other WHERE room.id = player.room.id AND room.id = other.room.id AND player.user.uuid = :userUuid GROUP BY room, player")
    public Iterable<JoinedRoom> findJoinedRoomsById(@Param("userUuid") String userUuid);

}
