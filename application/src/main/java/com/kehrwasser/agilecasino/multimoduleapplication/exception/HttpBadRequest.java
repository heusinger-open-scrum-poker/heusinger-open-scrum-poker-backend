package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class HttpBadRequest extends RuntimeException {
    public HttpBadRequest() {
        super("Bad Request");
    }

    public HttpBadRequest(String message) {
        super(message);
    }
}
