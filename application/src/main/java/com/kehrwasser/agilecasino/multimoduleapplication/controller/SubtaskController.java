package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.TaskUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.SubtaskOperation;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.*;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank.LexorankService;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.SubtaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.TaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@CrossOrigin(allowCredentials = "true")
public class SubtaskController {

    Logger logger = LoggerFactory.getLogger(SubtaskController.class);

	@Autowired
	@Lazy
	private RoomRepository roomRepository;

	@Autowired
	@Lazy
	private TaskRepository taskRepository;

	@Autowired
	@Lazy
	private UserRepository userRepository;

	@Autowired
	@Lazy
	private SubtaskRepository subtaskRepository;

	@Autowired
	private IWebSocketSender sender;

	@Autowired
	@Lazy
	private LexorankService lexorankService;

	@GetMapping("/room/{roomIdOrAccessToken}/task/{taskId}/subtasks")
	public List<Subtask> getSubtasks(
			@PathVariable String roomIdOrAccessToken,
			@PathVariable String taskId,
			@RequestParam("user_uuid") String userUuid)
	{
		validate(userUuid, roomIdOrAccessToken, taskId);

        return cacheableFindSubtasks(taskId);
    }

    public List<Subtask> cacheableFindSubtasks(String taskId) {
        ArrayList<Subtask> subtasks = new ArrayList<>();
        for (Subtask value : subtaskRepository.findAllByTaskId(Integer.valueOf(taskId))) {
            subtasks.add(value);
        }

        boolean ok = lexorankService.repairRanks(subtasks);
        if (!ok) {
            subtaskRepository.saveAll(subtasks);
        }
        lexorankService.sortEntitiesByRank(subtasks);

        return subtasks;
    }

    @GetMapping("/room/{roomIdOrAccessToken}/subtask/{subtaskId}")
    public Subtask getSubtask(
			@PathVariable String roomIdOrAccessToken,
			@PathVariable String subtaskId,
			@RequestParam("user_uuid") String userUuid)
	{
		Subtask subtask = RestUtil.requireSubtask(subtaskRepository, subtaskId);
		validate(userUuid, roomIdOrAccessToken, subtask.getTaskId().toString());

		return subtask;
	}

	@PostMapping("/room/{roomIdOrAccessToken}/subtask")
	public Subtask createSubtask(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid,
			@RequestBody Subtask subtask)
	{
		User user = RestUtil.requireUser(userRepository, userUuid);
		if (!user.getRole().equals(User.ROLE_PRO)) {
			throw new PermissionDeniedException("action requires 'pro' role");
		}

		Room room = validate(userUuid, roomIdOrAccessToken, subtask.getTaskId().toString());

        return TaskUtil.createSubtask(subtaskRepository, Optional.of(sender), lexorankService, room, subtask);
	}

	@PutMapping("/room/{roomIdOrAccessToken}/subtask/{subtaskId}")
	public Subtask updateSubtask(
			@PathVariable String roomIdOrAccessToken,
			@PathVariable String subtaskId,
			@RequestParam("user_uuid") String userUuid,
			@RequestBody Subtask update)
	{
		Subtask subtask = RestUtil.requireSubtask(subtaskRepository, subtaskId);
		Room room = validate(userUuid, roomIdOrAccessToken, subtask.getTaskId().toString());

        return TaskUtil.updateSubtask(subtaskRepository, Optional.of(sender), lexorankService, room, subtask, update);
	}

	@DeleteMapping("/room/{roomIdOrAccessToken}/subtask/{subtaskId}")
	public void deleteSubtask(
			@PathVariable String roomIdOrAccessToken,
			@PathVariable String subtaskId,
			@RequestParam("user_uuid") String userUuid
	) {
		Subtask subtask = RestUtil.requireSubtask(subtaskRepository, subtaskId);
		Room room = validate(userUuid, roomIdOrAccessToken, subtask.getTaskId().toString());

        TaskUtil.deleteSubtask(subtaskRepository, Optional.of(sender), room, subtask);
	}

	@PatchMapping("/room/{roomIdOrAccessToken}/subtask-batch")
	public List<Subtask> updateSubtasks(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid,
            @RequestBody List<SubtaskOperation> subtaskOperations
	) {
		RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
		Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));

        List<Subtask> results = TaskUtil.batchUpdateSubtasks(subtaskRepository, Optional.of(sender), lexorankService, room, subtaskOperations);
        return results;
	}

	private Room validate(
			String userUuid,
			String roomIdOrAccessToken,
			String taskId
	)
	throws RoomNotFoundException, TaskNotFoundException, PermissionDeniedException
	{
		RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
		Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
		Task task = RestUtil.requireTask(taskRepository, taskId);
		RestUtil.taskBelongsToRoom(task, room);

		return room;
	}
}
