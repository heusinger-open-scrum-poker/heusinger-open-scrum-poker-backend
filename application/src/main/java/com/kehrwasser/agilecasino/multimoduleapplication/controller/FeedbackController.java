package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Feedback;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.FeedbackNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.PermissionDeniedException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IFeedbackService;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IUserService;

@RestController
@CrossOrigin(allowCredentials = "true")
public class FeedbackController {

    @Autowired
    IFeedbackService feedbackService;

    @Autowired
    IUserService userService;

    @PostMapping("/feedback")
    public Feedback postFeedback(@RequestBody Feedback feedback) {
        return feedbackService.createFeedback(feedback);
    }

    @PatchMapping("/feedback")
    public void patchFeedback(
            @RequestParam("user_uuid") String userUuid,
            @RequestBody Feedback update
            ) throws UserNotFoundException, FeedbackNotFoundException, PermissionDeniedException {

        userService.findUserByUuid(userUuid).orElseThrow(() -> new UserNotFoundException(userUuid));

        feedbackService.findFeedbackById(update.getId())
            .filter(it -> it.getUserUuid() != null && it.getUserUuid().equals(userUuid))
            .orElseThrow(() -> new FeedbackNotFoundException(update.getId()));

        feedbackService.updateFeedback(update);
    }
}
