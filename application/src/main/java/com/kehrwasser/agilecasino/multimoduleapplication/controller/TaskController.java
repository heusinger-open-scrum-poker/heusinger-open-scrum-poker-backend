package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.RestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.TaskUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.TaskOperation;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Game;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank.LexorankService;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.GameRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.SubtaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.TaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.service.IWebSocketSender;

@RestController
@CrossOrigin(allowCredentials = "true")
public class TaskController {
    Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    @Lazy
    private RoomRepository roomRepository;

    @Autowired
    @Lazy
    private TaskRepository taskRepository;

    @Autowired
    @Lazy
    private SubtaskRepository subtaskRepository;

    @Autowired
    @Lazy
    private UserRepository userRepository;

    @Autowired
    private IWebSocketSender sender;

    @Autowired
    @Lazy
    private LexorankService lexorankService;

    @Autowired
    private GameRepository gameRepository;

    @GetMapping("/room/{roomIdOrAccessToken}/tasks")
    public List<Task> getTasks(
            @PathVariable String roomIdOrAccessToken,
            @RequestParam("user_uuid") String userUuid)
            throws RoomNotFoundException, PermissionDeniedException
    {
        return this.loadTasks(roomIdOrAccessToken, userUuid, Optional.empty());
    }

    public static class GameWithTasksAndSubtasks {
        Game game;
        List<TaskWithSubtasks> tasksWithSubtasks;
        public Game getGame() {
            return game;
        }
        public void setGame(Game game) {
            this.game = game;
        }
        public List<TaskWithSubtasks> getTasksWithSubtasks() {
            return tasksWithSubtasks;
        }
        public void setTasksWithSubtasks(List<TaskWithSubtasks> tasksWithSubtasks) {
            this.tasksWithSubtasks = tasksWithSubtasks;
        }
    }

    public static class TaskWithSubtasks {
        Task task;
        List<Subtask> subtasks;
        public Task getTask() {
            return task;
        }
        public void setTask(Task task) {
            this.task = task;
        }
        public List<Subtask> getSubtasks() {
            return subtasks;
        }
        public void setSubtasks(List<Subtask> subtasks) {
            this.subtasks = subtasks;
        }
    }

    @GetMapping("/room/{roomIdOrAccessToken}/games-with-tasks-and-subtasks")
    public List<GameWithTasksAndSubtasks> getTasksWithSubtasks(
            @PathVariable String roomIdOrAccessToken,
            @RequestParam("user_uuid") String userUuid)
            throws RoomNotFoundException, PermissionDeniedException
    {
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);

        Iterable<Game> games = gameRepository.findAllByRoomId(room.getId());

        List<Task> tasks = this.loadTasks(roomIdOrAccessToken, userUuid, Optional.empty());

        List<Subtask> subtasks = new ArrayList<>();
        subtaskRepository.findAllByRoomId(room.getId()).forEach(subtasks::add);

        List<TaskWithSubtasks> tasksWithSubtasks = new ArrayList<>();
        for (Task task: tasks) {
            TaskWithSubtasks taskWithSubtasks = new TaskWithSubtasks();
            taskWithSubtasks.setTask(task);
            taskWithSubtasks.setSubtasks(new ArrayList<>());
            tasksWithSubtasks.add(taskWithSubtasks);

            for (Subtask subtask: subtasks) {
                if (subtask.getTaskId().equals(task.getId())) {
                    taskWithSubtasks.getSubtasks().add(subtask);
                }
            }
        }

        ArrayList<GameWithTasksAndSubtasks> result = new ArrayList<>();
        for (Game game: games) {
            GameWithTasksAndSubtasks gameWithTasksAndSubtasks = new GameWithTasksAndSubtasks();
            gameWithTasksAndSubtasks.setGame(game);
            gameWithTasksAndSubtasks.setTasksWithSubtasks(new ArrayList<>());
            result.add(gameWithTasksAndSubtasks);

            for (TaskWithSubtasks task: tasksWithSubtasks) {
                if (task.getTask().getGameId() != null && task.getTask().getGameId().equals(game.getId())) {
                    gameWithTasksAndSubtasks.getTasksWithSubtasks().add(task);
                }
            }
        }
            
        return result;
    }

    @GetMapping("/room/{roomIdOrAccessToken}/game/{gameId}/tasks")
    public List<Task> getTasksForGame(
            @PathVariable String roomIdOrAccessToken,
            @PathVariable Integer gameId,
            @RequestParam("user_uuid") String userUuid)
            throws RoomNotFoundException, PermissionDeniedException, GameNotFoundException, GameDoesNotBelongToRoomException
    {
        return this.loadTasks(roomIdOrAccessToken, userUuid, Optional.of(gameId));
    }

    private List<Task> loadTasks(
            String roomIdOrAccessToken,
            String userUuid,
            Optional<Integer> gameId
    ) {
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);

        ArrayList<Task> tasks = new ArrayList<>();

        Iterator<Task> it;
        if (gameId.isPresent()) {
            Optional<Game> game = gameRepository.findById(gameId.get());
            if (!game.isPresent()) {
                throw new GameNotFoundException(gameId.get());
            }

            if (!(room.getId().equals(game.get().getRoomId()))) {
                throw new GameDoesNotBelongToRoomException(gameId.get(), roomIdOrAccessToken);
            }

            it = taskRepository.findAllByGameId(gameId.get()).iterator();
        } else {
            it = taskRepository.findAllByRoomId(room.getId()).iterator();
        }

        while (it.hasNext()) {
            Task task = it.next();
            tasks.add(task);
        }

        boolean ok = lexorankService.repairRanks(tasks);

        if (!ok) {
            taskRepository.saveAll(tasks);
        }

        lexorankService.sortEntitiesByRank(tasks);

        return tasks;
    }

    @GetMapping("/room/{roomIdOrAccessToken}/task/{taskId}")
    public Task getTask(
            @PathVariable String roomIdOrAccessToken,
            @PathVariable String taskId,
            @RequestParam("user_uuid") String userUuid)
            throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException, TaskNotFoundException {
        RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
        Task task = RestUtil.requireTask(taskRepository, taskId);

        return task;
    }

    @PostMapping("/room/{roomIdOrAccessToken}/task")
    public Task createTask(
            @PathVariable String roomIdOrAccessToken,
            @RequestParam("user_uuid") String userUuid,
            @RequestBody Task task)
            throws RoomNotFoundException, PermissionDeniedException {

        RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));

        return TaskUtil.createTask(taskRepository, roomRepository, Optional.of(sender), lexorankService, room, task);
    }

    @PutMapping("/room/{roomIdOrAccessToken}/task/{taskId}/makeCurrent")
    public void makeCurrentTask(
            @PathVariable String roomIdOrAccessToken,
            @PathVariable String taskId,
            @RequestParam("user_uuid") String userUuid)
            throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException, TaskNotFoundException, TaskDoesNotBelongToActiveGameException {
        RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        Task task = RestUtil.requireTask(taskRepository, taskId);
        RestUtil.taskBelongsToRoom(task, room);

        if (!task.getGameId().equals(room.getActiveGameId())) {
            throw new TaskDoesNotBelongToActiveGameException(taskId);
        }

        room.setCurrentTask(Optional.of(task));
        roomRepository.save(room);

        sender.emitCurrentTaskChanged(room, Optional.of(task));
    }

    @PutMapping("/room/{roomIdOrAccessToken}/task/{taskId}")
    public Task updateTask(
            @PathVariable String roomIdOrAccessToken,
            @PathVariable String taskId,
            @RequestParam("user_uuid") String userUuid,
            @RequestBody Task update)
            throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException, TaskNotFoundException {
        RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        Task task = RestUtil.requireTask(taskRepository, taskId);
        RestUtil.taskBelongsToRoom(task, room);

        return TaskUtil.updateTask(taskRepository, Optional.of(sender), room, task, update);
    }

    @DeleteMapping("/room/{roomIdOrAccessToken}/task/{taskId}")
    public void deleteTask(
            @PathVariable String roomIdOrAccessToken,
            @PathVariable String taskId,
            @RequestParam("user_uuid") String userUuid)
            throws TaskNotFoundException, PermissionDeniedException {

        RestUtil.userHasPlayerInRoom(roomRepository, userRepository, userUuid, roomIdOrAccessToken);
        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        Task task = RestUtil.requireTask(taskRepository, taskId);
        RestUtil.taskBelongsToRoom(task, room);

        TaskUtil.deleteTask(taskRepository, roomRepository, Optional.of(sender), room, task);
    }

	@PatchMapping("/room/{roomIdOrAccessToken}/task-batch")
	public List<Task> batchUpdateTasksInActiveGame(
			@PathVariable String roomIdOrAccessToken,
			@RequestParam("user_uuid") String userUuid,
            @RequestBody List<TaskOperation> taskOperations
	) {
        if (taskOperations.isEmpty()) {
            return new ArrayList<>();
        }

        Room room = RestUtil.requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));

        List<Task> results = TaskUtil.batchUpdateTasks(taskRepository, subtaskRepository, roomRepository, Optional.of(sender), lexorankService, room, taskOperations);

        return results;
	}

}
