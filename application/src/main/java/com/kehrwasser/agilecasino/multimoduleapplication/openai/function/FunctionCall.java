package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

import java.util.ArrayList;
import java.util.List;

public class FunctionCall {
    private String model = "";
    private List<Message> messages = new ArrayList<>();
    private List<Function> functions = new ArrayList<>();

    public FunctionCall() {}

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessage(Message messages) {
        this.messages.add(messages);
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }

    public void addFunction(Function messages) {
        this.functions.add(messages);
    }
}
