package com.kehrwasser.agilecasino.multimoduleapplication.openai.functionResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {
    private String index = "";
    private String role = "";
    private String content = "";
    private FunctionCall function_call = null;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String message) {
        this.content = message;
    }

    public FunctionCall getFunction_call() {
        return function_call;
    }

    public void setFunction_call(FunctionCall functionCall) {
        this.function_call = functionCall;
    }
}
