package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class PasswordVerificationException extends RuntimeException {
    public PasswordVerificationException() {
        super("wrong password");
    }
}
