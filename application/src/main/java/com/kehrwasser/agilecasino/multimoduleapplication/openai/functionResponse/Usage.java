package com.kehrwasser.agilecasino.multimoduleapplication.openai.functionResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Usage {
    private Integer prompt_tokens = 0;
    private Integer completion_tokens = 0;
    private Integer total_tokens = 0;

    public Integer getPrompt_tokens() {
        return prompt_tokens;
    }
    public void setPrompt_tokens(Integer prompt_tokens) {
        this.prompt_tokens = prompt_tokens;
    }
    public Integer getCompletion_tokens() {
        return completion_tokens;
    }
    public void setCompletion_tokens(Integer completion_tokens) {
        this.completion_tokens = completion_tokens;
    }
    public Integer getTotal_tokens() {
        return total_tokens;
    }
    public void setTotal_tokens(Integer total_tokens) {
        this.total_tokens = total_tokens;
    }
}
