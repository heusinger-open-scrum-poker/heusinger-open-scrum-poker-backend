package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;

public class RoomChange {
    public static String TYPE_PLAYER_CHANGE = "PLAYER_CHANGE";
    public static String TYPE_SERIES_CHANGE = "SERIES_CHANGE";
    public static String TYPE_NAME_CHANGE = "NAME_CHANGE";
    public static String TYPE_EMPTY = "EMPTY";
    public static String TYPE_LOCKED = "LOCKED";
    public static String TYPE_UNLOCKED = "UNLOCKED";

    private String type;
    private Optional<Player> player;

    public RoomChange(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public Optional<Player> getPlayer() {
        return player;
    }

    public static RoomChange seriesChange(Optional<Player> player) {
        RoomChange change = new RoomChange(TYPE_SERIES_CHANGE);
        change.player = player;
        return change;
    }

    public static RoomChange empty() {
        RoomChange change = new RoomChange(TYPE_EMPTY);
        return change;
    }

    public static RoomChange nameChange(Player player) {
        RoomChange change = new RoomChange(TYPE_NAME_CHANGE);
        change.player = Optional.of(player);
        return change;
    }

    public static RoomChange playerChange(Player player) {
        RoomChange change = new RoomChange(TYPE_PLAYER_CHANGE);
        change.player = Optional.of(player);
        return change;
    }

    public static RoomChange locked(Player player) {
        RoomChange change = new RoomChange(TYPE_LOCKED);
        change.player = Optional.of(player);
        return change;
    }

    public static RoomChange unlocked(Player player) {
        RoomChange change = new RoomChange(TYPE_UNLOCKED);
        change.player = Optional.of(player);
        return change;
    }
}
