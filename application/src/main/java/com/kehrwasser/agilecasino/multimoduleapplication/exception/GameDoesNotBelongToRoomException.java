package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.METHOD_NOT_ALLOWED)
public class GameDoesNotBelongToRoomException extends RuntimeException {
    public GameDoesNotBelongToRoomException(Integer gameId, String roomId) {
        super("game with id id=" + gameId + " does not belong to room with id=" + roomId);
    }
}
