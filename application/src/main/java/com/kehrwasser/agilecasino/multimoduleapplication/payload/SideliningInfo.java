package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;

public class SideliningInfo {
    Player player;
    Player sideliningPlayer;
    public SideliningInfo(Player player, Player sideliningPlayer) {
        this.player = player;
        this.sideliningPlayer = sideliningPlayer;
    }
    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }
    public Player getSideliningPlayer() {
        return sideliningPlayer;
    }
    public void setSideliningPlayer(Player sideliningPlayer) {
        this.sideliningPlayer = sideliningPlayer;
    }
}

