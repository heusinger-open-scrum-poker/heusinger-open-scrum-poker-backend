package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;

public class RepositoryUtil {

    public static String getAvailableAvatar(RoomRepository roomRepository, Room room) {

        Set<String> availableAvatars = getAvailableAvatars(roomRepository, room);

        if (availableAvatars.size() == 0) {
            return null;
        }

        return availableAvatars.iterator().next();

    }

    public static Set<String> getAvailableAvatars(RoomRepository roomRepository, Room room) {

        List<String> usedAvatars = roomRepository.findUsedAvatarsByRoomId(room.getId());

        return getAvailableAvatars(roomRepository, usedAvatars);
    }

    public static Set<String> getAvailableAvatars(RoomRepository roomRepository, Collection<String> usedAvatars) {
        Set<String> availableAvatars = new HashSet<>(Arrays.asList(Room.AVATARS));

        availableAvatars.removeAll(usedAvatars);

        return availableAvatars;
    }

    public static boolean isAvailableAvatar(RoomRepository roomRepository, String avatar, Room room) {
        return getAvailableAvatars(roomRepository, room).contains(avatar);
    }

    public static boolean isAvailableName(String name, Room room) {
        return name != null
            && !name.isBlank()
            && room.getPlayers().stream()
            .filter(player -> player.getName().equals(name)).findFirst().isEmpty();
    }
}
