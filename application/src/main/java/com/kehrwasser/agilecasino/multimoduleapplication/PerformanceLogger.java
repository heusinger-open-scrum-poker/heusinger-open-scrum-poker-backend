package com.kehrwasser.agilecasino.multimoduleapplication;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.PerformanceLog;

@Component
@WebFilter("/*")
public class PerformanceLogger implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceLogger.class);

    private static final double LOGGING_PROBABILITY = new AgileCasinoConfiguration().getPerformanceLoggingProbability();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // empty
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        long time = System.currentTimeMillis();

        boolean randomlyPerformLogging = ((double)(time % 1000)) / 1000.0 < LOGGING_PROBABILITY;

        Timestamp startTime = new Timestamp(time);
        try {
            chain.doFilter(req, resp);
        } finally {
            int duration = Long.valueOf(System.currentTimeMillis() - time).intValue();
            PerformanceLog log = new PerformanceLog();
            log.setMethod(((HttpServletRequest) req).getMethod());
            log.setUrl(((HttpServletRequest) req).getRequestURI());
            log.setStartTime(startTime);
            log.setDurationMillis((int) duration);

            if (randomlyPerformLogging) {
                LOGGER.trace("[{}] {} {}: {} ms ", log.getMethod(), log.getUrl() , log.getStartTime(), log.getDurationMillis());
            }
        }
    }

    @Override
    public void destroy() {
        // empty
    }
}
