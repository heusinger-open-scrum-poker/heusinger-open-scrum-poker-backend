package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNAUTHORIZED)
public class HttpUnauthorized extends RuntimeException {
    public HttpUnauthorized() {
        super("Unauthorized");
    }

    public HttpUnauthorized(String message) {
        super(message);
    }
}
