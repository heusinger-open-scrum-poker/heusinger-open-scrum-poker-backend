package com.kehrwasser.agilecasino.multimoduleapplication.openai.function;

public class Message {
    private String role = "";
    private String content = "";
    // TODO: add nullable field function_call

    public Message() {}

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

