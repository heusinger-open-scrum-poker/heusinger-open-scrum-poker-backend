package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(allowCredentials = "true")
public class FeaturesController {
    static String[] FEATURES = {
        "email_password_registration",
        "preserve_break_card",
        "admin_registered_users_list",
        "joined_rooms_list",
        "patch_user",
        "sidelining",
    };

    @GetMapping("/features")
    public String[] getFeatures() {
        return FEATURES;
    }
}

