package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import java.util.Optional;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;

public class RevealInfo {
    private Optional<Player> player;

    public RevealInfo(Optional<Player> player) {
        this.player = player;
    }

    public Optional<Player> getPlayer() {
        return this.player;
    }
}

