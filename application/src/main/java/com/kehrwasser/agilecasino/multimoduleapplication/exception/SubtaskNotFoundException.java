package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class SubtaskNotFoundException extends RuntimeException {
    public SubtaskNotFoundException(Integer id) {
        super("subtask not found: id=" + id);
    }

    public SubtaskNotFoundException(String id) {
        super("subtask not found: id=" + id);
    }
}
