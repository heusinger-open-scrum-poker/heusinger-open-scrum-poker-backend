package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import org.springframework.data.repository.CrudRepository;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.PerformanceLog;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PerformanceLogRepository extends CrudRepository<PerformanceLog, Integer> {

}
