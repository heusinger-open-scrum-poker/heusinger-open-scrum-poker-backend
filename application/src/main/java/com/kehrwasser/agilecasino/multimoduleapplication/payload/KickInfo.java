package com.kehrwasser.agilecasino.multimoduleapplication.payload;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;

public class KickInfo {
    private Player player;
    private Player kickingPlayer;

    public KickInfo(Player player, Player kickingPlayer) {
        this.player = player;
        this.kickingPlayer = kickingPlayer;
    }

    public Player getPlayer() {
        return this.player;
    }

    public Player getKickingPlayer() {
        return this.kickingPlayer;
    }
}
