package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class UserAlreadySignedUpException extends RuntimeException {
    public UserAlreadySignedUpException(String userUuid) {
        super("user already signed-up: user with uuid=" + userUuid);
    }
}
