package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;

public interface SubtaskRepository extends CrudRepository<Subtask, Integer> {
    public Iterable<Subtask> findAllByTaskId(Integer taskId);


    @Query("SELECT subtask FROM Subtask subtask, Task task " +
        "WHERE subtask.taskId = task.id " +
        "AND task.roomId = :roomId")
    public Iterable<Subtask> findAllByRoomId(Integer roomId);
}
