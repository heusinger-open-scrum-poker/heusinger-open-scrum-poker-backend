package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class UserMergeSourceIsRegisteredException extends RuntimeException {
    public UserMergeSourceIsRegisteredException(String sourceUuid) {
        super("merge source is registered: uuid=" + sourceUuid);
    }
}
