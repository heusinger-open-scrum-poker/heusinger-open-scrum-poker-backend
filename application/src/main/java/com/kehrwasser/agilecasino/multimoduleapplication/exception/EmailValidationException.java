package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class EmailValidationException extends RuntimeException {
    public EmailValidationException(String email) {
        super("invalid email address: " + email);
    }
}

