package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "game")
public class Game {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name = "room_id")
    private Integer roomId;

    @Column(name = "name", columnDefinition = "TEXT")
    private String name;

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private Timestamp created_at;

    @Column(name = "import_parameters", columnDefinition = "TEXT")
    @JsonIgnore
    private String importParameters;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return this.roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public String toString() {
        ObjectMapper om = new ObjectMapper()
                .registerModule(new Jdk8Module());

        try {
            return om.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            System.err.println(e);
            return "failed to serialize " + super.toString();
        }
    }

    public String getImportParameters() {
        return importParameters;
    }

    public void setImportParameters(String importParameters) {
        this.importParameters = importParameters;
    }
}
