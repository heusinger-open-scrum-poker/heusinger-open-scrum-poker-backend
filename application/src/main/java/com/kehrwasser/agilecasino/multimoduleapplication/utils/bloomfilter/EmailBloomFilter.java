package com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter;

import com.google.common.hash.BloomFilter;

public class EmailBloomFilter {

    private BloomFilter<Email> filter;

    public EmailBloomFilter() {
        filter = BloomFilter.create(EmailFunnel.INSTANCE, 10000);
    }

    public boolean put(Email email) {
        return filter.put(email);
    } 

    public boolean test(Email email) {
        return filter.test(email);
    } 
}
