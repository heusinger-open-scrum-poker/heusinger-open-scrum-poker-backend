package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank.Rankable;

import javax.persistence.*;

@Entity()
@Table(name = "subtask")
public class Subtask extends Rankable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(name = "task_id")
    private Integer taskId;

    @Column(name = "subject")
    private String subject;

    @Column(name = "checked")
    private Boolean checked;

    @Column(name = "lexorank", columnDefinition = "TEXT")
    private String rank;

    @Column(name = "ai_generated")
    private Boolean aiGenerated;

    @Column(name = "provider", columnDefinition = "TEXT")
    private String provider;

    @Column(name = "metadata", columnDefinition = "TEXT")
    private String metadata;

    public Subtask() {
        this.subject = "";
        this.checked = false;
        this.rank = null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Boolean getAiGenerated() {
        return aiGenerated;
    }

    public void setAiGenerated(Boolean aiGenerated) {
        this.aiGenerated = aiGenerated;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Subtask clone() {
        Subtask clone = new Subtask();
        clone.setId(getId());
        clone.setSubject(getSubject());
        clone.setChecked(getChecked());
        clone.setRank(getRank());
        clone.setTaskId(getTaskId());
        clone.setAiGenerated(getAiGenerated());
        return clone;
    }
}
