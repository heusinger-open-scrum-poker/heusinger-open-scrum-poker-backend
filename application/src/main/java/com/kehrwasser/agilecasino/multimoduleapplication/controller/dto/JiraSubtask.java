package com.kehrwasser.agilecasino.multimoduleapplication.controller.dto;

import com.fasterxml.jackson.databind.JsonNode;

public class JiraSubtask {
    String id;
    String self;
    String key;
    String issuetypeId;
    String issuetypeSelf;
    String summary;
    public static JiraIssue fromJsonNode(JsonNode node) {
        JiraIssue subtask = new JiraIssue();

        subtask.id = node.get("id").asText();
        subtask.key = node.get("key").asText();
        subtask.self = node.get("self").asText();
        JsonNode fields = node.get("fields");
        subtask.issuetypeId = fields.get("issuetype").get("id").asText();
        subtask.issuetypeSelf = fields.get("issuetype").get("self").asText();
        subtask.summary = fields.get("summary").asText();

        return subtask;
    }
    public String getId() {
        return id;
    }
    public String getSelf() {
        return self;
    }
    public String getKey() {
        return key;
    }
    public String getIssuetypeId() {
        return issuetypeId;
    }
    public String getIssuetypeSelf() {
        return issuetypeSelf;
    }
    public String getSummary() {
        return summary;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setSelf(String self) {
        this.self = self;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public void setIssuetypeId(String issuetypeId) {
        this.issuetypeId = issuetypeId;
    }
    public void setIssuetypeSelf(String issuetypeSelf) {
        this.issuetypeSelf = issuetypeSelf;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
}


