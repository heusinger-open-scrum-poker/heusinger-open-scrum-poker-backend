package com.kehrwasser.agilecasino.multimoduleapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class InvalidSeriesJsonException extends RuntimeException {
    public InvalidSeriesJsonException(String msg) {
        super("invalid series JSON: " + msg);
    }
}



