package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import java.net.URI;
import java.net.http.HttpRequest;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.kehrwasser.agilecasino.multimoduleapplication.AgileCasinoConfiguration;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.AiSubtaskController;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Feedback;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Game;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Subtask;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.FeedbackRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.GameRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.JiraOAuthRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.OpenaiUsageRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.SubtaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.TaskRepository;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.UserRepository;

public class RestUtil {
    public static <T> T mandatory(Optional<T> ovalue, String errorMessage) throws PermissionDeniedException {
        if (ovalue.isEmpty()) {
            throw new PermissionDeniedException(errorMessage);
        }

        return ovalue.get();
    }

    public static Room requireRoom(RoomRepository roomRepository, String roomIdOrAccessToken, Optional<String> userUuid) throws RoomNotFoundException {
        try {
            boolean isRoomAccessToken = RestUtil.isRoomAccessToken(roomIdOrAccessToken);

            Optional<Room> room;
            if (isRoomAccessToken) {
                room = roomRepository.findByAccessToken(roomIdOrAccessToken);
            } else {
                room = roomRepository.findById(Integer.parseInt(roomIdOrAccessToken));
            }

            if (room.isEmpty()) {
                throw new RoomNotFoundException(roomIdOrAccessToken);
            }

            if (room.get().isLocked() && !isRoomAccessToken) {
                if (userUuid.isEmpty()) {
                    throw new RoomNotFoundException(roomIdOrAccessToken);
                }

                RestUtil.userHasPlayerInRoom(roomRepository, userUuid.get(), room.get());
            }

            return room.get();

        } catch (NumberFormatException e) {
            throw new RoomNotFoundException(roomIdOrAccessToken);
        }
    }

    public static User requireUser(UserRepository userRepository, String userUuid) throws UserNotFoundException {
        Optional<User> user = userRepository.findById(userUuid);

        if (user.isEmpty()) {
            throw new UserNotFoundException(userUuid);
        }

        return user.get();
    }

    public static User requireNonAnonymousUser(UserRepository userRepository, String userUuid) throws UserNotFoundException {
        User user = requireUser(userRepository, userUuid);

        if (user.isAnonymous()) {
            throw new RequiresSignedupUserException(userUuid);
        }

        return user;
    }

    public static User requireProUser(UserRepository userRepository, String userUuid) throws UserNotFoundException {
        User user = requireUser(userRepository, userUuid);

        if (!user.isPro()) {
            throw new RequiresProUserException(userUuid);
        }

        return user;
    }

    public static User requireUserByEmail(UserRepository userRepository, String email) throws UserNotFoundException {
        try {
            return userRepository.findByEmail(email).iterator().next();
        } catch (NoSuchElementException e) {
            throw new UserNotFoundException(email);
        }
    }

    public static Task requireTask(TaskRepository taskRepository, String taskId) throws TaskNotFoundException {
        try {
            Optional<Task> task = taskRepository.findById(Integer.parseInt(taskId));

            if (task.isEmpty()) {
                throw new TaskNotFoundException("Task with id='" + taskId + "' not found.");
            }

            return task.get();

        } catch (NumberFormatException e) {
            throw new TaskNotFoundException("Task id '" + taskId + "' is invalid.");
        }
    }

    public static void taskBelongsToRoom(Task task, Room room) {
        if (!task.getRoomId().equals(room.getId())) {
            throw new PermissionDeniedException("Task with id=" + task.getId() + " does not belong to room with id=" + room.getId() + ", but to id=" + task.getRoomId());
        }
    }

    public static Subtask requireSubtask(SubtaskRepository subtaskRepository, String subtaskId) throws SubtaskNotFoundException {
        try {
            Optional<Subtask> subtask = subtaskRepository.findById(Integer.parseInt(subtaskId));

            if (subtask.isEmpty()) {
                throw new SubtaskNotFoundException("Subask with id='" + subtaskId + "' not found.");
            }

            return subtask.get();

        } catch (NumberFormatException e) {
            throw new SubtaskNotFoundException("Subask id '" + subtaskId + "' is invalid.");
        }
    }

    public static Player userHasPlayerInRoom(RoomRepository roomRepository, UserRepository userRepository, String userUuid, String roomIdOrAccessToken) throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException {
        Room room = requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        return userHasPlayerInRoom(roomRepository, userUuid, room);
    }

    public static Player userHasPlayerInRoom(RoomRepository roomRepository, String userUuid, Room room) throws PlayerNotFoundException, PermissionDeniedException {

        Optional<Player> player = room.getPlayers()
            .stream()
            .filter((Player p) -> p.getUser().map(u -> u.getUuid().equals(userUuid)).orElse(false))
            .findFirst();

        if (player.isEmpty()) {
            throw new PermissionDeniedException("User with uuid=" + userUuid + " does not have a player in room with id=" + room.getId());
        }

        return player.get();

    }

    public static void userHasPlayerInRoomOrCreatedTheRoom(RoomRepository roomRepository, UserRepository userRepository, String userUuid, String roomIdOrAccessToken) throws RoomNotFoundException, PlayerNotFoundException, PermissionDeniedException {
        Room room = requireRoom(roomRepository, roomIdOrAccessToken, Optional.of(userUuid));
        if (room.getCreatedBy().isPresent() && room.getCreatedBy().get().getUuid().equals(userUuid)) {
            return;
        }
        userHasPlayerInRoom(roomRepository, userUuid, room);
    }

    /// returns a task if the current task changed
    public static Optional<Task> ensureRoomHasACurrentTask(RoomRepository roomRepository, Room room, Task task) throws InvalidDataException {

        if (room.getCurrentTask().isPresent()) {
            return Optional.empty();
        }

        if (!room.getId().equals(task.getRoomId())) {
            throw new InvalidDataException("Task(id=" + task.getId() + ") does not belong to Room(id=" + room.getId() + "), but to Room(id=" + task.getRoomId() + ")");
        }

        room.setCurrentTask(Optional.of(task));
        roomRepository.save(room);

        return Optional.of(task);
    }

    /// returns a task if the current task changed
    public static Optional<Task> ensureRoomHasACurrentTask(RoomRepository roomRepository, TaskRepository taskRepository, Room room) throws InvalidDataException {
        if (room.getCurrentTask().isPresent()) {
            return Optional.empty();
        }

        Iterator<Task> iterator = taskRepository.findAllByRoomId(room.getId()).iterator();

        if (iterator.hasNext()) {
            Task task = iterator.next();
            room.setCurrentTask(Optional.of(task));
            roomRepository.save(room);
            return Optional.of(task);
        } else {
            return Optional.empty();
        }
    }

    public static Feedback requireFeedback(FeedbackRepository feedbackRepository, Integer id) throws FeedbackNotFoundException {
        Optional<Feedback> feedback = feedbackRepository.findById(id);

        if (feedback.isEmpty()) {
            throw new FeedbackNotFoundException(id);
        }

        return feedback.get();
    }

    public static boolean isRoomAccessToken(String roomIdOrAccessToken) {
        try {
            Integer.parseInt(roomIdOrAccessToken);
            return false;
        } catch (NumberFormatException _e) {
            return true;
        }
    }

    public static JiraOAuth requireJira(JiraOAuthRepository jiraOAuthRepository, User user) throws PermissionDeniedException {
        Optional<JiraOAuth> jira = jiraOAuthRepository.findByUser(user.getUuid());

        if (jira.isEmpty()) {
            throw new PermissionDeniedException("no oauth access granted");
        }

        if (jira.get().getExpirationDatetime() == null) {
            throw new PermissionDeniedException("no oauth access granted");
        } 

        Timestamp now = Timestamp.from(Instant.now()); 
        if (now.getTime() > jira.get().getExpirationDatetime().getTime()) {
            return RestUtil.refreshJira(jiraOAuthRepository, jira.get(), user);
        }


        return jira.get();
    }

    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class AccessTokenRequest {
        private String grantType;
        private String clientId;
        private String clientSecret;
        private String refreshToken;
        public AccessTokenRequest(String grantType, String clientId, String clientSecret, String refreshToken) {
            this.grantType = grantType;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.refreshToken = refreshToken;
        }
        public String getGrantType() {
            return grantType;
        }
        public void setGrantType(String grantType) {
            this.grantType = grantType;
        }
        public String getClientId() {
            return clientId;
        }
        public void setClientId(String clientId) {
            this.clientId = clientId;
        }
        public String getClientSecret() {
            return clientSecret;
        }
        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }
        public String getRefreshToken() {
            return refreshToken;
        }
        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }
    }

    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class AccessTokenResponse {
        private String accessToken;
        private Integer expiresIn;
        private String refreshToken;
        private String tokenType;
        private String scope;
        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }
        public void setExpiresIn(Integer expiresIn) {
            this.expiresIn = expiresIn;
        }
        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }
        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }
        public void setScope(String scope) {
            this.scope = scope;
        }
        public String getAccessToken() {
            return accessToken;
        }
        public Integer getExpiresIn() {
            return expiresIn;
        }
        public String getRefreshToken() {
            return refreshToken;
        }
        public String getTokenType() {
            return tokenType;
        }
        public String getScope() {
            return scope;
        }
    }

    public static JiraOAuth refreshJira(JiraOAuthRepository jiraOAuthRepository, JiraOAuth jira, User user) throws PermissionDeniedException {
        AgileCasinoConfiguration config = new AgileCasinoConfiguration();
        String refreshToken = jira.getRefreshToken();

        if (refreshToken == null) {
            throw new PermissionDeniedException("no refresh token");
        }

        ObjectMapper objectMapper = new ObjectMapper();

        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://auth.atlassian.com/oauth/token"))
            .header("Content-Type", "application/json")
            .POST(Util.toBodyPublisher(objectMapper,
                        new AccessTokenRequest(
                            "refresh_token",
                            config.getJiraClientId(),
                            config.getJiraSecret(),
                            jira.getRefreshToken())))
            .build();

        AccessTokenResponse accessToken = Util.fetchJson(objectMapper, request, new TypeReference<AccessTokenResponse>() {});

        Integer MILLIS_PER_SECOND = 1000;
        Timestamp expiresAt = new Timestamp(System.currentTimeMillis() + MILLIS_PER_SECOND * accessToken.getExpiresIn());
        jira.setRefreshToken(accessToken.getRefreshToken());
        jira.setAccessToken(accessToken.getAccessToken());
        jira.setScope(accessToken.getScope());
        jira.setExpirationDatetime(expiresAt);

        jira = jiraOAuthRepository.save(jira);
        return jira;
    }

    public static void requireAvailableAiEstimates(
            OpenaiUsageRepository openaiUsageRepository,
            User user
            ) throws NoFreeEstimatesLeftException
    {
        switch (user.getRole()) {
            case User.ROLE_PRO:
                return;
            case User.ROLE_SIGNEDUP:
                Integer freeAiEstimations = Math.max(0, AiSubtaskController.MAX_FREE_AI_ESTIMATES - openaiUsageRepository.getNumberOfEstimatesForUser(user.getUuid()));
                if (freeAiEstimations > 0) {
                    return;
                }
                throw new NoFreeEstimatesLeftException();
            case User.ROLE_ANONYMOUS:
            default:
                throw new NoFreeEstimatesLeftException();
        }
    }

    public static Game requireActiveGame(GameRepository gameRepository, Room room) throws GameNotFoundException {
        return requireGame(gameRepository, room.getActiveGameId());
    }

    public static Game requireGame(GameRepository gameRepository, Integer gameId) throws GameNotFoundException {
		Optional<Game> game = gameRepository.findById(gameId);

		if (!game.isPresent()) {
			throw new GameNotFoundException(gameId);
		}

        return game.get();
    }

    public static Game requireGameOfRoom(GameRepository gameRepository, Integer gameId, Room room) throws GameNotFoundException {
        Game game = RestUtil.requireGame(gameRepository, gameId);

		if (!room.getId().equals(game.getRoomId())) {
			throw new GameDoesNotBelongToRoomException(gameId, room.getId().toString());
		}

        return game;
    }
}
