package com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter;

import org.apache.commons.validator.routines.EmailValidator;

import com.kehrwasser.agilecasino.multimoduleapplication.exception.EmailValidationException;

public class Email {
    private String email;

    public Email(String email) throws EmailValidationException {
        if (!EmailValidator.getInstance().isValid(email)) {
            throw new EmailValidationException(email);
        }

        this.email = normalize(email);
    }

    public String getEmail() {
        return email;
    } 

    private String normalize(String email) {
        return email.toLowerCase();
    }
}
