package com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank;

import com.github.pravin.raha.lexorank4j.LexoRank;

import java.util.Comparator;
import java.util.List;

public class LexorankService {
    private static final int LEXO_RANK_MAX_SIZE = 255;

    public boolean isValidRank(String rank) {
        try {
            LexoRank.parse(rank);
        } catch (IllegalArgumentException e) {
            return false;
        }

        return true;
    }

    /***
     * Assign rank to unranked elements.
     *
     * @return whether no modifications have taken place
     */
    public boolean repairRanks(List<? extends Rankable> entities) {
        boolean ok = entities.stream().allMatch(
            e -> e.getLexoRank() != null && e.getLexoRank().toString().length() < LEXO_RANK_MAX_SIZE
        );

        if (!ok) {
            sortEntitiesByRank(entities);
            LexoRank rank = LexoRank.middle();
            for (Rankable entity: entities) {
                entity.setLexoRank(rank);
                rank = rank.genNext();
            }
        }

        return ok;
    }

    public void sortEntitiesByRank(List<? extends Rankable> entities) {
        entities.sort(Comparator.comparing(this::getLexoRankSafe));
    }

    private LexoRank getLexoRankSafe(Rankable entity) {
        if (entity == null || entity.getLexoRank() == null) {
            return LexoRank.max();
        } else {
            return entity.getLexoRank();
        }
    }
}
