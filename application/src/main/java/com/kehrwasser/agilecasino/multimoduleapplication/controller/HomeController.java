package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import java.util.HashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(allowCredentials = "true")
public class HomeController {

    @GetMapping("/")
    public HashMap<String, String> showHomepage() {

        HashMap<String, String> map = new HashMap<>();
        map.put("app", "Open Scrum Poker Backend");
        map.put("author", "Kevin Heusinger");

        return map;

    }

}
