package com.kehrwasser.agilecasino.multimoduleapplication.jira.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.JiraOAuth;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.types.IssueType;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.List;


public class GetIssueTypes {

    private JiraOAuth jira;
    private String resourceId;
    private String projectIdOrKey;

    public GetIssueTypes(JiraOAuth jira, String resourceId, String projectIdOrKey) {
        this.jira = jira;
        this.resourceId = resourceId;
        this.projectIdOrKey = projectIdOrKey;
    }

    public static class IssueTypesPagination {
        private Integer startAt;
        private Integer maxResults;
        private Integer total;
        private List<IssueType> issueTypes;
        public Integer getStartAt() {
            return startAt;
        }
        public void setStartAt(Integer startAt) {
            this.startAt = startAt;
        }
        public Integer getMaxResults() {
            return maxResults;
        }
        public void setMaxResults(Integer maxResults) {
            this.maxResults = maxResults;
        }
        public Integer getTotal() {
            return total;
        }
        public void setTotal(Integer total) {
            this.total = total;
        }
        public List<IssueType> getIssueTypes() {
            return issueTypes;
        }
        public void setIssueTypes(List<IssueType> issueTypes) {
            this.issueTypes = issueTypes;
        }
    }

    public List<IssueType> fetch() {
        HttpRequest request = HttpRequest
            .newBuilder()
            .uri(URI.create("https://api.atlassian.com/ex/jira/"
                        + resourceId
                        + "/rest/api/3/issue/createmeta/"
                        + projectIdOrKey
                        + "/issuetypes"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + jira.getAccessToken())
            .GET()
            .build();


        ObjectMapper om = new ObjectMapper();
        IssueTypesPagination resp = Util.fetchJson(om, request, new TypeReference<IssueTypesPagination>() {});
        return resp.getIssueTypes();
    }
}
