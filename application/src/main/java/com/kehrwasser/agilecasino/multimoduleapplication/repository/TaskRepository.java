package com.kehrwasser.agilecasino.multimoduleapplication.repository;

import org.springframework.data.repository.CrudRepository;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;

public interface TaskRepository extends CrudRepository<Task, Integer> {
    public Iterable<Task> findAllByRoomId(Integer roomId);

    public Iterable<Task> findAllByGameId(Integer gameId);
}
