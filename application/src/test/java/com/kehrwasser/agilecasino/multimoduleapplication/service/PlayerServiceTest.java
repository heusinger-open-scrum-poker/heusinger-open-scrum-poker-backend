package com.kehrwasser.agilecasino.multimoduleapplication.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Player;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Room;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.RoomRepository;

@SpringBootTest()
@Transactional
public class PlayerServiceTest {

    @Autowired
    ObjectMapper om;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    IPlayerService playerService;

    @Autowired
    private EntityManager entityManager;

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }


    @Test
    void currentAvatarIsFree() {
        Room room = new Room();
        String avatarA = Room.AVATARS[0];
        String avatarB = Room.AVATARS[1];
        Player playerA = new Player();
        playerA.setName("A");
        playerA.setType(Player.TYPE_ESTIMATOR);
        playerA.setAvatar(avatarA);
        Player playerB = new Player();
        playerB.setName("B");
        playerB.setType(Player.TYPE_ESTIMATOR);
        playerB.setAvatar(avatarB);

        room.addPlayer(playerA);
        room.addPlayer(playerB);

        room = roomRepository.save(room);
        playerA = playerService.savePlayer(playerA);
        playerB = playerService.savePlayer(playerB);

        resetEntities();

        Optional<String> avatar = playerService.findFreeAvatar(playerB);
        assertThat(avatar.isPresent());

        avatar.ifPresent(it -> {
            assertThat(it).isEqualTo(avatarB);
        });
    }


    @Test
    void currentAvatarIsNotFree() {
        Room room = new Room();
        String avatarA = Room.AVATARS[0];
        Player playerA = new Player();
        playerA.setName("A");
        playerA.setType(Player.TYPE_INSPECTOR);
        playerA.setAvatar(avatarA);
        Player playerB = new Player();
        playerB.setName("B");
        playerB.setType(Player.TYPE_ESTIMATOR);
        playerB.setAvatar(avatarA);

        room.addPlayer(playerA);
        room.addPlayer(playerB);

        room = roomRepository.save(room);
        playerA = playerService.savePlayer(playerA);
        playerB = playerService.savePlayer(playerB);

        resetEntities();

        Optional<String> avatar = playerService.findFreeAvatar(playerA);
        assertThat(avatar.isPresent());

        avatar.ifPresent(it -> {
            assertThat(it).isNotEqualTo(avatar);
        });
    }
}
