package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest()
public class PlayerTest {

    @Test
    public void testSettingTypeToInspector() {

        Player player = new Player();
        String type = new String("inspector");

        try {
            player.setType(type);
        } catch (Exception e) {
            System.out.println(e);
        }

        assertThat(player.getType()).isEqualTo(type);

    }

    @Test
    public void testSettingTypeToEstimator() {

        Player player = new Player();
        String type = new String("estimator");

        try {
            player.setType(type);
        } catch (Exception e) {
            System.out.println(e);
        }

        assertThat(player.getType()).isEqualTo(type);

    }

    @Test
    public void testSettingTypeToNeither() {

        Player player = new Player();

        try {
            player.setType(new String("SOMETHING"));
        } catch (Exception e) {
            System.out.println(e);
        }

        assertThat(player.getType()).isEqualTo("inspector");

    }

}
