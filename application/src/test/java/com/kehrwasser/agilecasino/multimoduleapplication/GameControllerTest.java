package com.kehrwasser.agilecasino.multimoduleapplication;

import com.kehrwasser.agilecasino.multimoduleapplication.exception.*;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.*;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
@Transactional
public class GameControllerTest {
    @Autowired
    RoomController roomController;

    @Autowired
    GameController gameController;

    @Autowired
    UserController userController;

    @Autowired
    TaskController taskController;

    @Autowired
    GameRepository gameRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    private EntityManager entityManager;

    private static class RoomWithGames {
        public Room room;
        public User user;
        public Player player;
        public List<Game> games;
    }

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }

    private RoomWithGames setupRoomWithGames() {
        RoomWithGames rwg = new RoomWithGames();

        rwg.user = userController.postUser(Optional.empty());
        rwg.user.setRole(User.ROLE_ANONYMOUS);
        Optional<String> userUuid = Optional.of(rwg.user.getUuid());

        rwg.room = roomController.createNewRoom(userUuid);
        String roomId = rwg.room.getId().toString();

        Player player = new Player();
        player.setName("abc");
        rwg.player = roomController.createPlayersOfRoom(roomId, player, userUuid);

        rwg.games = new ArrayList<>();

        Game game1 = new Game();
        game1.setName("A");
        game1 = gameController.createGame(roomId, userUuid, game1);
        rwg.games.add(game1);

        Game game2 = new Game();
        game2.setName("B");
        game2 = gameController.createGame(roomId, userUuid, game2);
        rwg.games.add(game2);

        resetEntities();

        return rwg;
    }

    @Test()
    void canCreateGame() {
        RoomWithGames rwg = setupRoomWithGames();

        assertThat(rwg.games.get(0)).isNotNull();
        assertThat(rwg.games.get(0).getId()).isNotEqualTo(0);
    }

    @Test()
    void nameIsAutomaticallySetIfNotGiven() {
        RoomWithGames rwg = setupRoomWithGames();

        Game game = new Game();
        game = gameController.createGame(rwg.room.getId().toString(), Optional.of(rwg.user.getUuid()), game);

        assertThat(game.getName()).isEqualTo("Game #4");
    }

    @Test()
    void newGameHasNoTaskSelected() {
        RoomWithGames rwg = setupRoomWithGames();

        assertThat(rwg.room.getCurrentTask().isPresent()).isFalse();
    }

    @Test()
    void newGameIsActive() {
        RoomWithGames rwg = setupRoomWithGames();

        Optional<Game> activeGame = gameRepository.findById(rwg.room.getActiveGameId());

        assertThat(activeGame.isPresent()).isTrue();
        assertThat(activeGame.get().getName()).isEqualTo("B");

        Game game = new Game();
        game.setName("C");

        gameController.createGame(rwg.room.getId().toString(), Optional.of(rwg.user.getUuid()), game);

        Optional<Room> room = roomRepository.findById(rwg.room.getId());
        assertThat(room.isPresent()).isTrue();
        activeGame = gameRepository.findById(room.get().getActiveGameId());

        assertThat(activeGame.isPresent()).isTrue();
        assertThat(activeGame.get().getName()).isEqualTo("C");
    }

    @Test()
    void newTaskBelongsToActiveGame() {
        RoomWithGames rwg = setupRoomWithGames();

        Task task = new Task();
        task.setSubject("some subject");
        task.setDescription("some description");
        task.setEstimate("");
        task = taskController.createTask(rwg.room.getId().toString(), rwg.user.getUuid(), task);

        assertThat(task.getGameId()).isEqualTo(rwg.room.getActiveGameId());
    }

    @Test()
    void cannotCreateGameIfWrongUser() {
        RoomWithGames rwg = setupRoomWithGames();
        
        Game game = new Game();
        game.setName("C");

        Exception e1 = null;
        try {
            gameController.createGame(rwg.room.getId().toString(), Optional.of("Wrong user id"), game);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void cannotCreateGameIfWrongRoom() {
        RoomWithGames rwg = setupRoomWithGames();

        Game game = new Game();
        game.setName("C");

        Exception e1 = null;
        try {
            gameController.createGame("wrong room id", Optional.of(rwg.user.getUuid()), game);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void canGetGames() {
        RoomWithGames rwg = setupRoomWithGames();

        List<Game> games = gameController.getGames(rwg.room.getId().toString(), rwg.user.getUuid());

        assertThat(games.size()).isEqualTo(3);
        assertThat(games.get(0).getName()).isEqualTo("Game #1");
        assertThat(games.get(1).getName()).isEqualTo("A");
        assertThat(games.get(2).getName()).isEqualTo("B");
    }

    @Test()
    void cannotGetGamesIfWrongUser() {
        RoomWithGames rwg = setupRoomWithGames();

        Exception e1 = null;
        try {
            gameController.getGames(rwg.room.getId().toString(), "Wrong user id");
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void cannotGetGamesIfWrongRoom() {
        RoomWithGames rwg = setupRoomWithGames();

        Exception e1 = null;
        try {
            gameController.getGames("wrong room id", rwg.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void canActivateGame() {
        RoomWithGames rwg = setupRoomWithGames();

        Optional<Game> activeGame = gameRepository.findById(rwg.room.getActiveGameId());
        assertThat(activeGame.isPresent()).isTrue();
        assertThat(activeGame.get().getName()).isEqualTo("B");

        gameController.activateGame(rwg.room.getId().toString(), rwg.games.get(0).getId(), rwg.user.getUuid());

        Optional<Room> room = roomRepository.findById(rwg.room.getId());
        assertThat(room.isPresent()).isTrue();
        activeGame = gameRepository.findById(room.get().getActiveGameId());
        assertThat(activeGame.isPresent()).isTrue();
        assertThat(activeGame.get().getName()).isEqualTo("A");
    }

    @Test()
    void currentTaskIsSetUponActivation() {
        RoomWithGames rwg = setupRoomWithGames();

        Task task = new Task();
        task.setSubject("some subject");
        task.setDescription("some description");
        task.setEstimate("");
        taskController.createTask(rwg.room.getId().toString(), rwg.user.getUuid(), task);

        Optional<Game> activeGame = gameRepository.findById(rwg.room.getActiveGameId());
        assertThat(activeGame.isPresent()).isTrue();
        assertThat(activeGame.get().getName()).isEqualTo("B");

        // first task in game is current task
        Optional<Room> room = roomRepository.findById(rwg.room.getId());
        assertThat(room.isPresent()).isTrue();
        Optional<Task> currentTask = room.get().getCurrentTask();
        assertThat(currentTask.isPresent()).isTrue();
        assertThat(currentTask.get().getSubject()).isEqualTo("some subject");

        gameController.activateGame(rwg.room.getId().toString(), rwg.games.get(0).getId(), rwg.user.getUuid());
        activeGame = Optional.of(rwg.games.get(0));

        // current task is empty if game has no tasks
        assertThat(activeGame.isPresent()).isTrue();
        assertThat(activeGame.get().getName()).isEqualTo("A");

        currentTask = room.get().getCurrentTask();
        assertThat(currentTask.isPresent()).isFalse();

        // switch back to game 'B' and check current task
        gameController.activateGame(rwg.room.getId().toString(), rwg.games.get(1).getId(), rwg.user.getUuid());
        activeGame = Optional.of(rwg.games.get(1));

        assertThat(activeGame.isPresent()).isTrue();
        assertThat(activeGame.get().getName()).isEqualTo("B");

        currentTask = room.get().getCurrentTask();
        assertThat(currentTask.isPresent()).isTrue();
        assertThat(currentTask.get().getSubject()).isEqualTo("some subject");
    }

    @Test()
    void canNotActivateNonexistentGame() {
        RoomWithGames rwg = setupRoomWithGames();

        Exception e1 = null;
        try {
            gameController.activateGame(rwg.room.getId().toString(), -1, rwg.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(GameNotFoundException.class);
    }

    @Test()
    void canNotActivateGameThatBelongsToOtherRoom() {
        RoomWithGames rwg = setupRoomWithGames();

        Optional<String> userUuid = Optional.of(rwg.user.getUuid());
        Room otherRoom = roomController.createNewRoom(userUuid);

        Exception e1 = null;
        try {
            Game game = rwg.games.get(0);
            gameController.activateGame(otherRoom.getId().toString(), game.getId(), rwg.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(GameDoesNotBelongToRoomException.class);
    }

    @Test()
    void canGetActiveGame() {
        RoomWithGames rwg = setupRoomWithGames();

        Game game = gameController.getActiveGame(rwg.room.getId().toString(), rwg.user.getUuid());

        assertThat(game.getName()).isEqualTo("B");
    }

    @Test()
    void cannotGetActiveGameIfWrongRoom() {
        RoomWithGames rwg = setupRoomWithGames();

        Exception e1 = null;
        try {
            gameController.getActiveGame("wrong room id", rwg.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void cannotGetActiveGameIfWrongUser() {
        RoomWithGames rwg = setupRoomWithGames();

        Exception e1 = null;
        try {
            gameController.getActiveGame(rwg.room.getId().toString(), "wrong user id");
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void canGetArchivedGames() {
        RoomWithGames rwg = setupRoomWithGames();

        Optional<String> userUuid = Optional.of(rwg.user.getUuid());

        Game game3 = new Game();
        game3.setName("C");
        game3 = gameController.createGame(rwg.room.getId().toString(), userUuid, game3);
        rwg.games.add(game3);

        List<Game> games = gameController.getArchivedGames(rwg.room.getId().toString(), rwg.user.getUuid());

        assertThat(games.size()).isEqualTo(3);
        assertThat(games.get(0).getName()).isEqualTo("Game #1");
        assertThat(games.get(1).getName()).isEqualTo("A");
        assertThat(games.get(2).getName()).isEqualTo("B");

        gameController.activateGame(rwg.room.getId().toString(), games.get(1).getId(), rwg.user.getUuid());
        games = gameController.getArchivedGames(rwg.room.getId().toString(), rwg.user.getUuid());

        assertThat(games.size()).isEqualTo(3);
        assertThat(games.get(0).getName()).isEqualTo("Game #1");
        assertThat(games.get(1).getName()).isEqualTo("B");
        assertThat(games.get(2).getName()).isEqualTo("C");
    }
}
