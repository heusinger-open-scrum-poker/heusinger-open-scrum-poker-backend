package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.atlassian.adf.model.node.Doc;
import static com.atlassian.adf.model.mark.Strong.strong;
import static com.atlassian.adf.model.node.Doc.doc;
import static com.atlassian.adf.model.node.Paragraph.p;
import static com.atlassian.adf.model.node.Rule.hr;
import static com.atlassian.adf.model.node.Text.text;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.JiraUtil;

@SpringBootTest()
public class AdfSerializationTest {
    @Autowired
    ObjectMapper om;

    @Test
    public void fromDoc() {
        Doc doc = doc(
            p("Hello"),
            hr(),
            p(
                text("world", strong()),
                text("!")
            )
        );

        String intermediate = JiraUtil.jsonFromDoc(doc);

        JsonProcessingException e0 = null;
        String json = null;
        try {
            JsonNode node = JiraUtil.nodeFromJson(intermediate);
            json = om.writeValueAsString(node);
        } catch (JsonProcessingException e) {
            e0 = e;
        }

        assertThat(e0).isNull();
        assertEquals(intermediate, json);
        assertEquals("{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"paragraph\",\"content\":[{\"type\":\"text\",\"text\":\"Hello\"}]},{\"type\":\"rule\"},{\"type\":\"paragraph\",\"content\":[{\"type\":\"text\",\"text\":\"world\",\"marks\":[{\"type\":\"strong\"}]},{\"type\":\"text\",\"text\":\"!\"}]}]}", json);

    }

    @Test
    public void fromHtml() {
        String html = "<p>Hello</p><hr><p><strong>world</strong>!</p>";

        JsonNode node = JiraUtil.adfNodeFromHtml(html);

        JsonProcessingException e0 = null;
        String json = null;
        try {
            json = om.writeValueAsString(node);
        } catch (JsonProcessingException e) {
            e0 = e;
        }

        assertEquals(e0, null);
        assertEquals("{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"paragraph\",\"content\":[{\"type\":\"text\",\"text\":\"Hello\"}]},{\"type\":\"rule\"},{\"type\":\"paragraph\",\"content\":[{\"type\":\"text\",\"text\":\"world\",\"marks\":[{\"type\":\"strong\"}]},{\"type\":\"text\",\"text\":\"!\"}]}]}", json);
    }
}
