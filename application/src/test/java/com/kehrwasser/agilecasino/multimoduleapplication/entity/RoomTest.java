package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest()
public class RoomTest {
    @Autowired
    ObjectMapper om;

    @Test
    public void testSettingToEstimatingTwice() {

        Room room = new Room();
        Exception exception = null;

        try {
            room.setEstimating();
        } catch (Exception e) {
            exception = e;
        }

        assertThat(exception).isNotNull();

    }

    @Test
    public void testSettingToRevealedWorks() {

        Room room = new Room();

        try {
            room.setRevealed();
        } catch (Exception e) {
        }

        assertThat(room.getStatus()).isEqualTo("revealed");

    }

    @Test
    public void testSettingToRevealedTwice() {

        Room room = new Room();
        Exception exception1 = null;
        Exception exception2 = null;

        try {
            room.setRevealed();
        } catch (Exception e) {
            exception1 = e;
        }

        try {
            room.setRevealed();
        } catch (Exception e) {
            exception2 = e;
        }

        assertThat(exception1).isNull();
        assertThat(exception2).isNotNull();

    }


    @Test()
    public void generateJson() {
        Room room = new Room();
        room.setId(10);
        room.setCreated_at(new Timestamp(1234567));


        JsonProcessingException exn = null; 
        String json = null;
        try {
            json = om.writeValueAsString(room);
        } catch (JsonProcessingException e) {
            exn = e;
        }

        assertThat(exn).isNull();
        assertThat(json).isNotNull();
        assertThat(json).isEqualTo("{\"id\":10,\"status\":\"estimating\",\"maxPlayers\":0,\"roundsPlayed\":0,\"created_at\":1234567,\"series\":\"[\\\"1\\\",\\\"2\\\",\\\"3\\\",\\\"5\\\",\\\"8\\\",\\\"13\\\",\\\"21\\\",\\\"34\\\",\\\"unclear\\\",\\\"break\\\"]\",\"accessToken\":null,\"activeGameId\":null,\"locked\":false,\"currentTaskId\":null}");
    }

}
