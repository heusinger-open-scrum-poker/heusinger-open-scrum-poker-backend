package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest()
public class SubtaskTest {
    @Autowired
    ObjectMapper om;

    @Test()
    public void generateJson() {
        Subtask subtask = new Subtask();
        Task task = new Task();
        task.setId(10);
        subtask.setTaskId(task.getId());
        subtask.setSubject("A");
        subtask.setChecked(true);
        subtask.setRank("some-rank");
        subtask.setProvider("some-provider");
        subtask.setMetadata("some-metadata");

        JsonProcessingException exn = null;
        String subtaskJson = null;
        try {
            subtaskJson = om.writeValueAsString(subtask);
        } catch (JsonProcessingException e) {
            exn = e;
        }

        assertThat(exn).isNull();
        assertThat(subtaskJson).isNotNull();
        assertThat(subtaskJson).isEqualTo("{\"id\":null,\"taskId\":10,\"subject\":\"A\",\"checked\":true,\"rank\":\"some-rank\",\"aiGenerated\":null,\"provider\":\"some-provider\",\"metadata\":\"some-metadata\"}");
    }

    @Test()
    public void parseJson() {
        String json = "{\"id\":2549,\"taskId\":783,\"subject\":\"asdf\",\"checked\":true}";
        Subtask subtask = null;
        Exception e1 = null;

        try {
            subtask = om.readValue(json, Subtask.class);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isNull();
        assertThat(subtask).isNotNull();
        assertThat(subtask.getId()).isEqualTo(2549);
        assertThat(subtask.getTaskId()).isEqualTo(783);
        assertThat(subtask.getSubject()).isEqualTo("asdf");
        assertThat(subtask.getChecked()).isEqualTo(true);
    }
}
