package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest()
public class TaskTest {
    @Autowired
    ObjectMapper om;

    @Test()
    public void generateJson() {
        Task task = new Task();
        Room room = new Room();
        room.setId(10);
        task.setRoomId(room.getId());
        task.setSubject("A");
        task.setDescription("B");
        task.setEstimate("C");



        JsonProcessingException exn = null; 
        String taskJson = null;
        try {
            taskJson = om.writeValueAsString(task);
        } catch (JsonProcessingException e) {
            exn = e;
        }

        assertThat(exn).isNull();
        assertThat(taskJson).isNotNull();
        assertThat(taskJson).isEqualTo("{\"id\":null,\"roomId\":10,\"subject\":\"A\",\"description\":\"B\",\"estimate\":\"C\",\"provider\":null,\"metadata\":null,\"rank\":null,\"gameId\":null}");
    }

    @Test()
    public void parseJson() {
        String json = "{\"id\":2549,\"roomId\":783,\"subject\":\"asdf\",\"description\":\"asdfc\",\"estimate\":\"asdfs\",\"provider\":null,\"metadata\":null}";
        Task task = null;
        Exception e1 = null;

        try {
            task = om.readValue(json, Task.class);
        } catch (Exception e) {
            e1 = e;
        }
        
        assertThat(e1).isNull();
        assertThat(task).isNotNull();
        assertThat(task.getId()).isEqualTo(2549);
        assertThat(task.getRoomId()).isEqualTo(783);
        assertThat(task.getSubject()).isEqualTo("asdf");
        assertThat(task.getEstimate()).isEqualTo("asdfs");
        assertThat(task.getDescription()).isEqualTo("asdfc");
    }
}
