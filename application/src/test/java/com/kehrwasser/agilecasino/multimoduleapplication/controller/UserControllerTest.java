package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotFoundException;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.UserNotRegisteredException;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.ImportModalState;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserPatch;

@SpringBootTest()
@Transactional
public class UserControllerTest {

    @Autowired
    ObjectMapper om;

    @Autowired
    private UserController userController;

    @Autowired
    private EntityManager entityManager;

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void canCreateAndRetrieveUser() {
        User user = userController.postUser(Optional.empty());
        resetEntities();
        User retrieved = userController.getUser(user.getUuid());

        assertThat(retrieved).isNotNull();
    }

    @Test
    public void throwWhenRetrievingNonexistentUser() {
        Exception e0 = null;

        try {
            userController.getUser("non-existent-uuid");
        } catch (UserNotFoundException e) {
            e0 = e;
        }

        assertThat(e0).isNotNull();
    }

    @Test
    public void throwWhenGettingProfileForNonexistentUser() {
        Exception e0 = null;

        try {
            userController.getUserProfile("non-existent-uuid", Optional.empty());
        } catch (UserNotFoundException e) {
            e0 = e;
        }

        assertThat(e0).isNotNull();
    }

    @Test
    public void throwWhenGettingProfileForAnonUser() {
        Exception e0 = null;
        User user = userController.postUser(Optional.empty());

        try {
            userController.getUserProfile(user.getUuid(), Optional.empty());
        } catch (UserNotRegisteredException e) {
            e0 = e;
        }

        assertThat(e0).isNotNull();
    }

    @Test
    public void throwWhenUpdatingNonexistentUser() {
        Exception e0 = null;

        try {
            userController.patchUser("non-existent-uuid", new UserPatch());
        } catch (UserNotFoundException e) {
            e0 = e;
        }

        assertThat(e0).isNotNull();
    }

    @Test
    public void throwWhenGettingJoinedRoomsAsNonexistentUser() {
        Exception e0 = null;

        try {
            userController.getJoinedRooms("non-existent-uuid");
        } catch (UserNotFoundException e) {
            e0 = e;
        }

        assertThat(e0).isNotNull();
    }


    @Test
    public void throwWhenGettingImportModalStateAsNonexistentUser() {
        Exception e0 = null;

        try {
            userController.getImportModalState("non-existent-uuid");
        } catch (UserNotFoundException e) {
            e0 = e;
        }

        assertThat(e0).isNotNull();
    }

    @Test
    public void throwWhenSettingImportModalStateAsNonexistentUser() {
        Exception e0 = null;

        try {
            userController.putImportModalState("non-existent-uuid", Optional.of(new ImportModalState()));
        } catch (UserNotFoundException e) {
            e0 = e;
        }

        assertThat(e0).isNotNull();
    }
}
