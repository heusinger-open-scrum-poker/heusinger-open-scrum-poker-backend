package com.kehrwasser.agilecasino.multimoduleapplication.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest()
public class UriBuilderTest {
    @Test
    public void simpleUrlTest() {
        URI uri = UriBuilder.baseUri("https://example.com").build();
        assertThat(uri.toString()).isEqualTo("https://example.com");
    }

    @Test
    public void pushSegmentTest() {
        URI uri = UriBuilder
            .baseUri("https://example.com/")
            .pushSegment("asdf asdf")
            .build();
        assertThat(uri.toString()).isEqualTo("https://example.com/asdf%20asdf");
    }

    @Test
    public void pushTest() {
        URI uri = UriBuilder
            .baseUri("https://example.com")
            .push("/asdf-asdf")
            .build();
        assertThat(uri.toString()).isEqualTo("https://example.com/asdf-asdf");
    }
}
