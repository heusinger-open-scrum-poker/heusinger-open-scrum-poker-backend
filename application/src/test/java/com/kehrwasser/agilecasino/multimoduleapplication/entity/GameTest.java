package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest()
public class GameTest {
    @Autowired
    ObjectMapper om;

    @Test()
    public void generateJson() {
        Game game = new Game();
        game.setId(10);
        game.setName("a game");
        game.setRoomId(123);
        game.setCreated_at(new Timestamp(1234567));



        JsonProcessingException exn = null; 
        String json = null;
        try {
            json = om.writeValueAsString(game);
        } catch (JsonProcessingException e) {
            exn = e;
        }

        assertThat(exn).isNull();
        assertThat(json).isNotNull();
        assertThat(json).isEqualTo("{\"id\":10,\"roomId\":123,\"name\":\"a game\",\"created_at\":1234567}");
    }
}
