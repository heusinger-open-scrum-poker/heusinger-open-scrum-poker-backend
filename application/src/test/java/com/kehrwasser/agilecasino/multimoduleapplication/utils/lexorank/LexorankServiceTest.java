package com.kehrwasser.agilecasino.multimoduleapplication.utils.lexorank;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.github.pravin.raha.lexorank4j.LexoRank;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;

@SpringBootTest()
public class LexorankServiceTest {
    @Test()
	public void repairRanks() {
        LexorankService service = new LexorankService();
        
        Task nullRank = new Task();
        nullRank.setRank(null);
        
        Task longRank = new Task();
        longRank.setRank("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        		+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        		+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        		+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        		+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        		+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        
        ArrayList<Task> tasks = new ArrayList<Task>();
        
        tasks.add(nullRank);
        tasks.add(longRank);
        
        boolean firstRun = service.repairRanks(tasks);
        
        assertThat(firstRun).isFalse();
        
        boolean secondRun = service.repairRanks(tasks);
        
        assertThat(secondRun).isTrue();
    }
	
    @Test()
    public void sortByRank() {
        LexorankService service = new LexorankService();
        
        Task taskMin = new Task();
        taskMin.setLexoRank(LexoRank.min());
        
        Task taskMax = new Task();
        taskMax.setLexoRank(LexoRank.max());
        
        Task taskMiddle = new Task();
        taskMiddle.setLexoRank(LexoRank.middle());
        
        ArrayList<Task> tasks = new ArrayList<Task>();
        
        tasks.add(taskMiddle);
        tasks.add(taskMax);
        tasks.add(taskMin);
        
        service.sortEntitiesByRank(tasks);
        
        assertThat(tasks.get(0)).isEqualTo(taskMin);
        assertThat(tasks.get(1)).isEqualTo(taskMiddle);
        assertThat(tasks.get(2)).isEqualTo(taskMax);
    }
}
