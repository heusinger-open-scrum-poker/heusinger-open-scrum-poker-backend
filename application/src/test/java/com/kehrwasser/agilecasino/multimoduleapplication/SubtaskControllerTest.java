package com.kehrwasser.agilecasino.multimoduleapplication;

import com.github.pravin.raha.lexorank4j.LexoRank;
import com.kehrwasser.agilecasino.multimoduleapplication.exception.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.*;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.dto.SubtaskOperation;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;

@SpringBootTest()
@Transactional
public class SubtaskControllerTest {
    @Autowired
    PlayerController playerController;

    @Autowired
    RoomController roomController;

    @Autowired
    TaskController taskController;

    @Autowired
    SubtaskController subtaskController;

    @Autowired
    UserController userController;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    SubtaskRepository subtaskRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    private class RoomWithTasks {
        public Room room;
        public User user;
        public Player player;
        public Task task;
        public List<Subtask> subtasks;
    }

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }

    private RoomWithTasks setupRoomWithTasksAndSubtask() {
        RoomWithTasks rwt = new RoomWithTasks();

        rwt.user = userController.postUser(Optional.empty());
        rwt.user.setRole(User.ROLE_PRO);
        Optional<String> userUuid = Optional.of(rwt.user.getUuid());

        rwt.room = roomController.createNewRoom(userUuid);
        String roomId = rwt.room.getId().toString();

        Player player = new Player();
        player.setName("abc");
        rwt.player = roomController.createPlayersOfRoom(roomId, player, userUuid);

        Task task = new Task();
        task.setSubject("some subject");
        task.setDescription("some description");
        task = taskController.createTask(roomId, userUuid.orElse(""), task);
        rwt.task = task;

        rwt.subtasks = new ArrayList<>();
        Subtask subtask1 = new Subtask();
        subtask1.setSubject("some subtask");
        subtask1.setTaskId(task.getId());
        subtask1 = subtaskController.createSubtask(roomId, userUuid.orElse(""), subtask1);
        rwt.subtasks.add(subtask1);

        Subtask subtask2 = new Subtask();
        subtask2.setSubject("another subtask");
        subtask2.setTaskId(task.getId());
        subtask2 = subtaskController.createSubtask(roomId, userUuid.orElse(""), subtask2);
        rwt.subtasks.add(subtask2);

        resetEntities();

        return rwt;
    }

    @Test()
    void canCreateSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        assertThat(rwt.subtasks.get(0)).isNotNull();
        assertThat(rwt.subtasks.get(0).getId()).isNotEqualTo(0);
    }

    @Test()
    void cannotCreateSubtaskIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask subtask = new Subtask();
        subtask.setSubject("new subtask");
        subtask.setTaskId(rwt.task.getId());


        Exception e1 = null;
        try {
            subtaskController.createSubtask(rwt.room.getId().toString(), "Wrong user id", subtask);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(UserNotFoundException.class);
    }

    @Test()
    void cannotCreateSubtaskIfNotPro() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask subtask = new Subtask();
        subtask.setSubject("new subtask");
        subtask.setTaskId(rwt.task.getId());

        User user = rwt.user;
        user.setRole(User.ROLE_SIGNEDUP);
        user = userRepository.save(user);

        Exception e1 = null;
        try {
            subtaskController.createSubtask(rwt.room.getId().toString(), user.getUuid(), subtask);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void cannotCreateSubtaskIfWrongTask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask subtask = new Subtask();
        subtask.setSubject("new subtask");
        subtask.setTaskId(0);

        Exception e1 = null;
        try {
            subtaskController.createSubtask(rwt.room.getId().toString(), rwt.user.getUuid(), subtask);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(TaskNotFoundException.class);
    }

    @Test()
    void cannotCreateSubtaskIfWrongRoom() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask subtask = new Subtask();
        subtask.setSubject("new subtask");
        subtask.setTaskId(rwt.task.getId());

        Exception e1 = null;
        try {
            subtaskController.createSubtask("wrong room id", rwt.user.getUuid(), subtask);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void cannotCreateSubtaskIfTaskDoesNotBelongToRoom() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Optional<String> userUuid = Optional.of(rwt.user.getUuid());
        Room room = roomController.createNewRoom(userUuid);

        Player player = new Player();
        player.setName("efg");
        roomController.createPlayersOfRoom(room.getId().toString(), player, userUuid);

        Task task = new Task();
        task.setSubject("dummy task");
        task.setDescription("dummy description");
        task = taskController.createTask(room.getId().toString(), userUuid.orElse(""), task);

        Subtask subtask = new Subtask();
        subtask.setSubject("new subtask");
        subtask.setTaskId(task.getId());

        Exception e1 = null;
        try {
            subtaskController.createSubtask(rwt.room.getId().toString(), rwt.user.getUuid(), subtask);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void canGetSubtasks() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        List<Subtask> subtasks = subtaskController.getSubtasks(rwt.room.getId().toString(), rwt.task.getId().toString(), rwt.user.getUuid());

        assertThat(subtasks.size()).isEqualTo(2);
        assertThat(subtasks.get(0).getSubject()).isEqualTo("some subtask");
        assertThat(subtasks.get(1).getSubject()).isEqualTo("another subtask");
    }

    @Test()
    void cannotGetSubtasksIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Exception e1 = null;
        try {
            subtaskController.getSubtasks(rwt.room.getId().toString(), rwt.task.getId().toString(), "Wrong user id");
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void cannotGetSubtasksIfWrongTask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Exception e1 = null;
        try {
            subtaskController.getSubtasks(rwt.room.getId().toString(), "wrong task Id", rwt.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(TaskNotFoundException.class);
    }

    @Test()
    void cannotGetSubtasksIfWrongRoom() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Exception e1 = null;
        try {
            subtaskController.getSubtasks("wrong room id", rwt.task.getId().toString(), rwt.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void cannotGetSubtasksIfTaskDoesNotBelongToRoom() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Optional<String> userUuid = Optional.of(rwt.user.getUuid());
        Room room = roomController.createNewRoom(userUuid);

        Player player = new Player();
        player.setName("efg");
        roomController.createPlayersOfRoom(room.getId().toString(), player, userUuid);

        Task task = new Task();
        task.setSubject("dummy task");
        task.setDescription("dummy description");
        task = taskController.createTask(room.getId().toString(), userUuid.orElse(""), task);

        Exception e1 = null;
        try {
            subtaskController.getSubtasks(rwt.room.getId().toString(), task.getId().toString(), rwt.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void canGetSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);

        Subtask actual = subtaskController.getSubtask(
                rwt.room.getId().toString(),
                target.getId().toString(),
                rwt.user.getUuid()
        );

        assertThat(actual.getId()).isEqualTo(target.getId());
    }

    @Test()
    void cannotGetSubtaskIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);

        Exception e1 = null;
        try {
            subtaskController.getSubtask(
                    rwt.room.getId().toString(),
                    target.getId().toString(),
                    "wrong user id"
            );
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void cannotGetSubtaskIfWrongSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Exception e1 = null;
        try {
            subtaskController.getSubtask(rwt.room.getId().toString(), "wrong subtask Id", rwt.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(SubtaskNotFoundException.class);
    }

    @Test()
    void cannotGetSubtaskIfWrongRoom() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);

        Exception e1 = null;
        try {
            subtaskController.getSubtasks("wrong room id", target.getId().toString(), rwt.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void canUpdateSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);
        Subtask other = rwt.subtasks.get(1);

        Subtask update = new Subtask();
        update.setChecked(true);
        update.setSubject("new subject");
        update.setRank(LexoRank.parse(other.getRank()).genNext().toString());

        subtaskController.updateSubtask(
                rwt.room.getId().toString(),
                target.getId().toString(),
                rwt.user.getUuid(),
                update
        );

        Subtask actual = subtaskRepository.findById(target.getId()).get();

        assertThat(actual.getId()).isEqualTo(target.getId());
        assertThat(actual.getChecked()).isTrue();
        assertThat(actual.getSubject()).isEqualTo("new subject");
        assertThat(actual.getSubject()).isEqualTo("new subject");

        List<Subtask> subtasks = subtaskController.getSubtasks(
                rwt.room.getId().toString(),
                rwt.task.getId().toString(),
                rwt.user.getUuid()
        );

        assertThat(subtasks.size()).isEqualTo(2);
        assertThat(subtasks.get(0).getSubject()).isEqualTo("another subtask");
        assertThat(subtasks.get(1).getSubject()).isEqualTo("new subject");
    }

    @Test()
    void cannotUpdateSubtaskIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);
        Subtask other = rwt.subtasks.get(1);

        Subtask update = new Subtask();
        update.setChecked(true);
        update.setSubject("new subject");
        update.setRank(LexoRank.parse(other.getRank()).genNext().toString());

        Exception e1 = null;
        try {
            subtaskController.updateSubtask(
                    rwt.room.getId().toString(),
                    target.getId().toString(),
                    "wrong user id",
                    update
            );
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void cannotUpdateSubtaskIfWrongSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask other = rwt.subtasks.get(1);

        Subtask update = new Subtask();
        update.setChecked(true);
        update.setSubject("new subject");
        update.setRank(LexoRank.parse(other.getRank()).genNext().toString());

        Exception e1 = null;
        try {
            subtaskController.updateSubtask(
                    rwt.room.getId().toString(),
                    "wrong subtask id",
                    rwt.user.getUuid(),
                    update
            );
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(SubtaskNotFoundException.class);
    }

    @Test()
    void cannotUpdateSubtaskIfWrongRoom() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);
        Subtask other = rwt.subtasks.get(1);

        Subtask update = new Subtask();
        update.setChecked(true);
        update.setSubject("new subject");
        update.setRank(LexoRank.parse(other.getRank()).genNext().toString());

        Exception e1 = null;
        try {
            subtaskController.updateSubtask(
                    "wrong room id",
                    target.getId().toString(),
                    rwt.user.getUuid(),
                    update
            );
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void cannotUpdateSubtaskIfRankInvalid() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);

        Subtask update = new Subtask();
        update.setChecked(true);
        update.setSubject("new subject");
        update.setRank("some invalid rank");

        Exception e1 = null;
        try {
            subtaskController.updateSubtask(
                    rwt.room.getId().toString(),
                    target.getId().toString(),
                    rwt.user.getUuid(),
                    update
            );
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(InvalidDataException.class);
    }

    @Test()
    void canDeleteSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);
        Integer id = target.getId();

        subtaskController.deleteSubtask(
                rwt.room.getId().toString(),
                target.getId().toString(),
                rwt.user.getUuid()
        );

        Optional<Subtask> actual = subtaskRepository.findById(id);

        assertThat(actual.isEmpty()).isTrue();
    }

    @Test()
    void subtasksWillBeDeletedOnCascade() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Task target = rwt.task;
        Integer id = target.getId();

        Iterable<Subtask> subtasks = subtaskRepository.findAllByTaskId(id);
        int size = 0;
        for (Object subtask : subtasks) {
            size++;
        }

        assertThat(size).isEqualTo(2);

        taskController.deleteTask(
                rwt.room.getId().toString(),
                id.toString(),
                rwt.user.getUuid()
        );

        Optional<Task> actual = taskRepository.findById(id);
        subtasks = subtaskRepository.findAllByTaskId(id);

        size = 0;
        for (Object subtask : subtasks) {
            size++;
        }

        assertThat(actual.isEmpty()).isTrue();
        assertThat(size).isEqualTo(0);
    }

    @Test()
    void cannotDeleteSubtaskIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);

        Exception e1 = null;
        try {
            subtaskController.deleteSubtask(
                    rwt.room.getId().toString(),
                    target.getId().toString(),
                    "wrong user id"
            );
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(PermissionDeniedException.class);
    }

    @Test()
    void cannotDeleteSubtaskIfWrongSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Exception e1 = null;
        try {
            subtaskController.deleteSubtask(
                    rwt.room.getId().toString(),
                    "wrong subtask id",
                    rwt.user.getUuid()
            );        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(SubtaskNotFoundException.class);
    }

    @Test()
    void cannotDeleteSubtaskIfWrongRoom() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);

        Exception e1 = null;
        try {
            subtaskController.deleteSubtask(
                    "wrong room id",
                    target.getId().toString(),
                    rwt.user.getUuid()
            );        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isExactlyInstanceOf(RoomNotFoundException.class);
    }

    @Test()
    void updateSubtasksCanCreate() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = new Subtask();
        target.setSubject("a new subtask");
        target.setTaskId(rwt.task.getId());
        SubtaskOperation operation = new SubtaskOperation();
        operation.setType(SubtaskOperation.OPERATION_CREATE);
        operation.setSubtask(target);

        List<SubtaskOperation> ops = new ArrayList<SubtaskOperation>();
        ops.add(operation);
        

        List<Subtask> result = subtaskController.updateSubtasks(
                rwt.room.getId().toString(),
                rwt.user.getUuid(),
                ops
                );

        assertThat(result.size()).isEqualTo(1);
    }

    @Test()
    void updateSubtasksCanIgnoreUpdateOfNonexistentSubtask() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = new Subtask();
        target.setId(-1);
        target.setSubject("a nonexistent subtask");
        SubtaskOperation operation = new SubtaskOperation();
        operation.setType(SubtaskOperation.OPERATION_UPDATE);
        operation.setSubtask(target);

        List<SubtaskOperation> ops = new ArrayList<SubtaskOperation>();
        ops.add(operation);
        


        List<Subtask> result = subtaskController.updateSubtasks(
                rwt.room.getId().toString(),
                rwt.user.getUuid(),
                ops
                );

        assertThat(result.size()).isEqualTo(0);
    }

    @Test()
    void updateSubtasksCanUpdate() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);
        target.setSubject("an updated subtask");
        SubtaskOperation operation = new SubtaskOperation();
        operation.setType(SubtaskOperation.OPERATION_UPDATE);
        operation.setSubtask(target);

        List<SubtaskOperation> ops = new ArrayList<SubtaskOperation>();
        ops.add(operation);
        


        List<Subtask> result = subtaskController.updateSubtasks(
                rwt.room.getId().toString(),
                rwt.user.getUuid(),
                ops
                );

        assertThat(result.size()).isEqualTo(1);
    }

    @Test()
    void updateSubtasksCanDelete() {
        RoomWithTasks rwt = setupRoomWithTasksAndSubtask();

        Subtask target = rwt.subtasks.get(0);
        SubtaskOperation operation = new SubtaskOperation();
        operation.setType(SubtaskOperation.OPERATION_DELETE);
        operation.setSubtask(target);

        List<SubtaskOperation> ops = new ArrayList<SubtaskOperation>();
        ops.add(operation);
        


        List<Subtask> result = subtaskController.updateSubtasks(
                rwt.room.getId().toString(),
                rwt.user.getUuid(),
                ops
                );

        assertThat(result.size()).isEqualTo(0);
    }
}
