package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.*;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;

@SpringBootTest()
@Transactional
public class WebSocketControllerTest {

    @Autowired
    private WebSocketController webSocketController;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private UserController userController;

    @Autowired
    private RoomController roomController;

    private Room room;

    @BeforeEach
    public void init() {

        System.out.println("BeforeEach init() method called");

        User user = userController.postUser(Optional.empty());
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));
        Player mockPayload = new Player();
        mockPayload.setName("Testname");
        roomController.createPlayersOfRoom(room.getId().toString(), mockPayload, Optional.of(user.getUuid()));

        this.room = room;

    }

    @Test
    public void testRoomIsRevealedAndCountedAsRound() {

        Integer roomId = this.room.getId();

        try {
            webSocketController.reveal(roomId.toString());
        } catch (Exception e) {
            System.out.println(e);
        }

        Optional<Room> room = roomRepository.findById(roomId);

        assertThat(room.get().getStatus()).isEqualTo("revealed");
        assertThat(room.get().getRoundsPlayed()).isEqualTo(1);

    }

    @Test
    public void testRevealTwiceShouldNotThrow() {

        String roomId = this.room.getId().toString();
        Exception exception = null;

        try {
            webSocketController.reveal(roomId);
            webSocketController.reveal(roomId);
        } catch (Exception e) {
            exception = e;
        }

        assertThat(exception).isNull();

    }

    @Test
    public void testRoomIsResetProperly() {

        Integer roomId = this.room.getId();

        try {
            webSocketController.reset(roomId.toString());
        } catch (Exception e) {
            System.out.println(e);
        }

        Optional<Room> room = roomRepository.findById(roomId);

        assertThat(room.isPresent());
        assertThat(room.get().getStatus()).isEqualTo("estimating");
        assertThat(room.get().getPlayers().get(0).getCardValue()).isEqualTo(Optional.empty());

    }

    @Test
    public void testResetTwiceShouldNotThrow() {

        String roomId = this.room.getId().toString();
        Exception exception = null;

        try {
            webSocketController.reveal(roomId);
            webSocketController.reset(roomId);
            webSocketController.reset(roomId);
        } catch (Exception e) {
            exception = e;
        }

        assertThat(exception).isNull();

    }

}
