package com.kehrwasser.agilecasino.multimoduleapplication;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public TestUtil util() {
        return new TestUtil();
    }
}
