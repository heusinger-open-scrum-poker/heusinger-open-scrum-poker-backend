package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.kehrwasser.agilecasino.multimoduleapplication.exception.TaskDoesNotBelongToActiveGameException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.*;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;

@SpringBootTest()
@Transactional
public class TaskControllerTest {
    @Autowired
    PlayerController playerController;

    @Autowired
    RoomController roomController;

    @Autowired
    TaskController taskController;

    @Autowired
    UserController userController;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private GameController gameController;

    private class RoomWithTasks {
        public Room room;
        public User user;
        public Player player;
        public List<Task> tasks;
    }

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }

    private RoomWithTasks setupRoomWithTasks() {
        RoomWithTasks rwt = new RoomWithTasks();

        rwt.user = userController.postUser(Optional.empty());
        Optional<String> userUuid = Optional.of(rwt.user.getUuid());

        rwt.room = roomController.createNewRoom(userUuid);
        String roomId = rwt.room.getId().toString();

        Player player = new Player();
        player.setName("abc");
        rwt.player = roomController.createPlayersOfRoom(roomId, player, userUuid);

        rwt.tasks = new ArrayList<Task>();
        Task task = new Task();
        task.setSubject("some subject");
        task.setDescription("some description");
        task.setEstimate("");
        task = taskController.createTask(roomId, userUuid.orElse(""), task);
        rwt.tasks.add(task);

        resetEntities();

        return rwt;
    }

    @Test()
    void entitiesAreSetUp() {
        RoomWithTasks rwt = setupRoomWithTasks();
        User user = userController.getUser(rwt.user.getUuid()); 
        Room room = roomController.getRoom(rwt.room.getId().toString(), Optional.empty());
        Player player = playerController.readPlayer(rwt.player.getId().toString());
        assertThat(rwt.tasks.size() > 0);
        Task task = taskController.getTask(rwt.room.getId().toString(), rwt.tasks.get(0).getId().toString(), rwt.user.getUuid());

        assertThat(user).isNotNull();
        assertThat(user.getUuid()).isEqualTo(rwt.user.getUuid());

        assertThat(room).isNotNull();
        assertThat(room.getId()).isEqualTo(rwt.room.getId());

        assertThat(player).isNotNull();
        assertThat(player.getId()).isEqualTo(rwt.player.getId());

        assertThat(task).isNotNull();
        assertThat(task.getId()).isEqualTo(rwt.tasks.get(0).getId());

        assertThat(room.getCurrentTask().isPresent()).isTrue();
        assertThat(room.getCurrentTask().get().getId()).isEqualTo(task.getId());
    }


    @Test()
    void canCreateTask() {

        RoomWithTasks rwt = setupRoomWithTasks();
        assertThat(rwt.tasks.get(0)).isNotNull();
        assertThat(rwt.tasks.get(0).getId()).isNotEqualTo(0);

    }

    @Test()
    void cannotCreateTaskIfWrongUser() {
        User user = userController.postUser(Optional.empty());
        Optional<String> userUuid = Optional.of(user.getUuid());

        Room room = roomController.createNewRoom(userUuid);
        String roomId = room.getId().toString();

        Player player = new Player();
        player.setName("abc");
        player = roomController.createPlayersOfRoom(roomId, player, userUuid);

        Task task = new Task();
        task.setSubject("some subject");
        task.setDescription("some description");

        Exception e1 = null;
        try {
            taskController.createTask(roomId, "wrong user uuid", task);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isNotNull();
    }

    @Test()
    void canFetchTask() {
        RoomWithTasks rwt = setupRoomWithTasks();
        List<Task> tasks = taskController.getTasks(rwt.room.getId().toString(), rwt.user.getUuid());

        assertThat(rwt.tasks.size() > 0).isTrue();
        assertThat(rwt.tasks.size()).isEqualTo(tasks.size());
        assertThat(rwt.tasks.get(0).getId()).isEqualTo(tasks.get(0).getId());
    }

    @Test()
    void cannotFetchTaskIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasks();
        Exception e1 = null;

        try {
            taskController.getTasks(rwt.room.getId().toString(), "wrong user uuid");
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isNotNull();
    }

    @Test()
    void canUpdateTask() {
        RoomWithTasks rwt = setupRoomWithTasks();
        assertThat(rwt.tasks.size() > 0).isTrue();
        Task oldTask = rwt.tasks.get(0);
        Task task = new Task();
        String subject = "new subject";
        String description = "new description";
        String estimate = "new estimate";
        task.setSubject(subject);
        task.setDescription(description);
        task.setEstimate(estimate);
        taskController.updateTask(rwt.room.getId().toString(), oldTask.getId().toString(), rwt.user.getUuid(), task);

        resetEntities();

        Optional<Task> check = taskRepository.findById(oldTask.getId());

        assertThat(check.isPresent()).isTrue();
        assertThat(check.get().getSubject()).isEqualTo(subject);
        assertThat(check.get().getDescription()).isEqualTo(description);
        assertThat(check.get().getEstimate()).isEqualTo(estimate);
    }

    @Test()
    void cannotUpdateTaskIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasks();
        assertThat(rwt.tasks.size() > 0).isTrue();
        Task oldTask = rwt.tasks.get(0);
        String roomId = rwt.room.getId().toString();
        String oldTaskId = oldTask.getId().toString();
        String userUuid = "wrong user uuid";

        Task task = new Task();
        String subject = "new subject";
        String description = "new description";
        String estimate = "new estimate";
        task.setSubject(subject);
        task.setDescription(description);
        task.setEstimate(estimate);

        Exception e1 = null;
        try {
        taskController.updateTask(roomId, oldTaskId, userUuid, task);
        } catch (Exception e) {
            e1 = e;
        }

        assertThat(e1).isNotNull();

    }

    @Test()
    void canDeleteTask() {
        RoomWithTasks rwt = setupRoomWithTasks();
        String roomId = rwt.room.getId().toString();
        String userUuid = rwt.user.getUuid();
        assertThat(rwt.tasks.size() > 0).isTrue();
        String taskId = rwt.tasks.get(0).getId().toString();

        resetEntities();
        List<Task> tasks1 = taskController.getTasks(roomId, userUuid);
        Integer tasks1Size = tasks1.size();
        assertThat(tasks1Size).isEqualTo(rwt.tasks.size());

        taskController.deleteTask(roomId, taskId, userUuid);

        resetEntities();
        List<Task> tasks2 = taskController.getTasks(roomId, userUuid);

        assertThat(tasks1Size > 0);
        assertThat(tasks2.size()).isEqualTo(tasks1Size - 1);
    }

    @Test()
    void cannotDeleteTaskIfWrongUser() {
        RoomWithTasks rwt = setupRoomWithTasks();
        String roomId = rwt.room.getId().toString();
        String userUuid = "wrong user uuid";
        assertThat(rwt.tasks.size() > 0).isTrue();
        String taskId = rwt.tasks.get(0).getId().toString();

        Exception e1 = null;
        try {
            taskController.deleteTask(roomId, taskId, userUuid);
        } catch(Exception e) {
            e1 = e;
        }

        assertThat(e1).isNotNull();
    }

    @Test()
    void canFetchTasksForGame() {
        RoomWithTasks rwt = setupRoomWithTasks();
        List<Task> tasks = taskController.getTasksForGame(
                rwt.room.getId().toString(),
                rwt.room.getActiveGameId(),
                rwt.user.getUuid()
        );

        assertThat(rwt.tasks.size() > 0).isTrue();
        assertThat(rwt.tasks.size()).isEqualTo(tasks.size());
        assertThat(rwt.tasks.get(0).getId()).isEqualTo(tasks.get(0).getId());
    }

    @Test()
    void canMakeCurrentTask() {
        RoomWithTasks rwt = setupRoomWithTasks();

        Task task = new Task();
        task.setSubject("new subject");
        task.setDescription("new description");
        task.setEstimate("new estimate");

        taskController.createTask(rwt.room.getId().toString(), rwt.user.getUuid(), task);

        assertThat(rwt.room.getCurrentTask().isPresent()).isTrue();
        assertThat(rwt.room.getCurrentTask().get().getSubject()).isEqualTo("some subject");

        taskController.makeCurrentTask(rwt.room.getId().toString(), task.getId().toString(), rwt.user.getUuid());

        Room room = roomController.getRoom(rwt.room.getId().toString(), Optional.of(rwt.user.getUuid()));

        assertThat(room.getCurrentTask().isPresent()).isTrue();
        assertThat(room.getCurrentTask().get().getSubject()).isEqualTo("new subject");
    }

    @Test()
    void canOnlyMakeCurrentTaskForActiveGame() {
        RoomWithTasks rwt = setupRoomWithTasks();

        assertThat(rwt.room.getCurrentTask().isPresent()).isTrue();
        assertThat(rwt.room.getCurrentTask().get().getSubject()).isEqualTo("some subject");

        Game game = new Game();
        game.setName("New Game");
        gameController.createGame(rwt.room.getId().toString(), Optional.of(rwt.user.getUuid()), game);

        Room room = roomController.getRoom(rwt.room.getId().toString(), Optional.of(rwt.user.getUuid()));
        assertThat(room.getCurrentTask().isPresent()).isFalse();

        Exception e1 = null;
        try {
            taskController.makeCurrentTask(rwt.room.getId().toString(), rwt.tasks.get(0).getId().toString(), rwt.user.getUuid());
        } catch (Exception e) {
            e1 = e;
        }
        assertThat(e1).isExactlyInstanceOf(TaskDoesNotBelongToActiveGameException.class);
    }
}
