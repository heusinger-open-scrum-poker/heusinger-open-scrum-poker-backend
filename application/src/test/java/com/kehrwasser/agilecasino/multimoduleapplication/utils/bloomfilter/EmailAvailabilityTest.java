package com.kehrwasser.agilecasino.multimoduleapplication.utils.bloomfilter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.RandomGenerator;

@SpringBootTest()
public class EmailAvailabilityTest {
    @Autowired
    ObjectMapper om;

    @Test
    void constructsValidEmails() {
        Exception exn = null;
        try {
            new Email("someone@example.com");
        } catch (Exception e) {
            exn = e;
        }
        assertThat(exn).isNull();
    }

    @Test
    void doesNotConstructInvalidEmails() {
        Exception exn = null;
        try {
            new Email("no-at,example.com");
        } catch (Exception e) {
            exn = e;
        }
        assertThat(exn).isNotNull();
    }

    @Test
    void noFalseNegatives() {
        EmailBloomFilter filter = new EmailBloomFilter();
        List<Email> list = new ArrayList<Email>();
        RandomGenerator gen = new RandomGenerator(123123);
        int numExamples = 100;

        for (int i = 0; i < numExamples; ++i) {
            Email email = new Email(gen.generateRandomEmail());
            list.add(email);
            filter.put(email);
        }

        list.stream().forEach(email -> {
            assertThat(filter.test(email)).isTrue();
        });
    }
}
