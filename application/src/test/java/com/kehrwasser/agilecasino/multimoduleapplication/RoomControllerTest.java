package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.*;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.*;

@SpringBootTest()
@Transactional
public class RoomControllerTest {

    @Autowired
    private RoomController roomController;

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private TaskRepository taskRepository;

    private Integer roomID;

    @Test
    public void testNewRoomIsPersisted() {

        Room room = roomController.createNewRoom(Optional.empty());

        roomID = room.getId();

        assertThat(roomID).isNotNull();

    }

    @Test
    public void testNewPlayersAreCounted() {

        User user = userController.postUser(Optional.empty());
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));
        Integer roomId = room.getId();

        Player mockPayload = new Player();
        mockPayload.setName("Testname");

        roomController.createPlayersOfRoom(roomId.toString(), mockPayload, Optional.of(user.getUuid()));
        Optional<Room> roomOpt = roomRepository.findById(roomId);

        assertThat(roomOpt.isPresent()).isTrue();
        assertThat(roomOpt.get().getMaxPlayers()).isEqualTo(1);

    }

    @Test
    public void testAvailableAvatarCanBeGenerated() {

        Room room = new Room();
        assertThat(RepositoryUtil.getAvailableAvatar(roomRepository, room)).isIn(Arrays.asList(Room.AVATARS));

    }

    @Test
    public void atLeastFifteenAvatars() {

        Room room = new Room();

        for (int i = 0; i < 15; i++) {

            assertThat(RepositoryUtil.getAvailableAvatar(roomRepository, room)).isNotNull();

            Player player = new Player();
            player.setAvatar(RepositoryUtil.getAvailableAvatar(roomRepository, room));
            room.addPlayer(player);

        }

    }

    @Test
    public void testNewPlayerIsPersisted() {

        Room room = roomController.createNewRoom(Optional.empty());

        User user = userController.postUser(Optional.empty());
        Player player = new Player();
        player.setName("Testname");
        player.setType("estimator");
        player.setUser(user);
        userRepository.save(user);
        playerRepository.save(player);


        String roomID = room.getId().toString();
        roomController.createPlayersOfRoom(roomID, player, Optional.of(user.getUuid()));

        assertThat(player.getId()).isNotNull();
        assertThat(room.getId()).isNotNull();

    }

    @Test
    public void testNewPlayerWithWrongTypeIsPersisted() {

        User user = userController.postUser(Optional.empty());
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));

        Player mockPayload = new Player();
        mockPayload.setName("Testname");
        mockPayload.setType("halunke");
        assertThat(mockPayload.getId()).isNull();

        String roomID = room.getId().toString();
        Player player = roomController.createPlayersOfRoom(roomID, mockPayload, Optional.of(user.getUuid()));

        assertThat(player.getId()).isNotNull();
        assertThat(room.getId()).isNotNull();

    }

    @Test
    public void testRoomIsRevealedAndCountedAsRound() {

        Room room = roomController.createNewRoom(Optional.empty());

        try {

            roomController.reveal(room, Optional.empty());
            room = roomRepository.findById(room.getId()).get();

        } catch (Exception e) {
            System.out.println(e);
        }

        assertThat(room.getStatus()).isEqualTo("revealed");
        assertThat(room.getRoundsPlayed()).isEqualTo(1);

    }

    @Test
    public void testRoomCannotBeRevealedTwice() {

        Room room = roomController.createNewRoom(Optional.empty());
        Exception exception = null;

        try {

            roomController.reveal(room, Optional.empty());
            roomController.reveal(room, Optional.empty());

        } catch (Exception e) {
            exception = e;
        }

        assertThat(exception).isNotNull();

    }

    @Test
    public void testRoomCannotBeResetedTwice() {

        Room room = roomController.createNewRoom(Optional.empty());
        Exception exception = null;

        try {

            roomController.reveal(room, Optional.empty());
            roomController.reset(room, Optional.empty());
            roomController.reset(room, Optional.empty());

        } catch (Exception e) {
            exception = e;
        }

        assertThat(exception).isNotNull();

    }

    @Test
    public void testStatusCanBeUpdated() {

        User user = userController.postUser(Optional.empty());
        userRepository.save(user);
        Player player = new Player();
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));
        player.setUser(user);
        player.setRoom(room);
        room.addPlayer(player);
        playerRepository.save(player);
        roomRepository.save(room);
        Room returnedRoom1 = null;
        Room returnedRoom2 = null;
        Exception exception = null;

        try {

            Room mockPayload = new Room();

            mockPayload.setId(room.getId());
            mockPayload.setStatus(new String("revealed"));

            String mockId = room.getId().toString();
            returnedRoom1 = roomController.updateRoom(mockId, mockPayload, Optional.of(user.getUuid()));
            assertThat(returnedRoom1.getStatus().equals("revealed")).isTrue();

            mockPayload.setRoundsPlayed(1);
            mockPayload.setStatus(new String("estimating"));
            returnedRoom2 = roomController.updateRoom(mockId, mockPayload, Optional.of(user.getUuid()));

        } catch (Exception e) {
            System.out.println(e);
            exception = e;
        }

        assertThat(exception).isNull();
        assertThat(room.getId()).isEqualTo(returnedRoom1.getId());
        assertThat(room.getId()).isEqualTo(returnedRoom2.getId());
        assertThat(returnedRoom2.getStatus().equals("estimating")).isTrue();

    }

    @Test
    public void testStatusForElapsedRoundCanNotBeUpdated() {

        Room room = roomController.createNewRoom(Optional.empty());
        Exception exception = null;
        String roomId = room.getId().toString();

        try {

            roomController.reveal(room, Optional.empty());

            Room mockPayload = new Room();
            mockPayload.setId(room.getId());
            mockPayload.setRoundsPlayed(0);
            mockPayload.setStatus("estimating");

            roomController.updateRoom(roomId, mockPayload, Optional.of(""));

        } catch (Exception e) {
            exception = e;
            System.out.println(e);
        }

        Room controlRoom = roomController.getRoom(roomId, Optional.empty());

        assertThat(room.getId()).isEqualTo(controlRoom.getId());
        assertThat(controlRoom.getStatus().equals("revealed")).isTrue();
        assertThat(exception).isNotNull();

    }

    @Test
    public void testSameStatusUpdateTwiceHasNoEffect() {


        User user = userController.postUser(Optional.empty());
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));
        Player mockPlayer = new Player();
        mockPlayer.setName("mockplayer");
        roomController.createPlayersOfRoom(room.getId().toString(), mockPlayer, Optional.of(user.getUuid()));
        Room returnedRoom = null;
        Room mockPayload = new Room();

        try {
            mockPayload.setStatus("estimating");
        } catch (Exception e) {
            System.out.println(e);
        }

        String roomId = room.getId().toString();
        returnedRoom = roomController.updateRoom(roomId, mockPayload, Optional.of(user.getUuid()));
        returnedRoom = roomController.updateRoom(roomId, mockPayload, Optional.of(user.getUuid()));

        assertThat(returnedRoom).isNotNull();
        assertThat(returnedRoom.getId()).isNotNull();
        assertThat(room.getId()).isEqualTo(returnedRoom.getId());
        assertThat(returnedRoom.getStatus().equals("estimating")).isTrue();

    }

    @Test
    public void cannotUpdateWithWrongRoundNumber() {


        User user = userController.postUser(Optional.empty());
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));
        Player mockPlayer = new Player();
        mockPlayer.setName("mockplayer");
        roomController.createPlayersOfRoom(room.getId().toString(), mockPlayer, Optional.of(user.getUuid()));
        Room returnedRoom = null;
        Room mockPayload = new Room();

        try {
            mockPayload.setStatus("revealed");
        } catch (Exception e) {
            System.out.println(e);
        }

        String roomId = room.getId().toString();
        returnedRoom = roomController.updateRoom(roomId, mockPayload, Optional.of(user.getUuid()));
        RuntimeException exception = null;
        try {
            roomController.updateRoom(roomId, mockPayload, Optional.of(user.getUuid()));
        } catch (RuntimeException e) {
            exception = e;
        }


        assertThat(returnedRoom).isNotNull();
        assertThat(returnedRoom.getId()).isNotNull();
        assertThat(room.getId()).isEqualTo(returnedRoom.getId());
        assertThat(returnedRoom.getStatus().equals("revealed")).isTrue();
        assertThat(returnedRoom.getRoundsPlayed().equals(1)).isTrue();
        assertThat(exception).isNotNull();

    }

    @Test
    public void saveInspectorWithoutAvatar() {
        User user = userController.postUser(Optional.empty());
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));

        Player player = new Player();
        player.setName("name");
        player.setType("inspector");
        player = roomController.savePlayerWithAvatar(room.getId(), player);

        assertThat(player.getAvatar()).isNull();
        assertThat(player.getType()).isNotNull();
        assertThat(player.getType().equals("inspector")).isTrue();
    }

    @Test
    public void saveEstimatorWithAvatar() {
        User user = userController.postUser(Optional.empty());
        Room room = roomController.createNewRoom(Optional.of(user.getUuid()));

        Player player = new Player();
        player.setName("name");
        player.setType("estimator");
        player = roomController.savePlayerWithAvatar(room.getId(), player);

        assertThat(player.getAvatar()).isNotNull();
        assertThat(player.getType()).isNotNull();
        assertThat(player.getType().equals("estimator")).isTrue();
    }




    @Test
    public void canSetCurrentTask() {

        Room room = roomController.createNewRoom(Optional.empty());
        Task task = new Task();
        room.setCurrentTask(Optional.of(task));

        room = roomRepository.save(room);
        task = taskRepository.save(task);

        Optional<Room> foundRoom = roomRepository.findById(room.getId());
        assertThat(foundRoom.isPresent()).isTrue();
        assertThat(foundRoom.get().getCurrentTask().isPresent()).isTrue();
        assertThat(foundRoom.get().getCurrentTask().get().getId()).isEqualTo(task.getId());

    }

    @Test()
    void initialGameIsCreatedWhenLoadingRoom() {
        User user = userController.postUser(Optional.empty());
        user.setRole(User.ROLE_ANONYMOUS);
        Optional<String> userUuid = Optional.of(user.getUuid());

        Room room = new Room();
        room = roomRepository.save(room);


        Player player = new Player();
        player.setName("just a new player");
        player = roomController.createPlayersOfRoom(room.getId().toString(), player, Optional.of(user.getUuid()));

        assertThat(room.getActiveGameId()).isNull();

        room = roomController.getRoom(room.getId().toString(), userUuid);

        assertThat(room.getActiveGameId()).isNotNull();
    }

    @Test()
    void initialGameIsCreatedWhenCreatingRoom() {
        User user = userController.postUser(Optional.empty());
        user.setRole(User.ROLE_ANONYMOUS);
        Optional<String> userUuid = Optional.of(user.getUuid());

        Room room = roomController.createNewRoom(userUuid);

        assertThat(room.getActiveGameId()).isNotNull();
    }
}
