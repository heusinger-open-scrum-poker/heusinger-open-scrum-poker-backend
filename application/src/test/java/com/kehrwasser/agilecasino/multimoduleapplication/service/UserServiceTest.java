package com.kehrwasser.agilecasino.multimoduleapplication.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.ImportModalState;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.ImportModalState.Metadata;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.UserPatch;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;

@SpringBootTest()
@Transactional
public class UserServiceTest {

    @Autowired
    ObjectMapper om;

    @Autowired
    private IUserService userService;

    @Autowired
    private EntityManager entityManager;

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void testNewPlayersAreCounted() {
        String newLanguage = "gr";

        User user = userService.createUser(Optional.empty());

        String lang0 = user.getLanguage();
        UserPatch patch = new UserPatch();
        patch.setLanguage(Optional.of(newLanguage));
        userService.updateUser(user, patch);
        String lang1 = userService.findUserByUuid(user.getUuid()).get().getLanguage();

        assertThat(lang0).isNull();
        assertThat(lang1).isEqualTo(newLanguage);
    }

    @Test
    public void importStateCanBeObtained() {
        User user = userService.createUser(Optional.empty());

        Optional<ImportModalState> state = userService.getImportModalState(user);
        assertThat(state).isEqualTo(Optional.empty());
    }

    @Test
    public void importStateCanBeSet() {
        User user = userService.createUser(Optional.empty());

        ImportModalState state = new ImportModalState();
        String[] currentPath = { "foo", "bar" };
        state.setCurrentPath(Arrays.asList(currentPath));
        state.setMetadata(new HashMap<String, Metadata>());
        userService.setImportModalState(user, Optional.of(state));
        resetEntities();
        Optional<ImportModalState> retrieved = userService.getImportModalState(user);

        assertThat(retrieved.isPresent()).isEqualTo(true);
        retrieved.ifPresent((it) -> {
            assertThat(it.getCurrentPath().size()).isEqualTo(2);
            assertThat(it.getCurrentPath().get(0)).isEqualTo("foo");
            assertThat(it.getCurrentPath().get(1)).isEqualTo("bar");
        });
    }

    @Test
    public void importStateCanBeEmpty() {
        User user = userService.createUser(Optional.empty());

        userService.setImportModalState(user, Optional.empty());
        resetEntities();
        Optional<ImportModalState> retrieved = userService.getImportModalState(user);
        assertThat(retrieved).isEqualTo(Optional.empty());
    }


    @Test
    public void emptyFieldsAreNotIncludedInImportState() {
        ImportModalState state = new ImportModalState();
        String[] currentPath = { "foo", "bar" };
        state.setCurrentPath(Arrays.asList(currentPath));
        state.setMetadata(new HashMap<String, Metadata>());

        String stateJson = null;
        Exception e1 = null;
        try {
            stateJson = om.writeValueAsString(state);
        } catch (Exception e) {
            e1 = e;
        }
        
        assertThat(e1).isNull();
        assertThat(stateJson).isEqualTo("{\"currentPath\":[\"foo\",\"bar\"],\"metadata\":{}}");
    }
}

