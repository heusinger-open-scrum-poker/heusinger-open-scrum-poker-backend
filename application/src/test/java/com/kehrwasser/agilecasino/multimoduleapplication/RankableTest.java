package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.github.pravin.raha.lexorank4j.LexoRank;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Task;

@SpringBootTest()
public class RankableTest {
    @Test()
    public void getAndSetLexorank() {
        Task task = new Task();

        assertThat(task.getRank()).isNull();
        
        LexoRank rank = LexoRank.middle();
        task.setLexoRank(rank);
        
        assertThat(task.getRank()).isEqualTo(rank.toString());
    }
}
