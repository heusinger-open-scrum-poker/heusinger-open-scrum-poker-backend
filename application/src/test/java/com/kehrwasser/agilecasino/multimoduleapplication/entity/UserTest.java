package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;

@SpringBootTest()
public class UserTest {
    @Autowired
    ObjectMapper om;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private AuthController authController;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private EntityManager entityManager;

    private TransactionTemplate transactionTemplate;

    @BeforeEach
    void setUp() {
        transactionTemplate = new TransactionTemplate(transactionManager);
    }

    @Test
    void canCreateUser() {
        User user = new User();
        userRepository.save(user);
        String uuid = user.getUuid();
        assertThat(uuid).isNotNull();
        assertThat(uuid.length()).isGreaterThan(1);

        Optional<User> findUser = userRepository.findById(uuid);
        assertThat(findUser.isPresent()).isTrue();
        assertThat(findUser.get().getUuid()).isEqualTo(uuid);
    }

    @Test
    void sameUuidsAfterSave() {
        User user = new User();
        userRepository.save(user);
        String uuid = user.getUuid();
        userRepository.save(user);
        String uuid2 = user.getUuid();
        assertThat(uuid).isEqualTo(uuid2);
    }

    /**
     * 
     */
    @Test
    @Transactional
    void testMergeAnonymousUser() {
        class Result {
            String anonUuid;
            Integer roomId;
            Integer playerId;
            String targetUuid;
        }

        Result result = transactionTemplate.execute(status -> {
            Result r = new Result();

            User anonUser = new User();
            anonUser = userRepository.save(anonUser);
            r.anonUuid = anonUser.getUuid();

            Room room = new Room();
            room.setCreatedBy(anonUser);
            room = roomRepository.save(room);
            r.roomId = room.getId();

            Player player = new Player();
            player.setName("testplayer");
            player.setUser(anonUser);
            player = playerRepository.save(player);
            r.playerId = player.getId();

            User targetUser = new User();
            targetUser = userRepository.save(targetUser);
            r.targetUuid = targetUser.getUuid();

            return r;
        });

        entityManager.flush();
        entityManager.clear();

        transactionTemplate.execute(status -> {
            authController.mergeAnonymousUser(result.anonUuid, result.targetUuid);
            return status;
        });

        entityManager.flush();
        entityManager.clear();

        transactionTemplate.execute(status -> {
            Optional<User> findUser = userRepository.findById(result.anonUuid);
            Optional<User> findUser2 = userRepository.findById(result.targetUuid);
            Optional<Room> findRoom = roomRepository.findById(result.roomId);
            Optional<Player> findPlayer = playerRepository.findById(result.playerId);

            assertThat(findUser.isEmpty()).isTrue();
            assertThat(findUser2.isPresent()).isTrue();

            assertThat(findUser2.get().getPlayers().size()).isEqualTo(1);
            assertThat(findUser2.get().getCreatedRooms().size()).isEqualTo(1);
            assertThat(findPlayer.isPresent()).isTrue();
            assertThat(findRoom.isPresent()).isTrue();
            assertThat(findRoom.get().getCreatedBy().isPresent()).isTrue();
            assertThat(findRoom.get().getCreatedBy().get().getUuid()).isEqualTo(result.targetUuid);
            assertThat(findPlayer.get().getUser().isPresent()).isTrue();
            assertThat(findPlayer.get().getUser().get().getUuid()).isEqualTo(result.targetUuid);

            return status;
        });
    }

    @Test
    void canSetLanguage() {
        User user = new User();
        userRepository.save(user);
        String lang0 = user.getLanguage();
        user.setLanguage("fr");
        userRepository.save(user);
        String lang1 = user.getLanguage();
        assertThat(lang0).isNull();
        assertThat(lang1).isEqualTo("fr");
    }

    @Test
    void defaultRole() {
        User user = new User();
        assertThat(user.getRole()).isNotNull();
        assertThat(user.getRole()).isEqualTo(User.ROLE_ANONYMOUS);
    }

    @Test
    void defaultSerialization() {
        User user = new User();

        JsonProcessingException exn = null; 
        String userJson = null;
        try {
            userJson = om.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            exn = e;
        }
        
        assertThat(exn).isNull();

        assertThat(userJson).matches("\\{\"uuid\":\"([-a-z0-9])+\",\"hash\":\"([a-f0-9])+\",\"createdAt\":(\\d)+,\"language\":null,\"role\":\"anonymous\"\\}");
    }
}
