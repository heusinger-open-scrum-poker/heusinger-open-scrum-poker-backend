package com.kehrwasser.agilecasino.multimoduleapplication.service;

import static org.assertj.core.api.Assertions.assertThat;

import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.EmailPasswordAuthentication;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;
import com.kehrwasser.agilecasino.multimoduleapplication.service.impl.EmailService;

@SpringBootTest()
public class EmailServiceTest {

    @Autowired
    private EmailService emailService;

    @Test
    public void createPasswordResetEmail_German() {
        User user = new User();
        user.setUuid("some-uuid");
        user.setLanguage("de");
        EmailPasswordAuthentication auth = new EmailPasswordAuthentication();
        auth.setEmail("user@example.com");
        user.setAuthMethod(auth);
        String token = "some-test-token";
        String content = null;
        Exception e0 = null;
        try {
            MimeMessageHelper builder = emailService.makePasswordResetMail(user, token);
            MimeMessage msg = builder.getMimeMessage();
            MimeMultipart multipart = (MimeMultipart)msg.getContent();
            content = (String)((MimeMultipart)multipart.getBodyPart(0).getContent()).getBodyPart(0).getContent();
        }
        catch (Exception e) {
            e0 = e;
        }

        assertThat(e0).isNull();
        assertThat(content).isEqualTo(
            "Hi,\n" +
            "\n" +
            "du hast um eine Zurücksetzung deines Passworts für dein Agile Casino Konto gebeten.\n" +
            "Klicke einfach auf den folgenden Link, um dein Passwort zurückzusetzen:\n" +
            "\n" +
            "http://localhost:4200/reset_password?token=some-test-token&email=user%40example.com\n" +
            "\n" +
            "Falls du dein Passwort nicht zurücksetzen wolltest, kannst du diese E-Mail einfach ignorieren.\n" +
            "\n" +
            "Bei weiteren Fragen oder Problemen wende dich bitte an unser Support-Team.\n" +
            "\n" +
            "Viele Grüße\n" +
            "Das Agile Casino Team\n"
        );
    }

    @Test
    public void createPasswordResetEmail_English() {
        User user = new User();
        user.setUuid("some-uuid");
        user.setLanguage("en");
        EmailPasswordAuthentication auth = new EmailPasswordAuthentication();
        auth.setEmail("user@example.com");
        user.setAuthMethod(auth);
        String token = "some-test-token";
        String content = null;
        Exception e0 = null;
        try {
            MimeMessageHelper builder = emailService.makePasswordResetMail(user, token);
            MimeMessage msg = builder.getMimeMessage();
            MimeMultipart multipart = (MimeMultipart)msg.getContent();
            content = (String)((MimeMultipart)multipart.getBodyPart(0).getContent()).getBodyPart(0).getContent();
        }
        catch (Exception e) {
            e0 = e;
        }

        assertThat(e0).isNull();
        assertThat(content).isEqualTo(
            "Hi,\n" +
            "\n" +
            "You've requested a password reset for your Agile Casino account.\n" +
            "Simply click the following link to reset your password:\n" +
            "\n" +
            "http://localhost:4200/reset_password?token=some-test-token&email=user%40example.com\n" +
            "\n" +
            "If you didn't request a password reset, you can safely ignore this email.\n" +
            "\n" +
            "For further questions or issues, please contact our support team.\n" +
            "\n" +
            "Best regards,\n" +
            "The Agile Casino Team\n"
        );
    }
}
