package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.CreateIssue;
import com.kehrwasser.agilecasino.multimoduleapplication.jira.api.CreateIssue.CreateIssueRequestBody;

@SpringBootTest()
public class JiraCreateIssueTest {
    @Autowired
    ObjectMapper om;

    /**
     * 
     */
    @Test()
    public void makeBodyTest() {
        JiraOAuth jira = new JiraOAuth();
        String accessToken = "the-access-token";
        jira.setAccessToken(accessToken);

        String resourceId = "resource-id";
        String projectId = "project-id";
        String issueType = "issue-type-id";
        String descriptionHtml = "<p>Some description</p>";
        String summary = "some-summary";

        String expectedJson = "{\"fields\":{\"description\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"paragraph\",\"content\":[{\"type\":\"text\",\"text\":\"Some description\"}]}]},\"project\":{\"id\":\"project-id\"},\"issuetype\":{\"id\":\"issue-type-id\"},\"summary\":\"some-summary\"}}";

        CreateIssue createIssue = new CreateIssue(jira, resourceId, projectId, issueType, descriptionHtml, summary);
        CreateIssueRequestBody body = createIssue.makeBody();
        String json = null;
        JsonProcessingException e0 = null;
		try {
			json = om.writeValueAsString(body);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
            e0 = e;
		}

        assertEquals(e0, null);
        assertEquals(json, expectedJson);
    }
}
