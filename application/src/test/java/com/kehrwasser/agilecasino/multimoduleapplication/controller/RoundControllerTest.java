package com.kehrwasser.agilecasino.multimoduleapplication.controller;

import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
@Transactional
public class RoundControllerTest {
    @Autowired
    RoomController roomController;

    @Autowired
    RoundController roundController;

    @Autowired
    GameController gameController;

    @Autowired
    UserController userController;

    @Autowired
    private EntityManager entityManager;

    private static class RoomWithGames {
        public Room room;
        public User user;
        public Player player;
        public List<Game> games;
    }

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }

    private RoomWithGames setupRoomWithGames() {
        RoomWithGames rwg = new RoomWithGames();

        rwg.user = userController.postUser(Optional.empty());
        rwg.user.setRole(User.ROLE_ANONYMOUS);
        Optional<String> userUuid = Optional.of(rwg.user.getUuid());

        rwg.room = roomController.createNewRoom(userUuid);
        String roomId = rwg.room.getId().toString();

        Player player = new Player();
        player.setName("abc");
        rwg.player = roomController.createPlayersOfRoom(roomId, player, userUuid);

        rwg.games = new ArrayList<>();

        Game game = new Game();
        game.setName("A");
        game = gameController.createGame(roomId, userUuid, game);
        rwg.games.add(game);

        resetEntities();

        return rwg;
    }

    @Test()
    void canStartRound() {
        RoomWithGames rwg = setupRoomWithGames();

        Round round = roundController.startRound(rwg.room.getId().toString(), rwg.user.getUuid());
        Room room = roomController.getRoom(rwg.room.getId().toString(), Optional.of(rwg.user.getUuid()));

        assertThat(room.getActiveRoundId()).isPresent();
        assertThat(room.getActiveRoundId().get()).isEqualTo(round.getId());

        assertThat(round.getInitiatorId()).isEqualTo(rwg.user.getUuid());
        assertThat(round.getRoomId()).isEqualTo(rwg.room.getId());
        assertThat(round.getGameId()).isEqualTo(rwg.games.get(0).getId());
        assertThat(round.getStartedAt()).isNotNull();
        assertThat(round.getRevealedAt()).isNull();
        assertThat(round.getRevealerId()).isNull();
    }

    @Test()
    void canEndRound() {
        RoomWithGames rwg = setupRoomWithGames();

        Round round = roundController.startRound(rwg.room.getId().toString(), rwg.user.getUuid());
        Room room = roomController.getRoom(rwg.room.getId().toString(), Optional.of(rwg.user.getUuid()));

        assertThat(room.getActiveRoundId()).isPresent();
        assertThat(room.getActiveRoundId().get()).isEqualTo(round.getId());

        round.setConsensus("XL");

        round = roundController.endRound(rwg.room.getId().toString(), rwg.user.getUuid(), round);
        room = roomController.getRoom(rwg.room.getId().toString(), Optional.of(rwg.user.getUuid()));

        assertThat(room.getActiveRoundId()).isNotPresent();

        assertThat(round.getConsensus()).isEqualTo("XL");
        assertThat(round.getRevealedAt()).isNotNull();
        assertThat(round.getRevealerId()).isEqualTo(rwg.user.getUuid());
    }
}
