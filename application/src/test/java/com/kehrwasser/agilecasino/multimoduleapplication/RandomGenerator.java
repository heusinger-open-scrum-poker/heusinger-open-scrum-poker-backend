package com.kehrwasser.agilecasino.multimoduleapplication;
import java.util.Random;

public class RandomGenerator {
    private Random random;
    public RandomGenerator(long seed) {
        random = new Random(seed);
    }

    public String generateRandomEmail() {
        String[] domains = {"gmail.com", "yahoo.com", "hotmail.com", "outlook.com", "example.com"};
        String[] prefixes = {"john", "emma", "david", "lisa", "michael", "sarah", "chris", "olivia"};

        String prefix = prefixes[random.nextInt(prefixes.length)];
        String domain = domains[random.nextInt(domains.length)];

        int randomNum = 1000 + random.nextInt(9000);
        String email = prefix + randomNum + "@" + domain;
        return email;
    }
}
