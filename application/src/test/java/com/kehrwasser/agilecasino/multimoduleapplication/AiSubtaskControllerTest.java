package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.*;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;
import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;
import com.kehrwasser.agilecasino.multimoduleapplication.controller.AiSubtaskController.QuotaResponse;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.OpenaiApi;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.SubtaskEstimate;
import com.kehrwasser.agilecasino.multimoduleapplication.openai.functionResponse.Usage;
import com.kehrwasser.agilecasino.multimoduleapplication.payload.JoinedRoom;

@SpringBootTest()
@Transactional
@ContextConfiguration(classes = AiSubtaskControllerTest.Mock.class)
public class AiSubtaskControllerTest {
    final static String ANONYMOUS_USER_ID = "anon-user";
    final static String PRO_USER_ID = "pro-user";
    final static String SIGNEDUP_USER_ID = "signed-up-user";

    @Autowired
    ObjectMapper om;

    @Autowired
    AiSubtaskController controller;

    @TestConfiguration
    public static class Mock {

        @Bean
        @Primary
        public OpenaiApi getOpenaiApiMock() {

            ObjectMapper om = new ObjectMapper()
                .registerModule(new Jdk8Module());

            return new OpenaiApi("", om) {
                @Override
                public GenerateSubtasksResult generateSubtasks(String summary) {
                    List<SubtaskEstimate> list = new ArrayList<SubtaskEstimate>();

                    SubtaskEstimate item = new SubtaskEstimate();
                    item.setSummary("Buy groceries according to recipe");
                    item.setEstimate(3);
                    list.add(item);

                    item = new SubtaskEstimate();
                    item.setSummary("Process, wash, ferment, freeze, etc.");
                    item.setEstimate(12);
                    list.add(item);

                    item = new SubtaskEstimate();
                    item.setSummary("Ignore the recipe and blend it all together");
                    item.setEstimate(0);
                    list.add(item);

                    Usage usage = new Usage();
                    usage.setTotal_tokens(200);
                    usage.setPrompt_tokens(100);
                    usage.setCompletion_tokens(100);

                    GenerateSubtasksResult result = new GenerateSubtasksResult();
                    result.setModel("chatgpt-3.5-turbo");
                    result.setUsage(usage);
                    result.setSubtasks(list);
                    return result;
                }
            };
        }

        @Bean
        @Primary
        public UserRepository getUserRepository() {

            return new UserRepository() {

				@Override
				public <S extends User> S save(S entity) {
					throw new UnsupportedOperationException("Unimplemented method 'save'");
				}

				@Override
				public <S extends User> Iterable<S> saveAll(Iterable<S> entities) {
					throw new UnsupportedOperationException("Unimplemented method 'saveAll'");
				}

				@Override
				public Optional<User> findById(String id) {
                    Map<String, User> users = new TreeMap<String, User>();


					User user;

                    user = new User();
                    user.setUuid(ANONYMOUS_USER_ID);
                    user.setRole(User.ROLE_ANONYMOUS);
                    users.put(ANONYMOUS_USER_ID, user);

                    user = new User();
                    user.setUuid(PRO_USER_ID);
                    user.setRole(User.ROLE_PRO);
                    users.put(PRO_USER_ID, user);

					user = new User();
                    user.setUuid(SIGNEDUP_USER_ID);
                    user.setRole(User.ROLE_SIGNEDUP);
                    users.put(SIGNEDUP_USER_ID, user);


                    user = users.get(id);

                    if (user == null) {
                        return Optional.empty();
                    }

                    return Optional.of(user);
				}

				@Override
				public boolean existsById(String id) {
					throw new UnsupportedOperationException("Unimplemented method 'existsById'");
				}

				@Override
				public Iterable<User> findAll() {
					throw new UnsupportedOperationException("Unimplemented method 'findAll'");
				}

				@Override
				public Iterable<User> findAllById(Iterable<String> ids) {
					throw new UnsupportedOperationException("Unimplemented method 'findAllById'");
				}

				@Override
				public long count() {
					throw new UnsupportedOperationException("Unimplemented method 'count'");
				}

				@Override
				public void deleteById(String id) {
					throw new UnsupportedOperationException("Unimplemented method 'deleteById'");
				}

				@Override
				public void delete(User entity) {
					throw new UnsupportedOperationException("Unimplemented method 'delete'");
				}

				@Override
				public void deleteAll(Iterable<? extends User> entities) {
					throw new UnsupportedOperationException("Unimplemented method 'deleteAll'");
				}

				@Override
				public void deleteAll() {
					throw new UnsupportedOperationException("Unimplemented method 'deleteAll'");
				}

				@Override
				public Iterable<User> findByHash(String ref) {
					throw new UnsupportedOperationException("Unimplemented method 'findByHash'");
				}

				@Override
				public Iterable<User> findByEmail(String email) {
					throw new UnsupportedOperationException("Unimplemented method 'findByEmail'");
				}

				@Override
				public Iterable<RegisteredUserInfo> findRegisteredUsers() {
					throw new UnsupportedOperationException("Unimplemented method 'findRegisteredUsers'");
				}

				@Override
				public Iterable<JoinedRoom> findJoinedRoomsById(String userUuid) {
					throw new UnsupportedOperationException("Unimplemented method 'findJoinedRoomsById'");
				}
            };
        }

        @Bean
        @Primary
        public OpenaiUsageRepository getOpenaiUsageRepository() {
            return new OpenaiUsageRepository() {

				@Override
				public <S extends OpenaiUsage> S save(S entity) {
					return entity;
				}

				@Override
				public <S extends OpenaiUsage> Iterable<S> saveAll(Iterable<S> entities) {
					throw new UnsupportedOperationException("Unimplemented method 'saveAll'");
				}

				@Override
				public Optional<OpenaiUsage> findById(Integer id) {
					throw new UnsupportedOperationException("Unimplemented method 'findById'");
				}

				@Override
				public boolean existsById(Integer id) {
					throw new UnsupportedOperationException("Unimplemented method 'existsById'");
				}

				@Override
				public Iterable<OpenaiUsage> findAll() {
					throw new UnsupportedOperationException("Unimplemented method 'findAll'");
				}

				@Override
				public Iterable<OpenaiUsage> findAllById(Iterable<Integer> ids) {
					throw new UnsupportedOperationException("Unimplemented method 'findAllById'");
				}

				@Override
				public long count() {
					throw new UnsupportedOperationException("Unimplemented method 'count'");
				}

				@Override
				public void deleteById(Integer id) {
					throw new UnsupportedOperationException("Unimplemented method 'deleteById'");
				}

				@Override
				public void delete(OpenaiUsage entity) {
					throw new UnsupportedOperationException("Unimplemented method 'delete'");
				}

				@Override
				public void deleteAll(Iterable<? extends OpenaiUsage> entities) {
					throw new UnsupportedOperationException("Unimplemented method 'deleteAll'");
				}

				@Override
				public void deleteAll() {
					throw new UnsupportedOperationException("Unimplemented method 'deleteAll'");
				}

				@Override
				public Integer getNumberOfEstimatesForUser(String userUuid) {
					return 3;
				}
                
            };
        }
    }


    @Test
    void acceptProUser() {
        Exception e = null;
        try {
            controller.postSubtaskEstimate("some summary", AiSubtaskControllerTest.PRO_USER_ID);
        } catch (Exception caught) {
            e = caught;
        }

        assertThat(e).isNull();
    }

    @Test
    void rejectNonProUser() {
        Exception e = null;
        try {
            controller.postSubtaskEstimate("some summary", AiSubtaskControllerTest.ANONYMOUS_USER_ID);
        } catch (Exception caught) {
            e = caught;
        }

        assertThat(e).isNotNull();
    }

    @Test
    void noQuotaForAnonymousUser() {
        Exception e = null;
        try {
            controller.getQuota(AiSubtaskControllerTest.ANONYMOUS_USER_ID);
        } catch (Exception caught) {
            e = caught;
        }

        assertThat(e).isNotNull();
    }

    @Test
    void withinQuota() {
        QuotaResponse resp = controller.getQuota(AiSubtaskControllerTest.SIGNEDUP_USER_ID);

        assertThat(resp.getFreeAiEstimations()).isNotNull();
        assertThat(resp.getFreeAiEstimations()).isEqualTo(2);
    }

    @Test
    void noQuotaForPro() {
        QuotaResponse resp = controller.getQuota(AiSubtaskControllerTest.PRO_USER_ID);

        assertThat(resp.getFreeAiEstimations()).isNull();
    }
}
