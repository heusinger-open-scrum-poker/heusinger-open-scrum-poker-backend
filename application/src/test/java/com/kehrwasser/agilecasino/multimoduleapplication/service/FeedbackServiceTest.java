package com.kehrwasser.agilecasino.multimoduleapplication.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.Feedback;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.User;

@SpringBootTest()
@Transactional
public class FeedbackServiceTest {

    @Autowired
    ObjectMapper om;

    @Autowired
    private IUserService userService;

    @Autowired
    private IFeedbackService feedbackService;

    @Autowired
    private EntityManager entityManager;

    private void resetEntities() {
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void canCreateAndRetrieveFeedback() {
        String content = "some feedback content";
        Feedback input = new Feedback();
        input.setContent(content);
        Feedback created = feedbackService.createFeedback(input);
        resetEntities();
        Optional<Feedback> retrieved = feedbackService.findFeedbackById(created.getId());

        assertThat(retrieved.isPresent()).isEqualTo(true);
        retrieved.ifPresent(it -> { 
            assertThat(it.getContent()).isEqualTo(content);
        });
    }

    @Test
    public void canUpdateEMail() {
        User user = userService.createUser(Optional.empty());
        String content = "some feedback content";
        String updatedContent = "some updated feedback content";
        String email = "some@emailaddr.es";
        Feedback input = new Feedback();
        input.setContent(content);
        input.setUserUuid(user.getUuid());
        Feedback created = feedbackService.createFeedback(input);
        resetEntities();
        Feedback update = new Feedback();
        update.setId(created.getId());
        update.setContent(updatedContent);
        update.setEmail(email);
        feedbackService.updateFeedback(update);
        resetEntities();
        Optional<Feedback> retrieved = feedbackService.findFeedbackById(created.getId());

        assertThat(retrieved.isPresent()).isEqualTo(true);
        retrieved.ifPresent(it -> { 
            assertThat(it.getContent()).isEqualTo(updatedContent);
            assertThat(it.getEmail()).isEqualTo(email);
        });
    }
}

