package com.kehrwasser.agilecasino.multimoduleapplication;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtil {
    @Autowired
    ObjectMapper om;

    public String encodeJSON(Object object) {
        try {
            return om.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
