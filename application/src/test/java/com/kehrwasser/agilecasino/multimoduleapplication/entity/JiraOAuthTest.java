package com.kehrwasser.agilecasino.multimoduleapplication.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kehrwasser.agilecasino.multimoduleapplication.TestUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.Util;

@SpringBootTest()
public class JiraOAuthTest {
    @Autowired
    ObjectMapper om;

    @Autowired
    TestUtil util;


    @Test()
    public void canBeConstructed() {
        JiraOAuth jira = new JiraOAuth();

        jira.setState("some state");
        jira.setAccessToken("some access token");
        jira.setRefreshToken("some refresh token");
        jira.setTokenType("code");
        jira.setScope("read:jira-work write:jira-work offline_access");

        String json = util.encodeJSON(jira);

        assertThat(json).isEqualTo("{\"id\":null,\"state\":\"some state\",\"accessToken\":\"some access token\",\"refreshToken\":\"some refresh token\",\"tokenType\":\"code\",\"scope\":\"read:jira-work write:jira-work offline_access\",\"expirationDatetime\":null}");

    }

    @Test()
    public void urlEncode() {
        String specialChars = Util.encodeURIComponent("~'()! ");
        assertThat(specialChars).isEqualTo("~'()!%20");

        String encoded = Util.encodeURIComponent("project = KEH sprint in futureSprints() and issueType in standardIssueTypes()");
        assertThat(encoded).isEqualTo("project%20%3D%20KEH%20sprint%20in%20futureSprints()%20and%20issueType%20in%20standardIssueTypes()");
    }
}
