package com.kehrwasser.agilecasino.multimoduleapplication;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import com.kehrwasser.agilecasino.multimoduleapplication.repository.*;
import com.kehrwasser.agilecasino.multimoduleapplication.utils.AuthUtil;
import com.kehrwasser.agilecasino.multimoduleapplication.entity.*;


@SpringBootTest()
public class AuthTest {

    @Autowired
    @Lazy
    private UserRepository userRepository;

    @Autowired
    @Lazy
    private AuthRepository authRepository;

    @Test
    public void passwordHashing() {
        User user = new User();

        String password = "some example password";
        EmailPasswordAuthentication auth = new EmailPasswordAuthentication(user, "some@email.com", password);
        auth.setPassword(password);

        auth.verifyPassword(password);

        Exception exception = null;
        try {
            String wrongPassword = password + "!!!!";
            auth.verifyPassword(wrongPassword);
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull();
    }

    @Test
    public void userWithAuthentication() {
        User user = new User();
        userRepository.save(user);

        EmailPasswordAuthentication auth = new EmailPasswordAuthentication(user, "example@example.org", "abcd1234");
        authRepository.save(auth);
        userRepository.save(user);

        String uuid = user.getUuid();
        Optional<User> user2 = userRepository.findById(uuid);
        assertThat(user2.isPresent()).isTrue();
        assertThat(user2.get().getAuthMethod()).isNotNull();
    }


    @Test
    public void pbkdf2Test() {
        String pw = "test password";
        PasswordEncoder enc = new Pbkdf2PasswordEncoder("", 65536, 512);
        String hashedPw = enc.encode(pw);
        assertThat(enc.matches(pw, hashedPw)).isTrue();
    }

    @Test
    public void pbkdf2TestWithDifferentEncoderInstances() {
        String pw = "test password";
        PasswordEncoder enc1 = new Pbkdf2PasswordEncoder("", 65536, 512);
        PasswordEncoder enc2 = new Pbkdf2PasswordEncoder("", 65536, 512);
        String hashedPw = enc1.encode(pw);
        assertThat(enc2.matches(pw, hashedPw)).isTrue();
    }

    @Test
    public void pbkdf2DistinctHashes() {
        String pw = "test password";
        PasswordEncoder enc = new Pbkdf2PasswordEncoder("", 65536, 512);
        String hashedPw1 = enc.encode(pw);
        String hashedPw2 = enc.encode(pw);
        assertThat(hashedPw1).isNotEqualTo(hashedPw2);
    }

    @Test
    public void pbkdf2ExamplePassword() {
        String pw = "agilecasino";
        PasswordEncoder enc = new Pbkdf2PasswordEncoder("", 65536, 512);
        String hashedPw = enc.encode(pw);
        assertThat(hashedPw.length()).isEqualTo("1b6710692e4dd0e509fb9325e20f427d615429f44a5c0cc65f29b51bba3eac2efe027d868fca5ff38475a3ca2e18ec34452070ec2ac5e39e7f0538dee507b32d675529b3f0d489aa".length());
    }

    @Test
    public void passwordRestTokenLength() {
        String token = AuthUtil.generatePasswordResetToken();
        assertThat(token.length()).isEqualTo(32);
    }
}
